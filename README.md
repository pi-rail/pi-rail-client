# PI-Rail-Client

This PI-Rail project contains common client code for all platforms.
It is based on PI-Mobile and PI-Rail-Base.
Android, iOS and Desktop clients are build on this project.

See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app and [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on firmware.
