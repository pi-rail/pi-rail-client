package de.pidata.actions;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.log.FileOwner;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.List;

public class SendLogOp extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
      SystemManager systemManager = SystemManager.getInstance();

      List<FileOwner> fileOwners = systemManager.getReportFileOwners();
      if (fileOwners.size() != 0) {
        systemManager.sendMail( null, "Error logs - App", "Error log files from App.", fileOwners );
      }
      else {
        sourceCtrl.getDialogController().showToast( "No logfiles available!" );
      }
  }
}
