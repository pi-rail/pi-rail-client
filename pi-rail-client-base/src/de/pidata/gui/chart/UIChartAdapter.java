package de.pidata.gui.chart;

import de.pidata.gui.ui.base.UIAdapter;

public interface UIChartAdapter extends UIAdapter {

  /**
   * Disable auto range for Y Axis and fix to given range
   * @param lowerBound new lower bound for Y-Axis
   * @param upperBound new upper bound for Y-Axis
   */
  void setYAxis( double lowerBound, double upperBound );

  /**
   * Create a ne series and add it to this char.
   *
   * @param name name of the series - needed for later add/remove
   */
  void addSeries( String name );

  /**
   * Add data to series name.
   *
   * @param name name of teh series to add data to
   * @param x    x-value
   * @param y    y-value
   */
  void addData( String name, String x, int y );

  /**
   * Add data to series name.
   *
   * @param name name of teh series to add data to
   * @param x    x-value
   * @param y    y-value
   */
  void addData( String name, int x, int y );

  /**
   * Remove first count data from series name
   * @param name  name of the series,  null for all
   * @param count amount of entries to remove
   */
  public void removeData( String name, int count );

  /**
   * Removes all data from all series, but does not remove the series itself.
   */
  public void clearData();
}
