package de.pidata.gui.chart;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.AbstractViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

public class ChartViewPI extends AbstractViewPI {

  private ChartController chartController;
  private UIChartAdapter uiChartAdapter;
  private List<String> seriesList = new ArrayList<>();

  public ChartViewPI( ChartController chartController, QName componentID, QName moduleID ) {
    super( componentID, moduleID );
    this.chartController = chartController;
  }

  /**
   * Returns this view's controller
   *
   * @return this view's controller
   */
  @Override
  public Controller getController() {
    return chartController;
  }

  /**
   * Returns the UIAdapter connected to this view. The UI adapter is null
   * if the view is not attached to the UI.
   *
   * @return this view's UIAdapter
   */
  @Override
  public UIAdapter getUIAdapter() {
    return uiChartAdapter;
  }

  /**
   * Updates the value at (posX,posY). The meaning of posX, posY depends on the type of component
   * and may be ignored by some components.
   *
   * @param value the new value
   * @deprecated
   */
  public void updateValue( Object value ) {
    if (uiChartAdapter != null) {
      //uiChartAdapter.
    }
  }

  /**
   * Called to attach this view's to it's UIAdapter and UI component
   * In case of a child view within a complex parent view
   * (e.g. Table or Tree) a dataContext may be necessary to identify
   * the UI component.
   *
   * @param uiContainer the container to look for this view's UI component
   */
  @Override
  public void attachUIComponent( UIContainer uiContainer ) {
    if (uiChartAdapter == null) {
      uiChartAdapter = (UIChartAdapter) uiContainer.getUIFactory().createCustomAdapter( uiContainer, this );
      for (String series : seriesList) {
        uiChartAdapter.addSeries( series );
      }
    }
  }

  /**
   * Called to create UIAdapter for matching component in uiContainer.
   * The view must not store any link to the created UIAdapter.
   * This kind of UIAdapters is used for rendering table a tree cells and
   * makes it possible to process events from within such a cell by this view.
   *
   * @param uiContainer the container to look for this view's UI component
   * @return the UIAdapter
   */
  @Override
  public UIAdapter attachUIRenderComponent( UIContainer uiContainer ) {
    UIChartAdapter uiChartAdapter = (UIChartAdapter) uiContainer.getUIFactory().createCustomAdapter( uiContainer, this );
    for (String series : seriesList) {
      uiChartAdapter.addSeries( series );
    }
    return uiChartAdapter;
  }

  /**
   * Called to detach this view from it's UIAdapter and UI component
   */
  @Override
  public void detachUIComponent() {
    if (uiChartAdapter != null) {
      uiChartAdapter.detach();
      uiChartAdapter = null;
    }
  }

  public void addSeries( String name ) {
    this.seriesList.add( name );
    if (uiChartAdapter != null) {
      uiChartAdapter.addSeries( name );
    }
  }

  public void addData( String seriesName, String x, int y ) {
    if (uiChartAdapter != null) {
      uiChartAdapter.addData( seriesName, x, y );
    }
  }

  public void addData( String seriesName, int x, int y ) {
    if (uiChartAdapter != null) {
      uiChartAdapter.addData( seriesName, x, y );
    }
  }

  /**
   * Remove first count data from series name
   * @param name  name of the series,  null for all
   * @param count amount of entries to remove
   */
  public void removeData( String name, int count ) {
    if (uiChartAdapter != null) {
      uiChartAdapter.removeData( name, count );
    }

  }

  /**
   * Removes all data from all series, but does not remove the series itself.
   */
  public void clearData() {
    if (uiChartAdapter != null) {
      uiChartAdapter.clearData();
    }
  }

  public void setYAxis( double lowerBound, double upperBound ) {
    if (uiChartAdapter != null) {
      uiChartAdapter.setYAxis( lowerBound, upperBound );
    }
  }
}
