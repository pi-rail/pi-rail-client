package de.pidata.gui.chart;

import de.pidata.gui.controller.base.AbstractValueController;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.CustomController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.guidef.Comp;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.ControllerType;
import de.pidata.gui.guidef.CustomCtrlType;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.ValidationException;
import de.pidata.qnames.QName;

public class ChartController extends AbstractValueController implements CustomController {

  private ChartViewPI chartViewPI;

  /**
   * Called by controllerBuilder to initialize this controller from its definition.
   *
   * @param compID            this controller's UI component ID
   * @param controllerBuilder the calling ControllerBuilder
   */
  @Override
  public void init( QName id, Controller parent, QName compID, ControllerBuilder controllerBuilder ) {
    super.init( id, parent, null, false );
    this.chartViewPI = new ChartViewPI( this, compID, null );
  }

  /**
   * Callend whenever a validation error occurs
   *
   * @param vex
   */
  @Override
  protected void setValidationError( ValidationException vex ) {
    // do nothing
  }

  /**
   * Returns this controller's view  or null if controller has no view
   *
   * @return this controller's view or null
   */
  @Override
  public ViewPI getView() {
    return chartViewPI;
  }

  /**
   * Return GuiOperation with given guiOpID or null if not found.
   *
   * @param guiOpID GuiOperation having eventID==guiOpID or null
   * @return
   */
  @Override
  public GuiOperation getGuiOperation( QName guiOpID ) {
    return null;
  }

  @Override
  public Object getValue() {
    return null;
  }

  @Override
  public void viewSetValue( Object value ) {
    // do nothing
  }

  public void addData( String seriesName, int x, int y ) {
    chartViewPI.addData( seriesName, x, y );
  }
}
