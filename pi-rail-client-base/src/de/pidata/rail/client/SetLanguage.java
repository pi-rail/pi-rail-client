package de.pidata.rail.client;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.event.Dialog;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

public class SetLanguage extends GuiOperation {

  public static QName DEUTSCH = NAMESPACE.getQName( "Deutsch" );
  public static QName ENGLISH = NAMESPACE.getQName( "English" );
  public static QName FRENCH = NAMESPACE.getQName( "French" );

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (eventID == DEUTSCH) {
      SystemManager.getInstance().setLocale( Locale.GERMAN );
    }
    else if (eventID == ENGLISH) {
      SystemManager.getInstance().setLocale( Locale.ENGLISH );
    }
    else if (eventID == FRENCH) {
      SystemManager.getInstance().setLocale( Locale.FRENCH );
    }
    Properties params = new Properties();
    params.put( "lang", Locale.getDefault().getDisplayLanguage() );
    DialogController rootDlgCtrl = sourceCtrl.getDialogController();
    while (rootDlgCtrl.getParentDialogController() != null) {
      rootDlgCtrl = rootDlgCtrl.getParentDialogController();
    }
    Dialog rootDlg = rootDlgCtrl.getDialogComp();
    rootDlg.updateLanguage();
    String title = SystemManager.getInstance().getLocalizedMessage( "languageSwitched_H", null, null );
    String msg = SystemManager.getInstance().getLocalizedMessage( "languageSwitched_TXT", null, params );
    try {
      SystemManager.getInstance().setProperty( "language", SystemManager.getInstance().getLocale().getLanguage(), true );
    }
    catch (IOException e) {
      Logger.error( "Error saving language.", e );
    }
    showMessage( sourceCtrl, title, msg );
  }
}
