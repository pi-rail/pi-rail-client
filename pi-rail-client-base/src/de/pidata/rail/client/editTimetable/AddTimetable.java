package de.pidata.rail.client.editTimetable;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.track.Timetable;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddTimetable extends GuiDelegateOperation<EditTimetableDelegate> {

  private String title = "";

  @Override
  protected void execute( QName eventID, EditTimetableDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    this.title = SystemManager.getInstance().getLocalizedMessage( "addTimetableName_H", null, null );
    String text = SystemManager.getInstance().getLocalizedMessage( "addTimetableName_TXT", null, null );
    String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
    String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
    showInput( sourceCtrl, this.title, text, "", btnOk, btnCancel );
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && this.title.equals( result.getTitle() )) {
        String timetableName = ((QuestionBoxResult) resultList).getInputValue();
        String msg = Timetable.checkName( timetableName );
        if (msg == null) {
          QName id = PiRailFactory.NAMESPACE.getQName( timetableName );
          ((EditTimetableDelegate) parentDlgCtrl.getDelegate()).addTimetable( id );
        }
        else {
          Properties params = new Properties();
          params.put( "name", timetableName );
          this.title = SystemManager.getInstance().getLocalizedMessage( "addTimetableInvalidName_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addTimetableInvalidName_TXT", null, params );
          parentDlgCtrl.showMessage( title, text + "\n" + msg );
        }
      }
    }
  }
}
