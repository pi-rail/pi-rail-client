package de.pidata.rail.client.editTimetable;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.ActionPickerParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.RailFunction;
import de.pidata.rail.track.Schedule;
import de.pidata.rail.track.Timetable;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class AddSchedule extends GuiDelegateOperation<EditTimetableDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditTimetableDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ActionPickerParamList paramList = new ActionPickerParamList( null, ModelRailway.GROUP_FAHRAUFTRAG, null, null );
    String title = SystemManager.getInstance().getLocalizedMessage( "addScheduleSelect_H", null, null );
    openChildDialog( sourceCtrl, NAMESPACE.getQName( "action_script_picker" ), title, paramList );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof ActionPickerParamList) {
      if (resultOK) {
        ActionPickerParamList result = (ActionPickerParamList) resultList;
        RailAction railAction = PiRail.getInstance().getModelRailway().getRailAction( null, result.getReferencedActionId() );
        if (railAction instanceof RailFunction) {
          RailFunction railFunction = (RailFunction) railAction;
          StateScript stateScript = railFunction.getStateScript( result.getReferencedDeviceId() );
          if (stateScript != null) {
            TableController timetableCtrl = ((EditTimetableDelegate) parentDlgCtrl.getDelegate()).getTimetableCtrl();
            Timetable timeTable = (Timetable) timetableCtrl.getSelection().getListBinding().getModel();
            Schedule newSchedule = new Schedule();
            newSchedule.setDeparture( "00:00" );
            newSchedule.setJobID( railFunction.getId() );
            newSchedule.setJobValue( stateScript.getValue() );
            timeTable.addSchedule( newSchedule );
            timetableCtrl.selectRow( newSchedule );
          }
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}

