package de.pidata.rail.client.editTimetable;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.track.Schedule;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditSchedule extends GuiDelegateOperation<EditTimetableDelegate> {

  private Schedule schedule;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditTimetableDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TableController timetableCtrl = delegate.getTimetableCtrl();
    Model selModel = timetableCtrl.getSelectedRow( 0 );
    if (selModel instanceof Schedule) {
      schedule = (Schedule) selModel;
      String title = SystemManager.getInstance().getLocalizedMessage( "editScheduleTime_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editScheduleTime_TXT", null, null );
      String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
      String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
      showInput( sourceCtrl, title, text, schedule.getDeparture(), btnOk, btnCancel );
    }
    else {
      schedule = null;
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        if (schedule != null) {
          schedule.setDeparture( ((QuestionBoxResult) resultList).getInputValue() );
          // TODO sort timetable by time
        }
      }
    }
    schedule = null;
  }
}

