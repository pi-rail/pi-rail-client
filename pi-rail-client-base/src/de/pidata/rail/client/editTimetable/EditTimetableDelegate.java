package de.pidata.rail.client.editTimetable;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.ConfigLoaderListener;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.track.Timetable;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;

public class EditTimetableDelegate implements DialogControllerDelegate<EditCfgParamList,ParameterList>, ConfigLoaderListener {

  protected EditCfgUI uiModel;
  protected DialogController dlgCtrl;
  protected QName deviceId;
  protected InetAddress ipAddress;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.deviceId = parameterList.getDeviceId();
    this.ipAddress = parameterList.getIPAddress();
    this.uiModel = new EditCfgUI( parameterList.getDeviceName(), deviceId, ipAddress );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.TRACK_CFG_XML, this, false, false );
    return uiModel;
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    //do nothing
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (EditCfgDelegate.CONFIG_DEFEKT.equals( result.getTitle() )) {
        if (resultOK) {
          Cfg cfg = new Cfg();
          uiModel.setCfg( cfg );
          initCfg( cfg );
        }
        else {
          dlgCtrl.close( false );
        }
      }
      else if (EditCfgDelegate.CONFIG_ERROR.equals( result.getTitle() )) {
        dlgCtrl.close( false );
      }
    }
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      Model config = configLoader.getConfigModel();
      if (config instanceof TrackCfg){
        TrackCfg trackCfg = (TrackCfg) config;
        uiModel.setTrackCfg( trackCfg );
        Cfg actionCfg = trackCfg.getActionCfg();
        if (actionCfg == null) {
          actionCfg = new Cfg();
          actionCfg.setId( trackCfg.getId() );
          trackCfg.setActionCfg( actionCfg );
        }
        uiModel.setCfg( (Cfg) actionCfg.clone( null, true, false ) );
        SelectionController selCtrl = (SelectionController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "timetableList" ) );
        selCtrl.activate( dlgCtrl.getDialogComp() );
      }
    }
    else {
      String text = SystemManager.getInstance().getLocalizedMessage( "editTimetableLoadError_TXT", null, null );
      dlgCtrl.showMessage( EditCfgDelegate.CONFIG_ERROR, text );
    }
  }

  private void initCfg( Cfg config ) {

  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public ListController getTimetableList() {
    return (ListController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "timetableList" ) );
  }

  public TableController getTimetableCtrl() {
    return (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "timetable" ) );
  }

  public void addTimetable( QName id ) {
    TrackCfg trackCfg = uiModel.getTrackCfg();
    Timetable newTimetable = new Timetable( id );
    trackCfg.addTimetable( newTimetable );
    getTimetableList().selectRow( newTimetable );
  }
}
