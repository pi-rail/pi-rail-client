package de.pidata.rail.client.wifiMonitor;

import de.pidata.gui.chart.ChartController;
import de.pidata.gui.chart.ChartViewPI;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.comm.WifiState;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.stream.StreamHelper;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class WifiMonitorDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, DataListener {

  public String wifiDB = "WifiDB";
  public String lowSignal = "schlecht";
  public String goodSignal = "optimal";

  private ChartController chartController;
  private List<String> wifiSeries = new ArrayList<>();
  private Locomotive locomotive;
  private DialogController dlgCtrl;
  private long startTime = -1;
  private long deviceTimeDelta = 0;
  private long lastUpdate = 0;

  private OutputStream csvOut;
  private PrintWriter csvPrint;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.wifiDB = SystemManager.getInstance().getGlossaryString( "wifiDB" );
    this.lowSignal = SystemManager.getInstance().getGlossaryString( "wifiLow" );
    this.goodSignal = SystemManager.getInstance().getGlossaryString( "wifiGood" );

    chartController = (ChartController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "wifiChart" ) );
    locomotive = PiRail.getInstance().getModelRailway().getLoco( parameterList.getDeviceId() );
    RailFunction modeFunction = locomotive.getModeFunction();
    if (modeFunction != null) {
      PiRail.getInstance().sendSetCommand( modeFunction, '0', 0, null );
    }
    try {
      Storage appDataDir = SystemManager.getInstance().getStorage( SystemManager.STORAGE_APPDATA, null );
      csvOut = appDataDir.write( "Wifi-"+locomotive.getDisplayName()+".csv", true, false );
      csvPrint = new PrintWriter( csvOut );
      csvPrint.println("Time;Balise;Distance;Signal");
    }
    catch (Exception ex) {
      Logger.error( "Error opening Wifi-Monitor.csv", ex );
      StreamHelper.close( csvOut );
      csvOut = null;
      csvPrint = null;
    }
    return locomotive;
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    ChartViewPI chartViewPI = (ChartViewPI) chartController.getView();
    chartViewPI.addSeries( lowSignal );
    chartViewPI.addSeries( goodSignal );
    locomotive.setDataListener( this );
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    locomotive.setDataListener( null );
    if (csvPrint != null) {
      csvPrint.flush();
      csvPrint.close();
    }
    StreamHelper.close( csvOut );
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  @Override
  public void processData( Csv csvData ) {
    // do nothing
  }

  @Override
  public void processState( State state ) {
    Ln ln = state.getLn();
    if (ln != null) {
      String posStr = "";
      QName posID = ln.getPosID();
      if (posID != null) {
        posStr = posID.getName();
      }
      long time = state.getMsLong();
      if (startTime < 0) {
        startTime = time;
        this.deviceTimeDelta = state.getReceiveTime() - startTime;
      }
      String wifiMac = state.getApMAC();
      if (Helper.isNullOrEmpty( wifiMac )) {
        wifiMac = wifiDB;
      }
      updateChart( wifiMac, time, state.getDBInt(), ln.getDistInt(), posStr );
    }
  }

  private void updateChart( String wifiMac, long time, int wifiDB, int posDist, String posStr ) {
    ChartViewPI chartViewPI = (ChartViewPI) chartController.getView();
    String xLabel = Long.toString((time - startTime) / 100 ) + "_" + posStr;
    for (String wifi : wifiSeries) {
      if (!wifi.equals( wifiMac )) {
        chartViewPI.addData( wifi, xLabel, -100 );
      }
    }
    if (wifiMac != null) {
      if (!wifiSeries.contains( wifiMac )) {
        chartViewPI.addSeries( wifiMac );
        wifiSeries.add( wifiMac );
      }
      chartViewPI.addData( wifiMac, xLabel, wifiDB );
    }
    chartViewPI.addData( lowSignal, xLabel, RailDevice.WIFI_DB_BAD );
    chartViewPI.addData( goodSignal, xLabel, RailDevice.WIFI_DB_GOOD );
    if (csvPrint != null) {
      csvPrint.println( time+";\""+ posStr +"\";"+ posDist +";"+ wifiDB );
    }
    this.lastUpdate = time;
  }

  @Override
  public void processWifiState( WifiState newState ) {
    if ((newState == WifiState.yellow) || (newState == WifiState.red)) {
      long time = System.currentTimeMillis() - deviceTimeDelta;
      if ((time - lastUpdate) > 900) {
        updateChart( null, time, -100, -1, "---" );
      }
    }
  }
}
