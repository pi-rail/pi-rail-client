package de.pidata.rail.client.z21;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.z21.Z21Comm;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;
import z21Drive.Z21;

import java.util.Properties;

public class StartZ21Central extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (Z21.isActive()) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralRunning_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralRunning_TXT", null, null );
      showMessage( sourceCtrl, title, text );
    }
    else if (PiRail.getInstance().startZ21()) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralSuccess_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralSuccess_TXT", null, null );
      showMessage( sourceCtrl, title, text );
    }
    else {
      String title = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralError_H", null, null );
      Properties params = new Properties();
      params.put( "port", Integer.toString( Z21Comm.Z21_PORT ) );
      String text = SystemManager.getInstance().getLocalizedMessage( "startZ21CentralError_TXT", null, params );
      showMessage( sourceCtrl, title, text );
    }
  }
}
