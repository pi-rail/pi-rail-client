package de.pidata.rail.client.editIcon;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.util.Properties;

public class ChangePic extends GuiOperation {

  public static final int iconMaxSize = 50; // kBytes

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    FileChooserParameter fileChooserParameter = new FileChooserParameter( ".", "*.jpg;*.png" );
    String title = SystemManager.getInstance().getLocalizedMessage( "changePicChooseFile_H", null, null );
    sourceCtrl.getDialogController().showFileChooser( title, fileChooserParameter );
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof FileChooserResult) {
      if (resultOK) {
        String filePath = ((FileChooserResult) resultList).getFilePath();
        if (!Helper.isNullOrEmpty( filePath )) {
          ComponentBitmap icon = Platform.getInstance().loadBitmapFile( filePath );
          long fileSize = new File( filePath ).length();
          if (fileSize > (50 * 1024)) {
            Properties params = new Properties();
            params.put( "maxSize", Integer.toString( iconMaxSize ) );
            String title = SystemManager.getInstance().getLocalizedMessage( "changePicTooBig_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "changePicTooBig_TXT", null, params );
            parentDlgCtrl.showMessage( title, text);
          }
          else {
            ChangePicCaller changePicCaller = (ChangePicCaller) parentDlgCtrl.getDelegate();
            changePicCaller.setIcon( filePath, icon );
          }
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
