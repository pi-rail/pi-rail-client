package de.pidata.rail.client.editIcon;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TextController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.net.InetAddress;

public class EditIconDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, ChangePicCaller {

  private QName deviceId;
  private InetAddress deviceAddress;
  private String filePath;
  private Locomotive locomotive;
  private DialogController dlgCtrl;

  public QName getDeviceId() {
    return deviceId;
  }

  public InetAddress getDeviceAddress() {
    return deviceAddress;
  }

  public String getFilePath() {
    return filePath;
  }

  @Override
  public void setIcon( String filePath, ComponentBitmap icon ) {
    this.filePath = filePath;
    RailDevice parentModel = (RailDevice) dlgCtrl.getModel();
    TextController controller = (TextController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "PicText" ) );
    controller.setValue( filePath );
    parentModel.setIcon( icon );
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.deviceId = parameterList.getDeviceId();
    this.deviceAddress = parameterList.getIPAddress();
    ModelRailway model = PiRail.getInstance().getModelRailway();
    this.locomotive = (Locomotive) model.getLoco( deviceId ).clone( null, true, true );
    return locomotive;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    TextController controller = (TextController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "PicText" ) );
    controller.setValue( "icon.jpg" );
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
