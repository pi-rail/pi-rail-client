package de.pidata.rail.client.editIcon;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.models.types.simple.Binary;

public interface ChangePicCaller {

  String getFilePath();

  void setIcon( String filePath, ComponentBitmap icon );
}
