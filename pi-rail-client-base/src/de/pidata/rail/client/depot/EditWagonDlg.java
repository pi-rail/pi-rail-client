package de.pidata.rail.client.depot;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditWagonDlg extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    WagonCfg wagon = (WagonCfg) dataContext;
    Depot depot = wagon.getDepot();
    EditWagonParamList parameterList = new EditWagonParamList( wagon.getId(), depot.getDeviceAddress(), depot.getName() );
    String title = SystemManager.getInstance().getLocalizedMessage( "editWagonEdit_H", null, null );
    sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_wagon" ), title, parameterList );
  }
}
