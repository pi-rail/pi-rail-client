package de.pidata.rail.client.depot;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.ListController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class DepotAddWagon extends GuiOperation {

  private Depot depot;

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ListController depotSelCtrl = (ListController) sourceCtrl.getControllerGroup().getController( ControllerBuilder.NAMESPACE.getQName( "depotSelection" ) );
    if (depotSelCtrl != null) {
      depot = (Depot) depotSelCtrl.getSelectedRow( 0 );
      if (depot != null) {
        SelectWagonParams selectWagonParams = new SelectWagonParams( null );
        String title = SystemManager.getInstance().getLocalizedMessage( "depotAddWaggonSelect_H", null, null );
        openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "select_wagon" ), title, selectWagonParams );
      }
    }
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK && (resultList instanceof SelectWagonParams)) {
      QName selectedID = ((SelectWagonParams) resultList).getSelectedWagonId();
      if (selectedID != null) {
        WagonCfg wagonCfg = new WagonCfg( selectedID );
        depot.addWagonCfg( wagonCfg );
        EditWagonParamList parameterList = new EditWagonParamList( selectedID, depot.getDeviceAddress(), depot.getName() );
        String title = SystemManager.getInstance().getLocalizedMessage( "depotAddWaggonNew_H", null, null );
        parentDlgCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_wagon" ), title, parameterList );
      }
    }
    depot = null;
  }
}
