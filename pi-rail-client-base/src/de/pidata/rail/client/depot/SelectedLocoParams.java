package de.pidata.rail.client.depot;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectedLocoParams extends AbstractParameterList {

  public static final QName SELECTED_LOCO_ID = ModelRailway.NAMESPACE.getQName( "selectedLocoID" );

  public SelectedLocoParams() {
    super( ParameterType.QNameType, SELECTED_LOCO_ID);
  }


  public SelectedLocoParams( QName selectedLoco ) {
    super( ParameterType.QNameType, SELECTED_LOCO_ID);
    setQName( SELECTED_LOCO_ID, selectedLoco );
  }

  public QName getCSelectedLocoId() {
    return getQName( SELECTED_LOCO_ID );
  }
}