package de.pidata.rail.client.depot;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EditWagonParamList extends AbstractParameterList {

  public static final QName WAGON_ID = ModelRailway.NAMESPACE.getQName( "wagonID" );
  public static final QName IP_ADDRESS = ModelRailway.NAMESPACE.getQName( "ipAddress" );
  public static final QName DEPOT_NAME = ModelRailway.NAMESPACE.getQName( "depotName" );

  public EditWagonParamList() {
    super( ParameterType.QNameType, WAGON_ID, ParameterType.StringType, IP_ADDRESS, ParameterType.QNameType, DEPOT_NAME );
  }

  public EditWagonParamList( QName wagonID, InetAddress ipAddress, QName depotName ) {
    this();
    setQName( WAGON_ID, wagonID );
    if (ipAddress != null) {
      setString( IP_ADDRESS, ipAddress.getHostAddress() );
    }
    setQName( DEPOT_NAME, depotName );
  }

  public QName getWagonID() {
    return getQName( WAGON_ID );
  }

  public InetAddress getIPAddress() throws UnknownHostException {
    return InetAddress.getByName( getString( IP_ADDRESS ) );
  }

  public QName getDepotName() {
    return getQName( DEPOT_NAME );
  }
}
