package de.pidata.rail.client.depot;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectWagonParams  extends AbstractParameterList {

  public static final QName SELECTED_WAGON_ID = ModelRailway.NAMESPACE.getQName( "selectedWagonID" );

  public SelectWagonParams() {
    super( ParameterType.QNameType, SELECTED_WAGON_ID );
  }

  public SelectWagonParams( QName selectedID ) {
    super( ParameterType.QNameType, SELECTED_WAGON_ID );
    setQName( SELECTED_WAGON_ID, selectedID );
  }

  public QName getSelectedWagonId() {
    return getQName( SELECTED_WAGON_ID );
  }
}