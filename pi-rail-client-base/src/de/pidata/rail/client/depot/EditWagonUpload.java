package de.pidata.rail.client.depot;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ServiceException;

public class EditWagonUpload extends SaveCfgOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, DialogControllerDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditWagonDelegate editWagonDelegate = (EditWagonDelegate) delegate;
    Depot depotCfg = editWagonDelegate.getDepot();
    if (depotCfg == null) {
      Logger.info( "Could not upload Depot, Depot is null" );
      return;
    }
    WagonCfg newWagonCfg = editWagonDelegate.getWagonCfg();
    depotCfg.getWagonCfg( newWagonCfg.getId() ).update( newWagonCfg );

    source.getDialogController().getDialogComp().showBusy( true );
    new Thread( new Runnable() {
      @Override
      public void run() {
        if (uploadDepot( editWagonDelegate.getDeviceAddress(), depotCfg, source.getDialogController() )) {
          source.getDialogController().close( true );
        }
        source.getDialogController().getDialogComp().showBusy( false );
      }
    } ).start();
  }
}
