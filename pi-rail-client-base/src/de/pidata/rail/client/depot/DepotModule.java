package de.pidata.rail.client.depot;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.ListController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;

public class DepotModule extends ModuleGroup {

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    setModel( modelRailway );
    ListController depotSelection = (ListController) getController( GuiBuilder.NAMESPACE.getQName( "depotSelection" ) );
    super.activateModule( moduleContainer );
  }
}

