package de.pidata.rail.client.depot;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.Wagon;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddWagonDialogDelegate implements DialogControllerDelegate<SelectedLocoParams,SelectWagonParams>, DialogValidator{

  private SelectedLocoParams currentWagonParams;
  TableController tableController;
  private DialogController dlgCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectedLocoParams parameterList ) throws Exception {
    this.currentWagonParams = parameterList;
    this.dlgCtrl = dlgCtrl;
    this.dlgCtrl.setValidator( this );

    return null;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    tableController = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "wagonTable" ) );
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectWagonParams dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      WagonCfg wagonCfg = (WagonCfg) tableController.getSelectedRow( 0 );
      QName selectedID = null;
      if (wagonCfg != null) {
        selectedID = wagonCfg.getId();
      }
      return new SelectWagonParams( selectedID );
    }
    else {
      return new SelectWagonParams( null );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  @Override
  public boolean validate( DialogController dlgCtrl ) {
    Locomotive selectedLoco = PiRail.getInstance().getModelRailway().getLoco( currentWagonParams.getCSelectedLocoId() );
    WagonCfg wagonCfg = (WagonCfg) tableController.getSelectedRow( 0 );
    Wagon selectedWagon = PiRail.getInstance().getModelRailway().getOrCreateWagon( wagonCfg.getId(), wagonCfg.getIcon() );

    if (selectedLoco.getWagonList().contains( selectedWagon )) {
      Properties params = new Properties();
      params.put( "name", selectedWagon.getName() );
      String title = SystemManager.getInstance().getLocalizedMessage( "addWaggonDialogDelegateError_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "addWaggonDialogDelegateError_TXT", null, params );
      dlgCtrl.showMessage( title, text );
      return false;
    }

    return true;
  }
}
