package de.pidata.rail.client.depot;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.net.InetAddress;
import java.util.Properties;

public class EditWagonIconUpload extends GuiDelegateOperation<EditWagonDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditWagonDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    String filePath = delegate.getFilePath();
    DialogController parentDlgCtrl = source.getDialogController();
    if (Helper.isNullOrEmpty( filePath )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadNoImage_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadNoImage_TXT", null, null );
      parentDlgCtrl.showMessage( title, text );
    }
    else {
      int pos = filePath.lastIndexOf( '/' );
      if (pos < 0) {
        pos = filePath.lastIndexOf( '\\' );
      }
      String filename;
      if (pos >= 0) {
        filename = filePath.substring( pos + 1 );
      }
      else {
        filename = filePath;
      }
      String msg = WagonCfg.checkIconName( filename );
      if (msg != null) {
        String title = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadInvalidName_H", null, null );
        parentDlgCtrl.showMessage( title, msg );
      }
      else {
        InetAddress deviceAddress = delegate.getDeviceAddress();
        Properties params = new Properties();
        params.put( "filename", filename );
        params.put( "addr", deviceAddress );
        try {
          File file = new File( filePath );
          if (file.exists()) {
            int rc = ConfigLoader.doUpload( deviceAddress, file, filename );
            if ((rc >= 200) && (rc < 300)) {
              Logger.info( "Successfully updated " + filename + " on " + deviceAddress );
              WagonCfg newWagonCfg = delegate.getWagonCfg();
              ComponentBitmap loadedBitmap = Platform.getInstance().getBitmap( newWagonCfg.getIcon() );
              QName newIconID = PiRailTrackFactory.NAMESPACE.getQName( filename );
              Platform.getInstance().addToImageCache( newIconID, loadedBitmap );
              newWagonCfg.setIcon( newIconID );
              delegate.updateFilePath();
              String title = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadSuccess_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadSuccess_TXT", null, params );
              parentDlgCtrl.showMessage( title,  text );
            }
            else {
              Logger.error( "Error updating " + filename + " on " + deviceAddress + ", responseCode=" + rc );
              String title = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadError_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadError_TXT", null, params );
              parentDlgCtrl.showMessage( title, text + "\nHTTP rc="+rc );
            }
          }
        }
        catch (Exception ex) {
          Logger.error( "Error updating " + filename + " on " + deviceAddress, ex );
          String title = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadError_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editWagonIconUploadError_TXT", null, params );
          parentDlgCtrl.showMessage( title, text + "\nmsg=" + ex.getLocalizedMessage() );
        }
      }
    }
  }
}
