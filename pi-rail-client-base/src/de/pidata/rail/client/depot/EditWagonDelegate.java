package de.pidata.rail.client.depot;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TextController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editIcon.ChangePicCaller;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.net.InetAddress;

public class EditWagonDelegate implements DialogControllerDelegate<EditWagonParamList, ParameterList>, ChangePicCaller {

  private InetAddress deviceAddress;
  private Depot depot;
  private WagonCfg wagonCfg;
  private String filePath;
  private DialogController dlgCtrl;
  private TextController imagePathCtrl;

  public void updateFilePath() {
    if (wagonCfg.getIcon() != null) {
      imagePathCtrl.setValue( deviceAddress.toString() + "/" + wagonCfg.getIcon().getName() );
    }
  }

  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditWagonParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    this.depot = modelRailway.getDepot( parameterList.getDepotName() );
    this.deviceAddress = parameterList.getIPAddress();
    TextController controller = (TextController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "depotName" ) );
    controller.setValue( depot.getName() );
    this.wagonCfg = (WagonCfg) depot.getWagonCfg( parameterList.getWagonID() ).clone( null, true, true );
    imagePathCtrl = (TextController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "imagePath" ) );
    updateFilePath();
    return wagonCfg;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public String getFilePath() {
    if (filePath == null) {
      QName iconID = wagonCfg.getIcon();
      if (iconID == null) {
        return null;
      }
      else {
        String url = getDeviceAddress().toString() + "/" +  iconID.getName();
        return url;
      }
    }
    else {
      return filePath;
    }
  }

  @Override
  public void setIcon( String filePath, ComponentBitmap icon ) {
    this.filePath = filePath;
    imagePathCtrl.setValue( filePath );
    QName iconID = GuiBuilder.NAMESPACE.getQName( filePath ); // use PI-Mobile namespace to mark as not yet uploaded
    Platform.getInstance().addToImageCache( iconID, icon );
    wagonCfg.setIcon( iconID );
  }

  public InetAddress getDeviceAddress() {
    return deviceAddress;
  }

  public Depot getDepot() {
    return depot;
  }

  public WagonCfg getWagonCfg() {
    return wagonCfg;
  }
}
