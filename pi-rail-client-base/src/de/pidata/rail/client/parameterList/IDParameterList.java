/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.parameterList;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class IDParameterList extends AbstractParameterList {

    public static final QName INTEGER_ID = ModelRailway.NAMESPACE.getQName("Integer_id");
    public static final QName STRING_ID = ModelRailway.NAMESPACE.getQName("String_id");

    public IDParameterList() {
        super(ParameterType.IntegerType, INTEGER_ID, ParameterType.StringType, STRING_ID);
    }

    public IDParameterList( int intId ){
        this ();
        setInteger(INTEGER_ID, Integer.valueOf( intId ));
    }
    public IDParameterList( String stringId ){
        this ();
        setString(STRING_ID, stringId);
    }
}
