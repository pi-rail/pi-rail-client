/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.parameterList;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EditConfigParameterList extends AbstractParameterList {

  public static final QName CONFIG_ID = ModelRailway.NAMESPACE.getQName( "configID" );
  public static final QName IP_ADDRESS = ModelRailway.NAMESPACE.getQName( "ipAddress" );
  public static final QName RELATION_NAME = ModelRailway.NAMESPACE.getQName( "relationName" );
  public static final QName CATALOG_PATH = ModelRailway.NAMESPACE.getQName( "catalogPath" );

  public EditConfigParameterList() {
    super( ParameterType.QNameType, CONFIG_ID, ParameterType.StringType, IP_ADDRESS, ParameterType.QNameType, RELATION_NAME, ParameterType.StringType, CATALOG_PATH );
  }

  public EditConfigParameterList( QName configId, InetAddress deviceIP, QName relationName ) {
    this( configId, deviceIP, relationName, null );
  }

  public EditConfigParameterList( QName configId, InetAddress deviceIP, QName relationName, String catalogPath ) {
    this();
    setQName( CONFIG_ID, configId );
    setString( IP_ADDRESS, deviceIP.getHostAddress() );
    setQName( RELATION_NAME, relationName );
    setString( CATALOG_PATH, catalogPath );
  }

  public QName getConfigID() {
    return getQName( CONFIG_ID );
  }

  public InetAddress getIPAddress() throws UnknownHostException {
    return InetAddress.getByName( getString( IP_ADDRESS ) );
  }

  public QName getRelationName() {
    return getQName( RELATION_NAME );
  }

  public String getCatalogPath() {
    return getString( CATALOG_PATH );
  }
}
