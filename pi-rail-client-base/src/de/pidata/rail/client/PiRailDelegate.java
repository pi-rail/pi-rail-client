/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.*;
import de.pidata.rail.comm.PiRail;

public class PiRailDelegate extends ModuleGroup {

  public static final QName MAIN_CONTROLLER = ControllerBuilder.NAMESPACE.getQName( "mainController" );
  public static final QName SELECTION_FRAGMENT = ControllerBuilder.NAMESPACE.getQName( "selection_fragment" );
  public static final QName CONTROL_FRAGMENT = ControllerBuilder.NAMESPACE.getQName( "control_fragment" );
  public static final QName TURNOUT_FRAGMENT = ControllerBuilder.NAMESPACE.getQName( "turnout_fragment" );
  public static final QName LIGHT_FRAGMENT = ControllerBuilder.NAMESPACE.getQName( "light_fragment" );

  public ModelRailway getModelRailway() {
    PiRail piRailApp = PiRail.getInstance();
    return piRailApp.getModelRailway();
  }

  public void switchTab( QName tabCtrlID, Model model ) throws Exception {
    ModuleController moduleController = (ModuleController) getController( MAIN_CONTROLLER );
    moduleController.setModel( null );

    moduleController.getDialogController().getControllerBuilder().loadModule( moduleController, tabCtrlID, moduleController, null );
    moduleController.setModel( model );


  }
}



