/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class StartFragment extends GuiDelegateOperation<MainDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, MainDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    Logger.info( "in StartFragment.execute" );

    if (eventID.getName().contains( "createDeviceList" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentModulList_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "device_list_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createLocoList" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentLocoList_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "selection_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createControlView" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentLocoControl_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "control_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createSwitchGrid" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentTrackPlan_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "switch_grid_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createTurnout" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentSwitchboard_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "turnout_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createTimeTable" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentTimetable_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "timetable_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createSensor" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentSensors_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "sensor_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createBlockList" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentBlockList_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "block_list_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else if (eventID.getName().contains( "createDepot" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "startFragmentDepot_H", null, null );
      source.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "depot_dialog" ), title, AbstractParameterList.EMPTY );
    }
    else {
      Logger.error( "Unsupported eventID in StartFragment: " + eventID.getName() );
    }
  }
}
