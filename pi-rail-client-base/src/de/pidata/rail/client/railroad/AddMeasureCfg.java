package de.pidata.rail.client.railroad;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.track.MeasureCfg;
import de.pidata.rail.track.RailroadCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddMeasureCfg extends GuiDelegateOperation<EditRailRoadDelegate> {

  public static final String TITLE_NEUE_MESSSTRECKE = "Neue Messstrecke";

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditRailRoadDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    sourceCtrl.getDialogController().showInput( TITLE_NEUE_MESSSTRECKE, "Name der Messstrecke", "", "Anlegen", "Abbruch" );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && TITLE_NEUE_MESSSTRECKE.equals( result.getTitle() )) {
        String name = result.getInputValue();
        String msg = MeasureCfg.checkID( name );
        if (msg != null) {
          Properties params = new Properties();
          params.put( "name", name );
          String title = SystemManager.getInstance().getLocalizedMessage( "addMeasureCfgInvalidName_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addMeasureCfgInvalidName_TXT", null, params );
          parentDlgCtrl.showMessage( title, text + "\n" + msg );
          return;
        }
        RailroadCfg railroadCfg = ((EditRailRoadDelegate) parentDlgCtrl.getDelegate()).getRailroadCfg();
        MeasureCfg measureCfg = new MeasureCfg();
        measureCfg.setName( name );
        railroadCfg.addMeasureCfg( measureCfg );
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
