package de.pidata.rail.client.railroad;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.track.*;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class SectionCfgNeu extends GuiDelegateOperation<EditRailRoadDelegate> {

  private String title = "";

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditRailRoadDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    EditTrackUI editTrackUI = (EditTrackUI) dataContext;
    TrackCfg trackCfg = editTrackUI.getTrackCfg();
    boolean changed = false;
    if (trackCfg == null) {
      trackCfg = new TrackCfg( PiRailTrackFactory.NAMESPACE.getQName( delegate.getDeviceName() ) );
      trackCfg.setDeviceAddress( editTrackUI.getInetAddress() );
      editTrackUI.setTrackCfg( trackCfg );
      changed = true;
    }
    RailroadCfg railroadCfg = trackCfg.getRailroadCfg();
    if (railroadCfg == null) {
      railroadCfg = new RailroadCfg();
      railroadCfg.setName( delegate.getDeviceName() );
      trackCfg.setRailroadCfg( railroadCfg );
      changed = true;
    }
    if (changed) {
      DialogController dlgCtrl = sourceCtrl.getDialogController();
      dlgCtrl.activate( dlgCtrl.getDialogComp() );
    }
    this.title = SystemManager.getInstance().getLocalizedMessage( "sectionCfgNeuName_H", null, null );
    String text = SystemManager.getInstance().getLocalizedMessage( "sectionCfgNeuName_TXT", null, null );
    String btnOk = SystemManager.getInstance().getGlossaryString( "create" );
    String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
    sourceCtrl.getDialogController().showInput( this.title, text, "", btnOk, btnCancel );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && this.title.equals( result.getTitle() )) {
        String name = result.getInputValue();
        String msg = SectionCfg.checkID( name );
        if (msg != null) {
          Properties params = new Properties();
          params.put( "name", name );
          this.title = SystemManager.getInstance().getLocalizedMessage( "sectionCfgNeuInvalidName_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "sectionCfgNeuInvalidName_TXT", null, params );
          parentDlgCtrl.showMessage( this.title, text + "\n" + msg );
          return;
        }
        EditTrackUI editTrackUI = (EditTrackUI) parentDlgCtrl.getModel();
        SectionCfg sectionCfg = new SectionCfg();
        sectionCfg.setName( name );
        editTrackUI.getTrackCfg().getRailroadCfg().addSectionCfg( sectionCfg );
        ((EditRailRoadDelegate) parentDlgCtrl.getDelegate()).setSectionCfg( sectionCfg );
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
