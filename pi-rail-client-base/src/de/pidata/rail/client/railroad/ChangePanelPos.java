package de.pidata.rail.client.railroad;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.track.PanelRef;
import de.pidata.service.base.ServiceException;

public class ChangePanelPos extends GuiDelegateOperation<EditRailRoadDelegate> {

  private static final QName ID_PANEL_ROW_DEC = ControllerBuilder.NAMESPACE.getQName( "panelRowDec" );
  private static final QName ID_PANEL_ROW_INC = ControllerBuilder.NAMESPACE.getQName( "panelRowInc" );
  private static final QName ID_PANEL_COL_DEC = ControllerBuilder.NAMESPACE.getQName( "panelColDec" );
  private static final QName ID_PANEL_COL_INC = ControllerBuilder.NAMESPACE.getQName( "panelColInc" );

  private int getInt( Integer value ) {
    if (value == null) {
      return 0;
    }
    else {
      return value.intValue();
    }
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditRailRoadDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    IntegerController colPosCtrl = (IntegerController) ctrlGroup.getController( EditRailRoadDelegate.ID_PANEL_POS_COL );
    int col = getInt( colPosCtrl.getIntegerValue() );
    IntegerController rowPosCtrl = (IntegerController) ctrlGroup.getController( EditRailRoadDelegate.ID_PANEL_POS_ROW );
    int row = getInt( rowPosCtrl.getIntegerValue() );

    if (eventID == ID_PANEL_COL_DEC) {
      if (col > 0) {
        col--;
        colPosCtrl.setValue( Integer.valueOf( col ) );
      }
    }
    else if (eventID == ID_PANEL_COL_INC) {
      if (col < 1000) {
        col++;
        colPosCtrl.setValue( Integer.valueOf( col ) );
      }
    }
    else if (eventID == ID_PANEL_ROW_DEC) {
      if (row > 0) {
        row--;
        rowPosCtrl.setValue( Integer.valueOf( row-1 ) );
      }
    }
    else if (eventID == ID_PANEL_ROW_INC) {
      if (row < 1000) {
        row++;
        rowPosCtrl.setValue( Integer.valueOf( row+1 ) );
      }
    }

    PanelRef panelRef = delegate.getSelectedPanelRef();
    if (panelRef != null) {
      panelRef.setX( Integer.valueOf( col ) );
      panelRef.setY( Integer.valueOf( row ) );
      delegate.updatePanelPos( panelRef );
    }
  }
}
