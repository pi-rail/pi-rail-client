package de.pidata.rail.client.railroad;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.ActionPickerParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.RailMessage;
import de.pidata.rail.track.MeasureCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;

public class EditMeasureCfg extends GuiDelegateOperation<EditRailRoadDelegate> {

  private static final QName ID_EDIT_START = GuiBuilder.NAMESPACE.getQName( "editStart" );

  private MeasureCfg measureCfg;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditRailRoadDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    TableController mesasureCfgTable = (TableController) ctrlGroup.getController( GuiBuilder.NAMESPACE.getQName( "mesasureCfgTable" ) );
    measureCfg = (MeasureCfg) mesasureCfgTable.getSelectedRow( 0 );
    if (measureCfg != null) {
      String title;
      QName attrID;
      QName selectedID;
      if (eventID == ID_EDIT_START) {
        attrID = MeasureCfg.ID_STARTMSGID;
        selectedID = measureCfg.getStartMsgID();
        title = "Start-Message";
      }
      else {
        attrID = MeasureCfg.ID_ENDMSGID;
        selectedID = measureCfg.getEndMsgID();
        title = "End-Message";
      }
      ActionPickerParamList parameterList = new ActionPickerParamList( attrID, ModelRailway.GROUP_BALISE, null, selectedID );
      sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "action_picker" ), title, parameterList );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof ActionPickerParamList) {
      if (resultOK) {
        QName attrName = ((ActionPickerParamList) resultList).getSelectedActionId();
        QName selectedID = ((ActionPickerParamList) resultList).getReferencedActionId();
        if ((attrName != null) && (selectedID != null)) {
          ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
          RailAction refAction = modelRailway.getRailAction( null, selectedID );
          if (refAction instanceof RailMessage) {
            measureCfg.set( attrName, ((RailMessage) refAction).getPosID() );
          }
          else {
            measureCfg.set( attrName, selectedID );
          }
        }
      }
    }
    super.dialogClosed( parentDlgCtrl, resultOK, resultList );
  }
}
