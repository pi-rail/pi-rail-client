package de.pidata.rail.client.railroad;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.client.swgrid.SwitchGridFigure;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.ConfigLoaderListener;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.track.*;
import de.pidata.rect.Rect;
import de.pidata.rect.SimpleRect;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EditRailRoadDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, ConfigLoaderListener, EventListener {

  public static final int smallCellSize = 10;
  public static final QName ID_PANEL_POS_ROW = ControllerBuilder.NAMESPACE.getQName( "panelPosRow" );
  public static final QName ID_PANEL_POS_COL = ControllerBuilder.NAMESPACE.getQName( "panelPosCol" );

  private EditTrackUI uiModel;
  private DialogController dlgCtrl;
  private Map<QName, SwitchGridFigure> switchGridFigureMap = new HashMap<QName,SwitchGridFigure>();
  private RailroadCfg railroadCfg;
  private SectionCfg sectionCfg;
  private PanelRef selectedPanelRef;
  private SwitchGridFigure selectedFigure = null;

  public InetAddress getDeviceAddress() {
    return uiModel.getInetAddress();
  }

  public String getDeviceName() {
    return uiModel.getDeviceName();
  }

  public PanelRef getSelectedPanelRef() {
    return selectedPanelRef;
  }

  public RailroadCfg getRailroadCfg() {
    return railroadCfg;
  }

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    QName deviceId = parameterList.getDeviceId();
    InetAddress ipAddress = parameterList.getIPAddress();
    this.uiModel = new EditTrackUI( parameterList.getDeviceName(), ipAddress );
    SelectionController sectionCfgCtrl = (SelectionController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "sectionCfgName" ) );
    sectionCfgCtrl.addListener( this );
    SelectionController panelCfgCtrl = (SelectionController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "panelCfgTable" ) );
    panelCfgCtrl.addListener( this );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.TRACK_CFG_XML, this, false, false );
    return uiModel;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public void setSectionCfg( SectionCfg sectionCfg ) {
    PaintController switchGrid = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
    PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();

    if (this.sectionCfg != null) {
      for (SwitchGridFigure switchGridFigure : switchGridFigureMap.values()) {
        switchGridView.removeFigure( switchGridFigure );
      }
      switchGridFigureMap.clear();
    }
    if (sectionCfg != null) {
      this.sectionCfg = sectionCfg;
      for (PanelRef panelRef : sectionCfg.panelRefIter()) {
        addPanelCfg( panelRef );
      }
      PanelRef newSelectedPanelRef = sectionCfg.getPanelRef( null );
      setSelectedPanelRef( newSelectedPanelRef );
    }
    SelectionController sectionCtrl = (SelectionController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "sectionCfgName" ) );
    sectionCtrl.selectRow( this.sectionCfg );
  }

  private void addPanelCfg( PanelRef panelRef ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    PanelCfg panelCfg = modelRailway.getPanelCfg( panelRef.getRefID() );
    if (panelCfg != null) {
      PaintController switchGrid = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
      PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
      double w = smallCellSize;
      double h = smallCellSize;
      SimpleRect bounds = new SimpleRect( panelRef.getXInt() * w, panelRef.getYInt() * h, 2 + panelCfg.panelColCount() * w, 2 + panelCfg.panelRowCount() * h );
      SwitchGridFigure switchGridFigure = new SwitchGridFigure( bounds, panelCfg, null, smallCellSize );
      this.switchGridFigureMap.put( panelCfg.getId(), switchGridFigure );
      switchGridView.addFigure( switchGridFigure );
    }
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      TrackCfg trackCfg = (TrackCfg) configLoader.getConfigModel();
      trackCfg.setDeviceAddress( uiModel.getInetAddress() );
      uiModel.setTrackCfg( trackCfg );
      railroadCfg = trackCfg.getRailroadCfg();
      dlgCtrl.activate( dlgCtrl.getDialogComp() );
      if (railroadCfg != null) {
        SectionCfg sectionCfg  = railroadCfg.getSectionCfg( null );
        if (sectionCfg != null) {
          SelectionController sectionCfgCtrl = (SelectionController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "sectionCfgName" ) );
          sectionCfgCtrl.selectRow( sectionCfg );
        }
      }
    }
  }

  public void saveGrid() {
    dlgCtrl.getDialogComp().showBusy( true );
    Runnable task = new Runnable() {
      @Override
      public void run() {
        TrackCfg trackCfg = uiModel.getTrackCfg();
        String panelDevice = null;
        if (trackCfg != null) {
          if (SaveCfgOperation.uploadTrackCfg( uiModel.getInetAddress(), trackCfg, dlgCtrl )) {
            panelDevice = uiModel.getDeviceName();
          }
        }
        dlgCtrl.getDialogComp().showBusy( false );
        if (panelDevice == null) {
          String title = SystemManager.getInstance().getLocalizedMessage( "editRailRoadNotUpdated_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editRailRoadNotUpdated_TXT", null, null );
          dlgCtrl.showMessage( title, text );
        }
        else {
          Properties params = new Properties();
          params.put( "moduleName", panelDevice );
          String title = SystemManager.getInstance().getLocalizedMessage( "editRailRoadUpdated_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editRailRoadUpdated_TXT", null, params );
          dlgCtrl.showMessage( title, text );
        }
      }
    };
    new Thread( task ).start();
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (newValue instanceof SectionCfg) {
      if (newValue != sectionCfg) {
        setSectionCfg( (SectionCfg) newValue );
      }
    }
    else if (newValue instanceof PanelCfg) {
      PanelCfg panelCfg = (PanelCfg) newValue;
      if (sectionCfg != null) {
        PanelRef newSelectedPanelRef = sectionCfg.findPanelRef( panelCfg.getId() );
        setSelectedPanelRef( newSelectedPanelRef );
      }
    }
  }

  protected void setSelectedPanelRef( PanelRef newSelectedPanelRef ) {
    if (selectedFigure != null) {
      ShapePI borderShape = selectedFigure.getBorderShape();
      borderShape.getShapeStyle().setBorderColor( ComponentColor.BLACK );
      borderShape.getShapeStyle().setStrokeWidth( 1 );
      this.selectedFigure = null;
      this.selectedPanelRef = null;
    }
    if (newSelectedPanelRef != null) {
      PanelCfg panelCfg = PiRail.getInstance().getModelRailway().getPanelCfg( newSelectedPanelRef.getRefID() );
      if (panelCfg != null) {
        SwitchGridFigure switchGridFigure = switchGridFigureMap.get( panelCfg.getId() );
        if (switchGridFigure != null) {
          ShapePI borderShape = switchGridFigure.getBorderShape();
          borderShape.getShapeStyle().setBorderColor( ComponentColor.ORANGE );
          borderShape.getShapeStyle().setStrokeWidth( 5 );
          selectedPanelRef = sectionCfg.findPanelRef( panelCfg.getId() );
          IntegerController rowPosCtrl = (IntegerController) dlgCtrl.getController( ID_PANEL_POS_ROW );
          rowPosCtrl.setValue( selectedPanelRef.getY() );
          IntegerController colPosCtrl = (IntegerController) dlgCtrl.getController( ID_PANEL_POS_COL );
          colPosCtrl.setValue( selectedPanelRef.getX() );
        }
        this.selectedFigure = switchGridFigure;
      }
    }
  }

  public void updatePanelPos( PanelRef panelRef ) {
    SwitchGridFigure switchGridFigure = switchGridFigureMap.get( panelRef.getRefID() );
    if (switchGridFigure != null) {
      Rect bounds = switchGridFigure.getBounds();
      bounds.setX( panelRef.getXInt() * smallCellSize );
      bounds.setY( panelRef.getYInt() * smallCellSize );
    }
  }

  public int getWidth( PanelRef panelRef ) {
    PanelCfg panelCfg = PiRail.getInstance().getModelRailway().getPanelCfg( panelRef.getRefID() );
    if (panelCfg != null) {
      return panelCfg.panelColCount();
    }
    else {
      return 0;
    }
  }

  public int getHeight( PanelRef panelRef ) {
    PanelCfg panelCfg = PiRail.getInstance().getModelRailway().getPanelCfg( panelRef.getRefID() );
    if (panelCfg != null) {
      return panelCfg.panelRowCount();
    }
    else {
      return 0;
    }
  }

  public void addPanel() {
    TableController panelCfgTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "panelCfgTable" ) );
    PanelCfg panelCfg = (PanelCfg) panelCfgTable.getSelectedRow( 0 );
    if (panelCfg != null) {
      SwitchGridFigure panelFigure = switchGridFigureMap.get( panelCfg.getId() );
      if (panelFigure == null) {
        if (sectionCfg != null) {
          int x = 0;
          for (PanelRef pRef : sectionCfg.panelRefIter()) {
            int newX = pRef.getX() + getWidth( pRef );
            if (newX > x) {
              x = newX;
            }
          }
          PanelRef panelRef = new PanelRef( panelCfg, x, 0 );
          sectionCfg.addPanelRef( panelRef );
          addPanelCfg( panelRef );
          setSelectedPanelRef( panelRef );
        }
      }
    }
  }

  public void removePanel() {
    TableController panelCfgTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "panelCfgTable" ) );
    PanelCfg panelCfg = (PanelCfg) panelCfgTable.getSelectedRow( 0 );
    if (panelCfg != null) {
      PanelRef panelRef = sectionCfg.getPanelRef( panelCfg.getId() );
      if (panelRef != null) {
        sectionCfg.removePanelRef( panelRef );
        SwitchGridFigure panelFigure = switchGridFigureMap.get( panelCfg.getId() );
        if (panelFigure != null) {
          PaintController switchGrid = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
          PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
          switchGridView.removeFigure( panelFigure );
          switchGridFigureMap.remove( panelCfg.getId() );
        }
      }
    }
  }
}
