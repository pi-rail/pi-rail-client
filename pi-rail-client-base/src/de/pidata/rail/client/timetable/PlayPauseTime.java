package de.pidata.rail.client.timetable;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.ButtonController;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.ListController;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.track.Timetable;
import de.pidata.service.base.ServiceException;

public class PlayPauseTime extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ModelRailway modelRailway = (ModelRailway) dataContext;
    ListController timetableList = (ListController) sourceCtrl.getControllerGroup().getController( GuiBuilder.NAMESPACE.getQName( "timetableList" ) );
    Timetable selectedTimetable = null;
    if (timetableList != null) {
      selectedTimetable = (Timetable) timetableList.getSelectedRow( 0 );
    }
    if (modelRailway.stopStartModelTime( selectedTimetable )) {
      sourceCtrl.setValue( NAMESPACE.getQName( "icons/actions/Loco/button_play.png" ) );
    }
    else {
      sourceCtrl.setValue( NAMESPACE.getQName( "icons/actions/Loco/button_stop.png" ) );
    }
  }
}
