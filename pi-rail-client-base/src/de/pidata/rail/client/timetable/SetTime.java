package de.pidata.rail.client.timetable;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class SetTime extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ModelRailway modelRailway = (ModelRailway) dataContext;
    String title = SystemManager.getInstance().getLocalizedMessage( "timetableSetTime_H", null, null );
    String text = SystemManager.getInstance().getLocalizedMessage( "timetableSetTime_TXT", null, null );
    String okLabel = SystemManager.getInstance().getLocalizedMessage( "take_BTN", null, null );
    String cancelLabel = SystemManager.getInstance().getLocalizedMessage( "cancel_BTN", null, null );
    showInput( sourceCtrl, title, text, modelRailway.getModelTime(), okLabel, cancelLabel );
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
        modelRailway.setNewModelTime( ((QuestionBoxResult) resultList).getInputValue() );
      }
    }
  }
}
