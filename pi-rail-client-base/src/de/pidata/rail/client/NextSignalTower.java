/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;

public class NextSignalTower extends GuiOperation {

  public static final QName OP_NEXT_SIGNALTOWER = GuiBuilder.NAMESPACE.getQName( "NextSignalTower" );
  public static final QName OP_PREV_SIGNALTOWER = GuiBuilder.NAMESPACE.getQName( "PrevSignalTower" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    SwitchBox current = (SwitchBox) dataContext;
    SwitchBox next;
    if (eventID == OP_PREV_SIGNALTOWER) {
      next = (SwitchBox) current.prevSibling( current.getParentRelationID() );
      if (next == null) {
        ModelRailway model = (ModelRailway) current.getParent( false );
        ModelIterator<SwitchBox> towers = model.switchBoxIter();
        while (towers.hasNext()) {
          next = towers.next();
        }
      }
    }
    else {
      next = (SwitchBox) current.nextSibling( current.getParentRelationID() );
      if (next == null) {
        ModelRailway model = (ModelRailway) current.getParent( false );
        next = model.switchBoxIter().next();
      }
    }
    if (next != null) {
      sourceCtrl.getControllerGroup().setModel( next );
    }
  }
}
