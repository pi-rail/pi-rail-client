package de.pidata.rail.client.motor;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.MotorAction;
import de.pidata.rail.model.MotorMode;
import de.pidata.service.base.ServiceException;

public class SetMotorParameter extends GuiDelegateOperation<MotorSetupDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, MotorSetupDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    IntegerController intCtrl = null;
    for (MotorAction moCfg : delegate.getLocomotive().getConfig().motorIter()) {
      if (moCfg.getGroup() == null) {
        QName setID = null;
        MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
        if (eventID == MotorSetupDelegate.ID_SET_KP) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KP );
          setID = MotorMode.ID_KP;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_KI) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KI );
          setID = MotorMode.ID_KI;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_KD) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KD );
          setID = MotorMode.ID_KD;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_MIN) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_MIN );
          setID = MotorAction.ID_MINVAL;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_MAX) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_MAX );
          setID = MotorAction.ID_MAXVAL;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_SLOW) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_SLOW );
          setID = MotorMode.ID_SLOW;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_DECEL) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_DECEL );
          setID = MotorMode.ID_DECEL;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_BRK_FAK) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_BRK_FAK );
          setID = MotorMode.ID_BRKFAK;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_DELAY) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_DELAY );
          setID = MotorMode.ID_DELAY;
        }
        else if (eventID == MotorSetupDelegate.ID_SET_ACCEL) {
          intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_ACCEL );
          setID = MotorMode.ID_ACCEL;
        }
        if (intCtrl != null) {
          Integer value = intCtrl.getIntegerValue();
          if (value != null) {
            PiRail.getInstance().sendSetCmd( delegate.getLocomotive(), moCfg.getId(), setID, value );
          }
        }
      }
    }
  }
}
