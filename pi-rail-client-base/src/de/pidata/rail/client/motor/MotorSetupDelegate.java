package de.pidata.rail.client.motor;

import de.pidata.gui.chart.ChartController;
import de.pidata.gui.chart.ChartViewPI;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.comm.WifiState;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.DataListener;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MotorSetupDelegate implements DialogControllerDelegate<EditCfgParamList,ParameterList>, DataListener {

  public static final QName ID_SET_KP = GuiBuilder.NAMESPACE.getQName( "setKP" );
  public static final QName ID_SET_KI = GuiBuilder.NAMESPACE.getQName( "setKI" );
  public static final QName ID_SET_KD = GuiBuilder.NAMESPACE.getQName( "setKD" );
  public static final QName ID_SET_MIN = GuiBuilder.NAMESPACE.getQName( "setMin" );
  public static final QName ID_SET_MAX = GuiBuilder.NAMESPACE.getQName( "setMax" );
  public static final QName ID_SET_SLOW = GuiBuilder.NAMESPACE.getQName( "setSlow" );
  public static final QName ID_SET_DECEL = GuiBuilder.NAMESPACE.getQName( "setDecel" );
  public static final QName ID_SET_BRK_FAK = GuiBuilder.NAMESPACE.getQName( "setBrkFak" );
  public static final QName ID_SET_DELAY = GuiBuilder.NAMESPACE.getQName( "setDelay" );
  public static final QName ID_SET_ACCEL = GuiBuilder.NAMESPACE.getQName( "setAccel" );

  public static final QName ID_MOTOR_MODE = GuiBuilder.NAMESPACE.getQName( "motorMode" );
  public static final QName ID_CURRENT_KP = GuiBuilder.NAMESPACE.getQName( "currentKP" );
  public static final QName ID_CURRENT_KI = GuiBuilder.NAMESPACE.getQName( "currentKI" );
  public static final QName ID_CURRENT_KD = GuiBuilder.NAMESPACE.getQName( "currentKD" );
  public static final QName ID_EDIT_KP = GuiBuilder.NAMESPACE.getQName( "editKP" );
  public static final QName ID_EDIT_KI = GuiBuilder.NAMESPACE.getQName( "editKI" );
  public static final QName ID_EDIT_KD = GuiBuilder.NAMESPACE.getQName( "editKD" );
  public static final QName ID_EDIT_MIN = GuiBuilder.NAMESPACE.getQName( "editMin" );
  public static final QName ID_EDIT_MAX = GuiBuilder.NAMESPACE.getQName( "editMax" );
  public static final QName ID_EDIT_SLOW = GuiBuilder.NAMESPACE.getQName( "editSlow" );
  public static final QName ID_EDIT_DECEL = GuiBuilder.NAMESPACE.getQName( "editDecel" );
  public static final QName ID_EDIT_BRK_FAK = GuiBuilder.NAMESPACE.getQName( "editBrkFak" );
  public static final QName ID_EDIT_DELAY = GuiBuilder.NAMESPACE.getQName( "editDelay" );
  public static final QName ID_EDIT_ACCEL = GuiBuilder.NAMESPACE.getQName( "editAccel" );

  public static final String SENSOR_PREFIX = "Sensor_";
  public static final String SENSOR_RAW_PREFIX = "Sensor_Raw_";
  public static final String SOLL = "Soll";
  public static final String STELLWERT = "Stellwert";
  public static final int MAX_DATA_COUNT = 200;

  private ChartController chartController;
  private Locomotive locomotive;
  private DialogController dlgCtrl;
  private long startX = -1;
  private String xStr;
  private int dataCount = 0;
  private List<QName> motorIDs = new ArrayList<>();

  private OutputStream csvOut;
  private PrintWriter csvPrint;
  private char motorModeValue;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    chartController = (ChartController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "motorChart" ) );
    locomotive = PiRail.getInstance().getModelRailway().getLoco( parameterList.getDeviceId() );
    try {
      Storage appDataDir = SystemManager.getInstance().getStorage( SystemManager.STORAGE_APPDATA, null );
      csvOut = appDataDir.write( "Motor-Setup.csv", true, false );
      csvPrint = new PrintWriter( csvOut );
      csvPrint.println( Data.HEADER_MO_DATA );
    }
    catch (Exception ex) {
      Logger.error( "Error opening Motor-Setup.csv", ex );
      StreamHelper.close( csvOut );
      csvOut = null;
      csvPrint = null;
    }
    return locomotive;
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  private boolean isSlaveMotor( MotorAction motorAction, QName mainMotorActionID ) {
    ItemConn itemConn = motorAction.getItemConn();
    for (ModelIterator iter = itemConn.itemActionIter(); iter.hasNext();) {
      Model itemAction = iter.next();
      if (itemAction instanceof TriggerAction) {
        if (((TriggerAction) itemAction).getOnAction() == mainMotorActionID) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    ChartViewPI chartViewPI = (ChartViewPI) chartController.getView();
    chartViewPI.addSeries( SOLL );
    chartViewPI.addSeries( STELLWERT );
    Cfg locoCfg = locomotive.getConfig();
    for (MotorAction motorAction : locoCfg.motorIter()) {
      if (motorAction.getGroup() == null) {
        MotorMode motorMode = motorAction.getMode( MotorMode.MOTOR_MODE_PID );
        if (motorMode == null) {
          motorMode = new MotorMode( MotorMode.MOTOR_MODE_PID );
          motorAction.addMode( motorMode );
        }
      }
    }

    TextController modeLabel = (TextController) dlgCtrl.getController( ID_MOTOR_MODE );
    motorModeValue = locomotive.getModeFunction().getStateChar();
    MotorAction mainMotorAction = (MotorAction) locomotive.getMainMotor().getAction();
    MotorMode motorMode = mainMotorAction.getMoMode( motorModeValue );
    modeLabel.setValue( motorMode.getId().getName() );
    IntegerController intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_KP );
    intCtrl.setValue( motorMode.getKP() );
    intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_KI );
    intCtrl.setValue( motorMode.getKI() );
    intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_KD );
    intCtrl.setValue( motorMode.getKD() );
    intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_MIN );
    intCtrl.setValue( mainMotorAction.getMinVal() );
    intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_MAX );
    intCtrl.setValue( mainMotorAction.getMaxVal() );
    intCtrl = (IntegerController) dlgCtrl.getController( ID_EDIT_SLOW );
    intCtrl.setValue( motorMode.getSlow() );
    intCtrl = (IntegerController) dlgCtrl.getController( MotorSetupDelegate.ID_EDIT_DECEL );
    intCtrl.setValue( motorMode.getDecel() );
    intCtrl = (IntegerController) dlgCtrl.getController( MotorSetupDelegate.ID_EDIT_BRK_FAK );
    intCtrl.setValue( motorMode.getBrkFak() );
    intCtrl = (IntegerController) dlgCtrl.getController( MotorSetupDelegate.ID_EDIT_DELAY );
    intCtrl.setValue( motorMode.getDelay() );
    intCtrl = (IntegerController) dlgCtrl.getController( MotorSetupDelegate.ID_EDIT_ACCEL );
    intCtrl.setValue( motorMode.getAccel() );

    int motorIndex = 1;
    chartViewPI.addSeries( SENSOR_PREFIX + motorIndex );
    chartViewPI.addSeries( SENSOR_RAW_PREFIX + motorIndex );
    QName mainMotorActionID = mainMotorAction.getId();
    PiRail.getInstance().sendSetCmd( locomotive, mainMotorActionID, MotorAction.ID_SENDCSV, Integer.valueOf( motorIndex ) );
    motorIDs.add( mainMotorActionID );
    for (MotorAction motorAction : locoCfg.motorIter()) {
      if (isSlaveMotor( motorAction, mainMotorActionID)) {
        motorIndex++;
        chartViewPI.addSeries( SENSOR_PREFIX + motorIndex );
        chartViewPI.addSeries( SENSOR_RAW_PREFIX + motorIndex );
        PiRail.getInstance().sendSetCmd( locomotive, motorAction.getId(), MotorAction.ID_SENDCSV, Integer.valueOf( motorIndex ) );
        motorIDs.add( motorAction.getId() );
      }
    }
    locomotive.setDataListener( this );
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    locomotive.setDataListener( null );
    if (csvPrint != null) {
      csvPrint.flush();
      csvPrint.close();
    }
    StreamHelper.close( csvOut );
    for (QName motorActionID : motorIDs) {
      PiRail.getInstance().sendSetCmd( locomotive, motorActionID, MotorAction.ID_SENDCSV, Integer.valueOf( 0 ) );
    }
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public char getMotorModeValue() {
    return motorModeValue;
  }

  public void setMotorModeValue( char motorModeValue ) {
    this.motorModeValue = motorModeValue;
  }

  public void updatePID( String kP, String kI, String kD ) {
    TextController intCtrl = (TextController) dlgCtrl.getController( ID_CURRENT_KP );
    intCtrl.setValue( kP );
    intCtrl = (TextController) dlgCtrl.getController( ID_CURRENT_KI );
    intCtrl.setValue( kI );
    intCtrl = (TextController) dlgCtrl.getController( ID_CURRENT_KD );
    intCtrl.setValue( kD );
  }

  @Override
  public void processData( Csv csvData ) {
    if (csvData.getT() == CsvType.moData) {
      int index = csvData.getIndex();
      if (index == 0) {
        index = 1; // compatibility to firmware pre 20230616
      }
      ChartViewPI chartViewPI = (ChartViewPI) chartController.getView();
      chartViewPI.setYAxis( 0.0, 2500.0 );
      if (csvPrint != null) {
        csvPrint.println( csvData.getDat() );
      }
      String[] values = csvData.getDat().split( ";" );
      long time = Long.parseLong( values[0] );
      if (startX < 0) {
        startX = time;
      }
      long x = time - startX + 1000000;
      int stellwert = Integer.parseInt( values[1] );
      int sensorWert = Integer.parseInt( values[2] );
      int sollWert = Integer.parseInt( values[3] );
      int sensorRaw = Integer.parseInt( values[11] );
      if ((stellwert != 0) || (sensorWert != 0) || (sollWert != 0)) {
        if (index == 1) {
          xStr = Long.toString( x );
          chartViewPI.addData( STELLWERT, xStr, stellwert );
          chartViewPI.addData( SOLL, xStr, sollWert );
          dataCount++;
          if (dataCount >= MAX_DATA_COUNT) {
            chartViewPI.removeData( null, 1 );
          }
        }
        chartViewPI.addData( SENSOR_RAW_PREFIX + index, xStr, sensorRaw );
        chartViewPI.addData( SENSOR_PREFIX + index, xStr, sensorWert );
      }

      updatePID( values[4], values[5], values[6] );
    }
  }

  @Override
  public void processState( State state ) {
    // do nothing
  }

  @Override
  public void processWifiState( WifiState newState ) {
    // do nothing
  }
}
