package de.pidata.rail.client.motor;

import de.pidata.gui.chart.ChartViewPI;
import de.pidata.log.Logger;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.rail.model.MotorAction;
import de.pidata.rail.model.Point;

import java.util.ArrayList;
import java.util.List;

public class MotorCalibrationData {

  private ChartViewPI chartViewPI;
  private String seriesSensor;
  private String seriesOldPoints;
  private String seriesPoints;

  private List<Point> sensorPointList = new ArrayList<>();
  private List<Point> sensorOldPointList = new ArrayList<>();
  private long adjustDistSum = 0;
  private long adjustSpeedSum = 0;
  private int sensorMin = Integer.MAX_VALUE;
  private int sensorMax = 0;
  private int motorIndex;

  public MotorCalibrationData( ChartViewPI chartViewPI, int motorIndex ) {
    this.chartViewPI = chartViewPI;
    this.motorIndex = motorIndex;
    this.seriesSensor = "SENSOR_"+motorIndex;
    this.seriesOldPoints = "POINTS_ALT_"+motorIndex;
    this.seriesPoints = "POINTS_"+motorIndex;
    chartViewPI.addSeries( seriesOldPoints );
    chartViewPI.addSeries( seriesSensor );
    chartViewPI.addSeries( seriesPoints );
  }

  public void addSensorPoint( Point point ) {
    sensorOldPointList.add( point );
    chartViewPI.addData( seriesOldPoints, point.getXInt(), point.getYInt() );
  }

  public void addData( int sensorValue, int speed, int adjustLoopCount ) {
    chartViewPI.addData( seriesSensor, sensorValue, speed );
    adjustDistSum += sensorValue;
    adjustSpeedSum += speed;
    if (sensorMin > sensorValue) {
      sensorMin = sensorValue;
    }
    if (sensorMax < sensorValue) {
      sensorMax = sensorValue;
    }
    if (adjustLoopCount >= MotorCalibrationDelegate.ADJUST_LOOP_MAX) {
      int sensorAvg = (int) (adjustDistSum / MotorCalibrationDelegate.ADJUST_LOOP_MAX);
      int speedAvg = (int) (adjustSpeedSum / MotorCalibrationDelegate.ADJUST_LOOP_MAX);
      sensorPointList.add( new Point( sensorAvg, speedAvg ) );
      chartViewPI.addData( seriesPoints, sensorAvg, speedAvg );
      Logger.info( "--- Avg. for " + speedAvg + ": " + sensorAvg );
      adjustDistSum = 0;
      adjustSpeedSum = 0;
    }
  }

  public void replaceSensorPoints( MotorAction motorAction ) {
    motorAction.removeAll( MotorAction.ID_PT );
    // Inverted order: Smallest sensor values first
    for (int i = sensorPointList.size()-1; i >= 0; i--) {
      Point point = sensorPointList.get( i );
      motorAction.addPt( point );
    }
  }
}
