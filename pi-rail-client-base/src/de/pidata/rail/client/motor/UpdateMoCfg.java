package de.pidata.rail.client.motor;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.controller.base.IntegerController;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.MotorAction;
import de.pidata.rail.model.MotorMode;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.ServiceException;

public class UpdateMoCfg extends SaveCfgOperation<MotorSetupDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, MotorSetupDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    Locomotive locomotive = delegate.getLocomotive();
    Cfg cfg = locomotive.getConfig();
    char motorModeValue = delegate.getMotorModeValue();
    for (MotorAction motorAction : locomotive.getConfig().motorIter()) {
      MotorMode motorMode = motorAction.getMoMode( motorModeValue );
      IntegerController intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KP );
      motorMode.setKP( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KI );
      motorMode.setKI( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_KD );
      motorMode.setKD( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_MIN );
      motorAction.setMinVal( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_MAX );
      motorAction.setMaxVal( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_SLOW );
      motorMode.setSlow( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_DECEL );
      motorMode.setDecel( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_BRK_FAK );
      motorMode.setBrkFak( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_DELAY );
      motorMode.setDelay( intCtrl.getIntegerValue() );
      intCtrl = (IntegerController) ctrlGroup.getController( MotorSetupDelegate.ID_EDIT_ACCEL );
      motorMode.setAccel( intCtrl.getIntegerValue() );
    }

    if (uploadCfg( locomotive.getAddress().getInetAddress(), cfg, ctrlGroup.getDialogController() )) {
      ctrlGroup.getDialogController().close( true );
    }
  }
}
