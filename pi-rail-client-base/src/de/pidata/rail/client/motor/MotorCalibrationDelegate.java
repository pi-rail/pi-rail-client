package de.pidata.rail.client.motor;

import de.pidata.gui.chart.ChartController;
import de.pidata.gui.chart.ChartViewPI;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TextController;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.comm.WifiState;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.MeasureCfg;
import de.pidata.rail.track.RailroadCfg;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class MotorCalibrationDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, DataListener {

  public static final int ADJUST_LOOP_MAX = 3;
  public static final int SPEED_MAX = 80;
  public static final int SPEED_INC = 10;

  private ChartController chartController;
  private Locomotive locomotive;
  private HashMap<QName,MotorCalibrationData> calibrationMotors = new HashMap<>();
  private DialogController dlgCtrl;

  private boolean oscillate = false; // measure by driving oscillating or in a circle
  private boolean measureForward = true;
  private boolean adjustSensor = false;
  private List<MeasureCfg> measureCfgList = new ArrayList<>();
  private MeasureCfg currentMeasure = null;
  private DecimalObject adjustSpeed;
  private long timeStartEvent = 0;
  private long lastEventTime = -1;
  private int adjustLoopCount;

  private OutputStream csvOut;
  private PrintWriter csvPrint;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    chartController = (ChartController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "sensorChart" ) );
    locomotive = PiRail.getInstance().getModelRailway().getLoco( parameterList.getDeviceId() );
    RailFunction modeFunction = locomotive.getModeFunction();
    if (modeFunction != null) {
      PiRail.getInstance().sendSetCommand( modeFunction, '0', 0, null );
    }
    try {
      Storage appDataDir = SystemManager.getInstance().getStorage( SystemManager.STORAGE_APPDATA, null );
      csvOut = appDataDir.write( "Motor-Setup.csv", true, false );
      csvPrint = new PrintWriter( csvOut );
      csvPrint.println( Data.HEADER_MO_DATA );
    }
    catch (Exception ex) {
      Logger.error( "Error opening Motor-Setup.csv", ex );
      StreamHelper.close( csvOut );
      csvOut = null;
      csvPrint = null;
    }
    return locomotive;
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    int motorIndex = 0;
    for (MotorAction moCfg : locomotive.getConfig().motorIter()) {
      if ((moCfg.getGroup() == null) || (moCfg.getGroup() == ModelRailway.GROUP_HIDDEN)) {
        MotorCalibrationData calData = new MotorCalibrationData( (ChartViewPI) chartController.getView(), motorIndex);
        for (Point point : moCfg.getPts()) {
          calData.addSensorPoint( point );
        }
        calibrationMotors.put( moCfg.getId(), calData );
        PiRail.getInstance().sendSetCmd( locomotive, moCfg.getId(), MotorAction.ID_SENDCSV, Integer.valueOf( 1 ) );
        motorIndex++;
      }
    }
    locomotive.setDataListener( this );

    String measureTrack = SystemManager.getInstance().getGlossaryString( "measureTrack" );
    StringBuilder measureIdText = new StringBuilder( measureTrack + ": ");
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    RailroadCfg railroadCfg = modelRailway.getRailroadCfg();
    if (railroadCfg == null) {
      measureIdText.append( SystemManager.getInstance().getLocalizedMessage( "motorCalibrationNoRailway", null, null ) );
    }
    else {
      for (MeasureCfg measureCfg : railroadCfg.measureCfgIter()) {
        if ((measureCfg.getStartMsgID() != null) && (measureCfg.getEndMsgID() != null)) {
          // append e.g. "A01 -> A02"
          int startIndex = measureCfg.getStartMsgIndex();
          int endIndex = measureCfg.getEndMsgIndex();
          RailAction railMessage;
          if (startIndex < endIndex) {
            railMessage = modelRailway.getRailMessage( measureCfg.getEndMsgID() );
          }
          else {
            railMessage = modelRailway.getRailMessage( measureCfg.getStartMsgID() );
          }
          int dist = 0;
          if (railMessage != null) {
            TrackPos trackPos = railMessage.getTrackPos();
            if (trackPos != null) {
              dist = trackPos.getDistInt();
            }
            if ((dist == 0) && (railMessage instanceof RailTimer)) {
              TimerAction timerAction = (TimerAction) railMessage.getAction();
              dist = timerAction.getDistInt();
            }
          }
          if (dist > 0) {
            measureCfg.setDistance( dist );
            measureIdText.append( measureCfg.getStartMsgID().getName() );
            measureIdText.append( " -> " );
            measureIdText.append( measureCfg.getEndMsgID().getName() );
            measureIdText.append( " (" ).append( dist ).append( " cm) " );
            this.measureCfgList.add( measureCfg );
          }
        }
      }
    }
    TextController msgIDCtrl = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "msgIDs" ) );
    if (msgIDCtrl != null) {
      msgIDCtrl.setValue( measureIdText.toString() );
    }
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    locomotive.setDataListener( null );
    if (csvPrint != null) {
      csvPrint.flush();
      csvPrint.close();
    }
    StreamHelper.close( csvOut );
    locomotive.setTargetSpeedPercent( DecimalObject.ZERO );
    for (QName motID : calibrationMotors.keySet()) {
      PiRail.getInstance().sendSetCmd( locomotive, motID, MotorAction.ID_SENDCSV, Integer.valueOf( 0 ) );
    }
    calibrationMotors.clear();
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  private void updatePos( String posStr, State locoState, long eventTime ) {
    if (adjustSensor && (currentMeasure != null)) {
      String endPosStr;
      if (measureForward) {
        endPosStr = currentMeasure.getEndMsgID().getName();
      }
      else {
        endPosStr = currentMeasure.getStartMsgID().getName();
      }
      if (posStr.equals( endPosStr )) {
        adjustLoopCount++;
        StringBuilder msg = new StringBuilder( "Adjust End");
        int dist = currentMeasure.getDistance();
        long duration = eventTime - timeStartEvent;
        for (QName motorID : calibrationMotors.keySet()) {
          MotorState motorState = locoState.getSp( motorID );
          if (motorState != null) {
            int sensorValue = motorState.getAvgInt();
            int motorSetPoint = motorState.getCurIInt();
            int speed = (int) ((double) dist * 10000 / duration);
            msg.append( "; " ).append( motorID.getName() ).append(" sensor=" ).append( sensorValue ).append( ", motorSetPoint=" ).append( motorSetPoint );
            msg.append( ", speed=" ).append( speed ).append( ", duration=" ).append( duration ).append( ", dist=" ).append( dist );
            MotorCalibrationData calData = calibrationMotors.get( motorID );
            calData.addData( sensorValue, speed, adjustLoopCount );
          }
        }
        Logger.info( msg.toString() );
        currentMeasure = null;

        //--- Change speed if adjustLoopCount has reached ADJUST_LOOP_MAX
        if (adjustLoopCount >= ADJUST_LOOP_MAX) {
          int curSpeed = adjustSpeed.intValue();
          if (curSpeed < SPEED_MAX) {
            adjustSpeed = adjustSpeed.add( new DecimalObject( SPEED_INC, 0 ) );
            getLocomotive().setTargetSpeedPercent( adjustSpeed );
            adjustLoopCount = 0;
          }
          else {
            adjustSensor = false;
            getLocomotive().setTargetSpeedPercent( DecimalObject.ZERO );
          }
        }

        //--- Change loco dir if measure mode is oscillating
        if (oscillate) {
          try {
            Thread.sleep( 2000 );
          }
          catch (InterruptedException e) {
            // do nothing
          }
          Logger.info( "Loco change dir" );
          char targetDir = locomotive.getTargetDirChar();
          if (targetDir == MotorState.CHAR_FORWARD) {
            locomotive.setTargetDir( MotorState.DIR_BACK );
          }
          else {
            locomotive.setTargetDir( MotorState.DIR_FORWARD );
          }
        }
      }
    }
    else {
      currentMeasure = null;
      for (MeasureCfg measureCfg : measureCfgList) {
        if (posStr.equals( measureCfg.getStartMsgID().getName() )) {
          Logger.info( "Adjust start forward" );
          measureForward = true;
          currentMeasure = measureCfg;
          timeStartEvent = eventTime;
        }
        else if (posStr.equals( measureCfg.getEndMsgID().getName() )) {
          Logger.info( "Adjust start back" );
          measureForward = false;
          currentMeasure = measureCfg;
          timeStartEvent = eventTime;
        }
      }
    }
  }

  @Override
  public void processData( Csv csvData ) {
    if ((csvPrint != null) && (csvData.getT() == CsvType.moData)) {
      csvPrint.println( csvData.getDat() );
    }
  }

  @Override
  public void processState( State state ) {
    Ln ln = state.getLn();
    if (ln != null) {
      long eventTime = ln.getMsLong();
      if (eventTime != lastEventTime) {
        QName posID = ln.getPosID();
        if (posID != null) {
          String posStr = ln.getPosID().getName();
          updatePos( posStr, state, eventTime );
        }
        lastEventTime = eventTime;
      }
    }
  }

  @Override
  public void processWifiState( WifiState newState ) {
    // do nothing
  }

  public void adjustSensor( boolean oscillate ) {
    ChartViewPI chartViewPI = (ChartViewPI) chartController.getView();
    chartViewPI.clearData();
    this.oscillate = oscillate;
    adjustSensor = true;
    adjustSpeed = getLocomotive().getTargetSpeedPercent();
    adjustLoopCount = 0;
  }

  public void replaceSensorPoints( MotorAction motorAction ) {
    MotorCalibrationData caData = calibrationMotors.get( motorAction.getId() );
    if (caData != null) {
      caData.replaceSensorPoints( motorAction );
    }
  }
}
