package de.pidata.rail.client.motor;

import de.pidata.gui.controller.base.Controller;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.EnumAction;
import de.pidata.rail.model.MotorAction;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailFunction;
import de.pidata.service.base.ServiceException;

public class SaveCalibrationCfg extends SaveCfgOperation<MotorCalibrationDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, MotorCalibrationDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive locomotive = delegate.getLocomotive();
    Cfg cfg = locomotive.getConfig();
    for (MotorAction motorAction : locomotive.getConfig().motorIter()) {
      if ((motorAction.getGroup() == null) || (motorAction.getGroup() == ModelRailway.GROUP_HIDDEN)){
        delegate.replaceSensorPoints( motorAction );
      }
    }
    if (uploadCfg( locomotive.getAddress().getInetAddress(), cfg, sourceCtrl.getDialogController() )) {
      sourceCtrl.getDialogController().close( true );
    }
  }
}
