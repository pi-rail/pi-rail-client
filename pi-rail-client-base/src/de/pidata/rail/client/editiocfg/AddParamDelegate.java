package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Param;
import de.pidata.rail.railway.DefaultParameter;
import de.pidata.rail.railway.ParameterSelection;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddParamDelegate implements DialogControllerDelegate<ParameterList, ParameterPickerParamList>, EventListener, FlagListener, DialogValidator, TextFieldListener {


  public static final String SIFA_TIME = "sifaTime";
  public static final String DCC_ADDR = "dccAddr";
  public static final String DCC_F_PLUS = "dccF#+";
  public static final String DCC_F_MINUS = "dccF#-";
  public static final String DCC_REGEX = "dccF([0-5]?[0-9]|6[0-3])(\\+|-)";
  
  private String customParamName;
  private DialogController dlgCtrl;
  private ListController paramListCtrl;
  private DefaultParameter customParameter;
  private ParameterSelection parameterSelection;
  private FlagController textFlagCtrl;
  private FlagController numberFlagCtrl;
  private TextController paramNameCtrl;
  private TextController paramValueCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ParameterList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.dlgCtrl.setValidator( this );

    paramListCtrl = (ListController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "param_list") );
    paramListCtrl.addListener( this );

    textFlagCtrl = (FlagController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "text_check") );
    textFlagCtrl.addListener( this );

    numberFlagCtrl = (FlagController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "number_check") );
    numberFlagCtrl.addListener( this );

    paramNameCtrl = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "param_name_txt") );
    paramNameCtrl.addListener( this );

    paramValueCtrl = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "param_value_txt" ) );
    paramValueCtrl.addListener( this );

    parameterSelection = new ParameterSelection();
    parameterSelection.addParameter( new DefaultParameter( SIFA_TIME ) );
    parameterSelection.addParameter( new DefaultParameter( DCC_ADDR ) );
    parameterSelection.addParameter( new DefaultParameter( DCC_F_PLUS ) );
    parameterSelection.addParameter( new DefaultParameter( DCC_F_MINUS ) );
    customParamName = SystemManager.getInstance().getGlossaryString( "customParameter" );
    customParameter = new DefaultParameter( customParamName );
    parameterSelection.addParameter( customParameter );

    return parameterSelection;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    paramListCtrl.selectRow( customParameter );
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterPickerParamList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      return new ParameterPickerParamList(parameterSelection.getParamName(), parameterSelection.getParamValue(), parameterSelection.getIsNumber());
    }
    return null;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (eventSender.getOwner() == paramListCtrl && newValue instanceof DefaultParameter) {
      String selectedParamName = ((DefaultParameter) newValue).getName();
      paramNameCtrl.setValue( selectedParamName );

      if (selectedParamName.equals( DCC_F_PLUS ) || selectedParamName.equals( DCC_F_MINUS )) {
        numberFlagCtrl.setValue( Boolean.FALSE );
        textFlagCtrl.setValue( Boolean.TRUE );
      }
      else if (selectedParamName.equals( DCC_ADDR )) {
        numberFlagCtrl.setValue( Boolean.TRUE );
        textFlagCtrl.setValue( Boolean.FALSE );
      }
      else if (selectedParamName.equals( SIFA_TIME )) {
        numberFlagCtrl.setValue( Boolean.TRUE );
        textFlagCtrl.setValue( Boolean.FALSE );
      }
      else if (selectedParamName.equals( customParamName )) {
        numberFlagCtrl.setValue( Boolean.FALSE );
        textFlagCtrl.setValue( Boolean.FALSE );
      }
    }
  }

  @Override
  public void flagChanged( FlagController source, Boolean newValue ) {
    parameterSelection.setIsNumber( numberFlagCtrl.getBooleanValue());
  }

  @Override
  public boolean validate( DialogController dlgCtrl ) {

    StringBuilder messages = new StringBuilder();

    String paramName = parameterSelection.getParamName();
    if (Helper.isNullOrEmpty( paramName )) {
      messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamNoName_MSG", null, null ) );
      messages.append( System.lineSeparator() ).append( System.lineSeparator() );
    }
    else {
      String msg = Param.checkID( paramName );
      if (msg == null) {
        if (paramName.startsWith( "dccF" )) {
          Pattern pattern = Pattern.compile( DCC_REGEX, Pattern.CASE_INSENSITIVE );
          Matcher matcher = pattern.matcher( paramName );

          if (!matcher.matches()) {
            messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamDCCRange_MSG", null, null ) );
            messages.append( System.lineSeparator() ).append( System.lineSeparator() );
          }
          else {
            if (!textFlagCtrl.getBoolValue()) {
              Properties params = new Properties();
              params.put( "name", paramName );
              messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamTextReq_MSG", null, params ) );
              messages.append( System.lineSeparator() ).append( System.lineSeparator() );
            }
          }
        }
      }
      else {
        Properties params = new Properties();
        params.put( "name", paramName );
        String text = SystemManager.getInstance().getLocalizedMessage( "addParamInvalidName_TXT", null, params );
        messages.append( "- " + text + "\n" + msg );
        messages.append( System.lineSeparator() ).append( System.lineSeparator() );
      }

      if (paramName.equals( DCC_ADDR ) && !numberFlagCtrl.getBoolValue()) {
        Properties params = new Properties();
        params.put( "name", DCC_ADDR );
        messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamNumberReq_MSG", null, params ) );
        messages.append( System.lineSeparator() ).append( System.lineSeparator() );
      }
      if (paramName.equals( SIFA_TIME ) && !numberFlagCtrl.getBoolValue()) {
        Properties params = new Properties();
        params.put( "name", SIFA_TIME );
        messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamNumberReq_MSG", null, params ) );
        messages.append( System.lineSeparator() ).append( System.lineSeparator() );
      }
    }

    if (!numberFlagCtrl.getBoolValue() && !textFlagCtrl.getBoolValue()) {
      messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamTextOrNum_MSG", null, null ) );
      messages.append( System.lineSeparator() ).append( System.lineSeparator() );
    }

    // If a value is given check if it matches the selected type.
    String paramValue = parameterSelection.getParamValue();
    if (!(numberFlagCtrl.getBoolValue() && textFlagCtrl.getBoolValue()) && Helper.isNotNullAndNotEmpty( paramValue ) ) {
      // number selected as type
      if (numberFlagCtrl.getBoolValue()) {
        try {
          Integer.parseInt( paramValue );
        }
        catch (NumberFormatException nfe) {
          Properties params = new Properties();
          params.put( "value", paramValue );
          messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamCheckNumberReq_MSG", null, params ) );
          messages.append( System.lineSeparator() ).append( System.lineSeparator() );
        }
      }
      // text selected as type
      if (textFlagCtrl.getBoolValue()) {
        if (paramValue.length() > 1) {
          Properties params = new Properties();
          params.put( "value", paramValue );
          messages.append( SystemManager.getInstance().getLocalizedMessage( "addParamCheckCharReq", null, params ) );
          messages.append( System.lineSeparator() ).append( System.lineSeparator() );
        }
      }
    }

    if (messages.length() > 0) {
      String title = SystemManager.getInstance().getLocalizedMessage( "addParamDoComplete_H", null, null );
      dlgCtrl.showMessage( title, messages.toString() );
      return false;
    }

    return true;
  }

  @Override
  public void textChanged( TextController source, String oldValue, String newValue ) {
    if (source == paramNameCtrl) {
      parameterSelection.setParamName( newValue );
    }
    if (source == paramValueCtrl) {
      parameterSelection.setParamValue( newValue );
    }
  }
}
