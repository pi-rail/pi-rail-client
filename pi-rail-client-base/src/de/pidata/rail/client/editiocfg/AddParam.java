package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Action;
import de.pidata.rail.model.ItemConn;
import de.pidata.rail.model.Param;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddParam extends GuiDelegateOperation<EditCfgDelegate> {

  private ItemConn itemConn;
  private QName eventID;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    this.itemConn = null;
    TreeController actionTree = delegate.getActionTree();
    TreeNodePI selectedNode = actionTree.getSelectedNode();
    if (selectedNode != null) {
      Model model = selectedNode.getNodeModel();
      ItemConn selectedItemConn = null;
      if (model instanceof ItemConn) {
        selectedItemConn = (ItemConn) model;
      }
      else if (model instanceof Action) {
        selectedItemConn = (ItemConn) selectedNode.getParentNode().getNodeModel();
      }
      if (selectedItemConn != null) {
        this.itemConn = selectedItemConn;
        this.eventID = eventID;
        String title = SystemManager.getInstance().getLocalizedMessage( "addParamAdd_H", null, null );
        openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "action_add_param" ),  title, AbstractParameterList.EMPTY );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof ParameterPickerParamList) {
      if (resultOK && (itemConn != null)) {
        ParameterPickerParamList pickerParamList = (ParameterPickerParamList) resultList;
        String paramName = pickerParamList.getSelectedParamName();
        String paramValue = pickerParamList.getSelectedParamValue();
        QName paramID = PiRailFactory.NAMESPACE.getQName( paramName );

        if (itemConn.getParam( paramID ) == null) {
          if (pickerParamList.getParameterIsNumber().booleanValue()) {
            if (Helper.isNotNullAndNotEmpty(paramValue)) {
              itemConn.addParamInt( paramID, Integer.parseInt( paramValue ) );
            }
            else {
              itemConn.addParamInt( paramID, 0 );
            }
          }
          else {
            if (Helper.isNotNullAndNotEmpty(paramValue)) {
              itemConn.addParamChar( paramID, paramValue.charAt( 0 ) );
            }
            else {
              itemConn.addParamChar( paramID, ' ' );
            }
          }
        }
        else {
          Properties params = new Properties();
          params.put( "name", paramName );
          String title = SystemManager.getInstance().getLocalizedMessage( "addParamDuplicate_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addParamDuplicate_TXT", null, params );
          parentDlgCtrl.showMessage( title, text );
        }
      }
    }
    itemConn=null;
  }
}

