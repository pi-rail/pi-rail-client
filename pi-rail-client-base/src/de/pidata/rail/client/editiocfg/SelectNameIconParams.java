package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.model.FunctionType;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectNameIconParams extends AbstractParameterList {

  public static final QName SELECTED_ID = ModelRailway.NAMESPACE.getQName( "selectedID" );
  public static final QName FUNCTION_TYPE = ModelRailway.NAMESPACE.getQName( "functionType" );

  public SelectNameIconParams() {
    super( ParameterType.StringType, FUNCTION_TYPE, ParameterType.QNameType, SELECTED_ID );
  }

  public SelectNameIconParams( FunctionType functionType, QName selectedID ) {
    this();
    if (functionType != null) {
      setString( FUNCTION_TYPE, functionType.name() );
    }
    setQName( SELECTED_ID, selectedID );
  }

  public FunctionType getFunctionType() {
    String fktType = getString( FUNCTION_TYPE );
    if (fktType == null) {
      return null;
    }
    else {
      return FunctionType.fromString( fktType );
    }
  }
  public QName getSelectedID() {
    return getQName( SELECTED_ID );
  }
}
