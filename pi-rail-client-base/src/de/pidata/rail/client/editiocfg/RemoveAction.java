package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Action;
import de.pidata.rail.model.ItemConn;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class RemoveAction extends GuiDelegateOperation<EditCfgDelegate> {

  private Model removeModel;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController actionTree = (TreeController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "actionTree" ) );
    TreeNodePI selectedNode = actionTree.getSelectedNode();
    if (selectedNode != null) {
      Model selectedModel = selectedNode.getNodeModel();
      if (selectedModel instanceof Action) {
        removeModel = selectedModel;
        QName id = ((Action) removeModel).getId();
        Properties params = new Properties();
        params.put( "type", removeModel.type().name().getName() );
        params.put( "name", id.getName() );
        String title = SystemManager.getInstance().getLocalizedMessage( "removeActionConfirm_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "removeActionConfirm_TXT", null, params );
        String btnYes = SystemManager.getInstance().getGlossaryString( "yes" );
        String btnNo = SystemManager.getInstance().getGlossaryString( "no" );
        showQuestion( sourceCtrl, title, text,btnYes, btnNo, null );
      }
      else if (selectedModel instanceof ItemConn) {
        removeModel = selectedModel;
        QName id = ((ItemConn) removeModel).getId();
        Properties params = new Properties();
        params.put( "name", id.getName() );
        String title = SystemManager.getInstance().getLocalizedMessage( "removeActionProduct_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "removeActionProduct_TXT", null, params );
        String btnYes = SystemManager.getInstance().getGlossaryString( "yes" );
        String btnNo = SystemManager.getInstance().getGlossaryString( "no" );
        showQuestion( sourceCtrl, title, text,btnYes, btnNo, null );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
       if (((QuestionBoxResult) resultList).getSelectedButton() == QuestionBoxResult.YES_OK) {
         if (removeModel instanceof Action) {
           removeModel.getParent( false ).remove( removeModel.getParentRelationID(), removeModel );
         }
         else if (removeModel instanceof ItemConn) {
           EditCfgDelegate delegate = (EditCfgDelegate) parentDlgCtrl.getDelegate();
           delegate.deleteProduct( (ItemConn) removeModel );
         }
       }
      }
    }
    removeModel = null;
  }
}
