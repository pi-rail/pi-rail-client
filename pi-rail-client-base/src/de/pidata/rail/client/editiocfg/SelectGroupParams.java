package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectGroupParams extends AbstractParameterList {

  public static final QName SELECTED_GROUP = ModelRailway.NAMESPACE.getQName( "selectedGroup" );

  public SelectGroupParams() {
    super( ParameterType.QNameType, SELECTED_GROUP );
  }

  public SelectGroupParams( QName selectedID ) {
    super( ParameterType.QNameType, SELECTED_GROUP );
    setQName( SELECTED_GROUP, selectedID );
  }

  public QName getSelectedID() {
    return getQName( SELECTED_GROUP );
  }
}

