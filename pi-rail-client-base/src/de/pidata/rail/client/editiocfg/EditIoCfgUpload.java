package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class EditIoCfgUpload extends SaveCfgOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, DialogControllerDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditCfgUI uiModel = (EditCfgUI) dataContext;
    if (uiModel == null) {
      Logger.info( "Could not upload SwitchBox configuration, UI Model is null" );
      return;
    }
    Cfg cfg = uiModel.getCfg();
    if (cfg == null) {
      Logger.info( "Could not upload SwitchBox configuration, Cfg is null" );
      return;
    }
    
    String name = uiModel.getDeviceName();
    if (Helper.isNullOrEmpty( name )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadNameEmpty_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadNameEmpty_TXT", null, null );
      showMessage( source, title, text );
      return;
    }
    if (name.equals( "Leere-Config" )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadDefaultName_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadDefaultName_TXT", null, null );
      showMessage( source, title, text );
      return;
    }
    QName cfgID = cfg.getId();
    if ((cfgID == null) || (!cfgID.getName().equals( name ))) {
      QName newID = PiRailFactory.NAMESPACE.getQName( name );
      RailDevice railDevice = PiRail.getInstance().getModelRailway().getRailDevice( newID );
      if ((railDevice != null) && !uiModel.getInetAddress().equals( railDevice.getAddress().getInetAddress() ) ) {
        Properties params = new Properties();
        params.put( "deviceType", railDevice.getType() );
        params.put( "ipAddr", railDevice.getAddress().getInetAddress() );
        String title = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadDupName_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "editIoCfgUploadDupName_TXT", null, params );
        showMessage( source, title, text );
        return;
      }
      cfg.setId( newID );
    }

    source.getDialogController().getDialogComp().showBusy( true );
    new Thread( new Runnable() {
      @Override
      public void run() {
        if (uploadCfg( uiModel.getInetAddress(), cfg, source.getDialogController() )) {
          source.getDialogController().close( true );
        }
        source.getDialogController().getDialogComp().showBusy( false );
      }
    } ).start();
  }
}
