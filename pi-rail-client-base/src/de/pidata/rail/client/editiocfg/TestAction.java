package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ServiceException;

public class TestAction extends GuiDelegateOperation<EditCfgDelegate> {

  public static final QName ID_IO_CFG_TREE = NAMESPACE.getQName( "ioCfgTree" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController ioCfgTree = (TreeController) sourceCtrl.getControllerGroup().getController( ID_IO_CFG_TREE );
    TreeNodePI selectedNode = ioCfgTree.getSelectedNode();
    if (selectedNode != null) {
      Model selectedModel = selectedNode.getNodeModel();
      if (selectedModel instanceof OutCfg) {
        sendTestCmd( delegate, (OutCfg) selectedModel, PiRailFactory.NAMESPACE.getQName( "test" ) );
      }
      else if (selectedModel instanceof PortCfg) {
        sendTestCmd( delegate, (PortCfg) selectedModel, PiRailFactory.NAMESPACE.getQName( "test" ) );
      }
    }
  }

  private void sendTestCmd( EditCfgDelegate delegate, OutCfg outCfg, QName value ) {
    Integer pin = outCfg.getPin();
    Cmd cmd = new Cmd();
    SetCmd tstCmd = new SetCmd();
    tstCmd.setId( IoCfg.ID_OUTCFG );
    tstCmd.setVarID( outCfg.getId() );
    tstCmd.setIntVal( pin ); // only for compatibility to older CTC firmware
    cmd.setTst( tstCmd );
    RailDevice railDevice = PiRail.getInstance().getModelRailway().getRailDevice( delegate.getIpAddress() );
    PiRail.getInstance().sendCmd( railDevice, cmd );
  }

  private void sendTestCmd( EditCfgDelegate delegate, PortCfg portCfg, QName value ) {
    Cmd cmd = new Cmd();
    SetCmd tstCmd = new SetCmd();
    tstCmd.setId( portCfg.getId() );
    tstCmd.setVarID( value );
    tstCmd.setIntVal( Integer.valueOf( 0 ) );
    cmd.setTst( tstCmd );
    RailDevice railDevice = PiRail.getInstance().getModelRailway().getRailDevice( delegate.getIpAddress() );
    PiRail.getInstance().sendCmd( railDevice, cmd );
  }
}
