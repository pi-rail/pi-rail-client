package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class ParameterPickerParamList extends AbstractParameterList {

  public static final QName SELECTED_PARAM_NAME = ModelRailway.NAMESPACE.getQName( "selectedParamName" );
  public static final QName SELECTED_PARAM_VALUE = ModelRailway.NAMESPACE.getQName( "selectedParamValue" );
  public static final QName PARAMETER_IS_NUMBER = ModelRailway.NAMESPACE.getQName( "isNumber" );


  public ParameterPickerParamList() {
    super( ParameterType.StringType, SELECTED_PARAM_NAME, ParameterType.StringType, SELECTED_PARAM_VALUE, ParameterType.BooleanType, PARAMETER_IS_NUMBER );
  }

  public ParameterPickerParamList(String paramName, Boolean isNumber) {
    this();
    setString( SELECTED_PARAM_NAME, paramName );
    setBoolean( PARAMETER_IS_NUMBER, isNumber );
  }

  public ParameterPickerParamList(String paramName, String paramValue, Boolean isNumber) {
    this();
    setString( SELECTED_PARAM_NAME, paramName );
    setString( SELECTED_PARAM_VALUE, paramValue );
    setBoolean( PARAMETER_IS_NUMBER, isNumber );
  }


  public String getSelectedParamName() {
    return getString( SELECTED_PARAM_NAME );
  }
  public String getSelectedParamValue() {
    return getString( SELECTED_PARAM_VALUE );
  }

  public Boolean getParameterIsNumber() {
    return getBoolean( PARAMETER_IS_NUMBER );
  }

}
