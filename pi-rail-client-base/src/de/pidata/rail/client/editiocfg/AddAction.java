package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.KeyAndValue;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class AddAction extends GuiOperation {

  public static final QName ADD_TRIGGER = NAMESPACE.getQName( "addTrigger" );
  public static final QName ADD_ENUM = NAMESPACE.getQName( "addEnum" );
  public static final QName ADD_RANGE = NAMESPACE.getQName( "addRange" );
  public static final QName ADD_TIMER = NAMESPACE.getQName( "addTimer" );
  public static final QName ADD_SENSOR = NAMESPACE.getQName( "addSensor" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {

    EditCfgDelegate delegate = (EditCfgDelegate) sourceCtrl.getDialogController().getDelegate();

    SelectionController actionTypeCtrl = (SelectionController) sourceCtrl.getControllerGroup().getController( ControllerBuilder.NAMESPACE.getQName( "action_selection" ) );
    int selectionCnt = actionTypeCtrl.getSelection().selectedValueCount();
    if (selectionCnt == 0){
      String title = SystemManager.getInstance().getLocalizedMessage( "addActionNoSelection_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "addActionNoSelection_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }

    KeyAndValue row = (KeyAndValue) actionTypeCtrl.getSelectedRow( 0 );
    if (row == null){
      String title = SystemManager.getInstance().getLocalizedMessage( "addActionNoSelection_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "addActionNoSelection_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    QName selectedAction = row.getID();

    TreeController actionTree = (TreeController) sourceCtrl.getDialogController().getController( EditCfgDelegate.ID_ACTION_TREE );
    TreeNodePI treeNodePI = actionTree.getSelectedNode();
    if (treeNodePI == null) {
      return;
    }

    Model nodeModel = treeNodePI.getNodeModel();
    ItemConn itemConn = null;
    if (nodeModel instanceof Action) {
      itemConn = ((Action) nodeModel).getItemConn();
    }
    else if (nodeModel instanceof ItemConn) {
      itemConn = (ItemConn) nodeModel;
    }

    if (itemConn != null) {
      String name = itemConn.getId().getName();
      int pos = name.lastIndexOf( '-' );
      if (pos >= 0) {
        name = name.substring( 0, pos );
      }
      if (selectedAction == ADD_TRIGGER) {
        QName actionID = delegate.getUniqueActionID( name + "-trg" );
        TriggerAction triggerAction = new TriggerAction( actionID );
        triggerAction.setItemID( itemConn.getId() );
        triggerAction.setGroup( ModelRailway.GROUP_TRIGGER );
        delegate.addTrigger( triggerAction );
        actionTree.setSelectedModel( triggerAction );
      }
      else if (selectedAction == ADD_ENUM) {
        QName actionID = delegate.getUniqueActionID( name + "-fkt" );
        EnumAction enumAction = new EnumAction( actionID );
        enumAction.setItemID( itemConn.getId() );
        delegate.addFunction( enumAction );
        actionTree.setSelectedModel( enumAction );
      }
      else if (selectedAction == ADD_RANGE) {
        QName actionID = delegate.getUniqueActionID( name + "-rng" );
        RangeAction rangeAction = new RangeAction( actionID );
        rangeAction.setItemID( itemConn.getId() );
        OutPin firstPin = itemConn.getOutPin( null );
        if (firstPin == null) {
          Param firstParam = itemConn.getParam( null );
          if (firstParam != null) {
            rangeAction.setParamID( firstParam.getId() );
          }
        }
        else {
          rangeAction.setPinID( firstPin.getId() );
        }
        delegate.addRange( rangeAction );
        actionTree.setSelectedModel( rangeAction );
      }
      else if (selectedAction == ADD_TIMER) {
        QName actionID = delegate.getUniqueActionID( name + "-tim" );
        TimerAction timerAction = new TimerAction( actionID );
        timerAction.setItemID( itemConn.getId() );
        StateScript newScript = new StateScript();
        newScript.setId( NAMESPACE.getQName( "onTimer" ) );
        timerAction.setOnState( newScript );
        delegate.addTimer( timerAction );
        actionTree.setSelectedModel( timerAction );
      }
      else if (selectedAction == ADD_SENSOR) {
        QName actionID = delegate.getUniqueActionID( name + "-sen" );
        SensorAction sensorAction = new SensorAction( actionID );
        sensorAction.setItemID( itemConn.getId() );
        delegate.addSensor( sensorAction );
        actionTree.setSelectedModel( sensorAction );
      }
      ((PopupController)sourceCtrl.getControllerGroup().getParentGroup()).closePopup();
    }
  }
}
