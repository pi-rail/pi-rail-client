package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.guidef.StringTable;
import de.pidata.models.binding.ModelBinding;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

public class AddActionModule extends ModuleGroup {

  @Override
  public void init( QName name, ModuleController parent, ModelBinding groupBinding, boolean readOnly, ParameterList parameterList ) {
    super.init( name, parent, groupBinding, readOnly, parameterList );
    StringTable model = new StringTable( ModelRailway.NAMESPACE.getQName( "addActionTable" ) );
    SystemManager sysMan = SystemManager.getInstance();
    model.addStringEntry( AddAction.ADD_ENUM, sysMan.getGlossaryString( "function" ) );
    model.addStringEntry( AddAction.ADD_SENSOR, sysMan.getGlossaryString( "sensor" ) );
    model.addStringEntry( AddAction.ADD_TIMER, sysMan.getGlossaryString( "timer" ) );
    model.addStringEntry( AddAction.ADD_TRIGGER, sysMan.getGlossaryString( "trigger" ) );
    model.addStringEntry( AddAction.ADD_RANGE, sysMan.getGlossaryString( "valueControl" ) );
    setModel( model );
  }
}
