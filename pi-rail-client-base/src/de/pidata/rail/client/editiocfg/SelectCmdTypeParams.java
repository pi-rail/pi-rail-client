package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectCmdTypeParams extends AbstractParameterList {

  public static final QName SELECTED_ID = ModelRailway.NAMESPACE.getQName( "selectedID" );

  public SelectCmdTypeParams() {
    super( ParameterType.QNameType, SELECTED_ID );
  }

  public SelectCmdTypeParams( QName selectedID ) {
    super( ParameterType.QNameType, SELECTED_ID );
    setQName( SELECTED_ID, selectedID );
  }

  public QName getSelectedID() {
    return getQName( SELECTED_ID );
  }
}
