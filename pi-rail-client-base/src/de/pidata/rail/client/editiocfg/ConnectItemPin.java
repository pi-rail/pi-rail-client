package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class ConnectItemPin extends GuiDelegateOperation<EditCfgDelegate> {

  public static final QName ID_IO_CFG_TREE = NAMESPACE.getQName( "ioCfgTree" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController ioCfgTree = (TreeController) sourceCtrl.getControllerGroup().getController( ID_IO_CFG_TREE );
    TreeNodePI selectedNode = ioCfgTree.getSelectedNode();
    if (selectedNode != null) {
      Model selectedModel = selectedNode.getNodeModel();
      QName pinID = (QName) selectedModel.get( OutCfg.ID_PRODUCT_PIN );
      if (pinID == null) {
        if (selectedModel instanceof OutCfg) {
          OutPin outPin = delegate.getSelectedOutPin();
          if (outPin != null) {
            if (outPin.getIoID() == null) {
              OutCfg outCfg = (OutCfg) selectedModel;
              if (outCfg.getType() == outPin.getType()) {
                outCfg.setConnectedPin( outPin );
                outPin.setIoID( outCfg.getId() );
              }
              else {
                Properties params = new Properties();
                params.put( "pinName", outPin.getId().getName() );
                params.put( "pinType", outPin.getType() );
                params.put( "cfgType", outCfg.getType() );
                String title = SystemManager.getInstance().getLocalizedMessage( "connectItemPinWrongType_H", null, null );
                String text = SystemManager.getInstance().getLocalizedMessage( "connectItemPinWrongType_TXT", null, params );
                showMessage( sourceCtrl, title, text );
              }
            }
            else {
              Properties params = new Properties();
              params.put( "pinName", outPin.getId().getName() );
              params.put( "otherName", outPin.getIoID().getName() );
              String title = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_TXT", null, params );
              showMessage( sourceCtrl, title, text );
            }
          }
        }
        else if (selectedModel instanceof InCfg) {
          InPin inPin = delegate.getSelectedInPin();
          if (inPin != null) {
            if (inPin.getIoID() == null) {
              ((InCfg) selectedModel).setConnectedPin( inPin );
              inPin.setIoID( ((InCfg) selectedModel).getId() );
            }
            else {
              Properties params = new Properties();
              params.put( "pinName", inPin.getId().getName() );
              params.put( "otherName", inPin.getIoID().getName() );
              String title = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_TXT", null, params );
              showMessage( sourceCtrl, title, text );
            }
          }
        }
        else if (selectedModel instanceof PortCfg) {
          Port port = delegate.getSelectedPort();
          if (port != null) {
            if (port.getIoID() == null) {
              PortCfg portCfg = (PortCfg) selectedModel;
              if (portCfg.getType() == port.getType()) {
                portCfg.setConnectedPort( port );
                port.setIoID( portCfg.getId() );
              }
              else {
                Properties params = new Properties();
                params.put( "portName", port.getId().getName() );
                params.put( "portType", port.getType() );
                params.put( "cfgType", portCfg.getType() );
                String title = SystemManager.getInstance().getLocalizedMessage( "connectItemPinWrongPortType_H", null, null );
                String text = SystemManager.getInstance().getLocalizedMessage( "connectItemPinWrongPortType_TXT", null, params );
                showMessage( sourceCtrl, title, text );
              }
            }
            else {
              Properties params = new Properties();
              params.put( "pinName", port.getId().getName() );
              params.put( "otherName", port.getIoID().getName() );
              String title = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "connectItemPinAlreadyConn_TXT", null, params );
              showMessage( sourceCtrl, title, text );
            }
          }
        }
      }
      else {
        if (selectedModel instanceof OutCfg) {
          OutPin outPin = ((OutCfg) selectedModel).getConnectedPin();
          if (outPin != null) {
            outPin.setIoID( null );
          }
          ((OutCfg) selectedModel).setConnectedPin( null );
        }
        else if (selectedModel instanceof InCfg) {
          InPin inPin = ((InCfg) selectedModel).getConnectedPin();
          if (inPin != null) {
            inPin.setIoID( null );
          }
          ((InCfg) selectedModel).setConnectedPin( null );
        }
        else if (selectedModel instanceof PortCfg) {
          Port port = ((PortCfg) selectedModel).getConnectedPort();
          if (port != null) {
            port.setIoID( null );
          }
          ((PortCfg) selectedModel).setConnectedPort( null );
        }
      }
    }
  }
}
