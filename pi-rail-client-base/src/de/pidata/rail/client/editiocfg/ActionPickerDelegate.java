package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ActionGroup;
import de.pidata.rail.railway.RailAction;
import de.pidata.service.base.ParameterList;

public class ActionPickerDelegate implements DialogControllerDelegate<ActionPickerParamList,ActionPickerParamList> {

  private static final QName ID_ACTION_GROUP_TABLE = GuiBuilder.NAMESPACE.getQName( "actionGroupTable" );
  private static final QName ID_ACTION_TABLE = GuiBuilder.NAMESPACE.getQName( "actionTable" );

  private ActionPickerParamList paramList;
  private DialogController dlgCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ActionPickerParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.paramList = parameterList;
    return PiRail.getInstance().getModelRailway();
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    ActionGroup actionGroup = null;
    RailAction railAction = null;
    QName refActionID = paramList.getReferencedActionId();
    QName selGroup;
    if (refActionID == null) {
      selGroup = paramList.getSelectedGroupName();
    }
    else {
      railAction = PiRail.getInstance().getModelRailway().getRailAction( null, refActionID );
      if (railAction != null) {
        selGroup = railAction.getAction().getGroup();
      }
      else {
        selGroup = paramList.getSelectedGroupName();
      }
    }
    if (selGroup != null) {
      TableController groupTable = (TableController) dlgCtrl.getController( ID_ACTION_GROUP_TABLE );
      actionGroup = PiRail.getInstance().getModelRailway().getActionGroup( selGroup );
      if (actionGroup != null) {
        groupTable.selectRow( actionGroup );
      }
    }
    if ((actionGroup != null) && (railAction != null)) {
      RailAction action = actionGroup.getRailAction( refActionID );
      TableController actionTable = (TableController) dlgCtrl.getController( ID_ACTION_TABLE );
      actionTable.selectRow( action );
    }
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ActionPickerParamList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      TableController actionTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionTable" ) );
      FlagController deviceSpecific = (FlagController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "deviceSpecific" ) );
      RailAction railAction = (RailAction) actionTable.getSelectedRow( 0 );
      if (railAction != null) {
        if ((deviceSpecific != null) && deviceSpecific.getBoolValue()) {
          paramList.setReferencedDeviceId( railAction.getDeviceID() );
        }
        else {
          paramList.setReferencedDeviceId( null );
        }
        paramList.setReferencedActionId( railAction.getId() );
      }
    }
    return paramList;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
