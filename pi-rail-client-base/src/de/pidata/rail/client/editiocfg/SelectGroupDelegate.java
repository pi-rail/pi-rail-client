package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ActionGroup;
import de.pidata.service.base.ParameterList;

public class SelectGroupDelegate  implements DialogControllerDelegate<SelectGroupParams,SelectGroupParams> {

  public static final QName GROUPS_LIST = GuiBuilder.NAMESPACE.getQName( "groupsList" );
  
  private SelectGroupParams selectGroupParams;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectGroupParams parameterList ) throws Exception {
    this.selectGroupParams = parameterList;
    return PiRail.getInstance().getModelRailway();
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    ActionGroup actionGroup = null;
    QName selectedID = selectGroupParams.getSelectedID();
    if (selectedID != null) {
      actionGroup = PiRail.getInstance().getModelRailway().getActionGroup( selectedID );
    }
    ListController groupsList = (ListController) dlgCtrl.getController( GROUPS_LIST );
    groupsList.selectRow( actionGroup );
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectGroupParams dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      ListController groupsList = (ListController) dlgCtrl.getController( GROUPS_LIST );
      QName selectedID = (QName) groupsList.getSelectedRow( 0 ).key();
      return new SelectGroupParams( selectedID );
    }
    else {
      return new SelectGroupParams( null );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}

