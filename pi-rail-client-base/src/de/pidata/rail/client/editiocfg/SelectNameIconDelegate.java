package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.StringTable;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.FunctionType;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.service.base.ParameterList;

public class SelectNameIconDelegate implements DialogControllerDelegate<SelectNameIconParams,SelectNameIconParams> {

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectNameIconParams parameterList ) throws Exception {
    StringTable nameIconTable = new StringTable( GuiBuilder.NAMESPACE.getQName( "nameIconTable" ) );
    FunctionType functionType = parameterList.getFunctionType();
    if (functionType == FunctionType.Pulse) {
      //TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if ((functionType == FunctionType.Switch) || (functionType == FunctionType.Path)) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Drehschalter aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on" ), "Drehschalter an" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "A" ), "Buchstabe A in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "B" ), "Buchstabe B in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "C" ), "Buchstabe C in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "D" ), "Buchstabe D in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "E" ), "Buchstabe E in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "F" ), "Buchstabe F in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "G" ), "Buchstabe G in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "H" ), "Buchstabe H in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "J" ), "Buchstabe J in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "K" ), "Buchstabe K in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "L" ), "Buchstabe L in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "M" ), "Buchstabe M in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis1-li-Abfahrt" ), "Gleis-Tafel 1 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis1-li-Ankunft" ), "Gleis-Tafel 1 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis1-re-Abfahrt" ), "Gleis-Tafel 1 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis1-re-Ankunft" ), "Gleis-Tafel 1 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis2-li-Abfahrt" ), "Gleis-Tafel 2 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis2-li-Ankunft" ), "Gleis-Tafel 2 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis2-re-Abfahrt" ), "Gleis-Tafel 2 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis2-re-Ankunft" ), "Gleis-Tafel 2 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis3-li-Abfahrt" ), "Gleis-Tafel 3 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis3-li-Ankunft" ), "Gleis-Tafel 3 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis3-re-Abfahrt" ), "Gleis-Tafel 3 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis3-re-Ankunft" ), "Gleis-Tafel 3 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis4-li-Abfahrt" ), "Gleis-Tafel 4 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis4-li-Abfahrt" ), "Gleis-Tafel 4 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis4-re-Ankunft" ), "Gleis-Tafel 4 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis4-re-Ankunft" ), "Gleis-Tafel 4 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis5-li-Abfahrt" ), "Gleis-Tafel 5 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis5-li-Ankunft" ), "Gleis-Tafel 5 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis5-re-Abfahrt" ), "Gleis-Tafel 5 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis5-re-Ankunft" ), "Gleis-Tafel 5 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis6-li-Abfahrt" ), "Gleis-Tafel 6 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis6-li-Ankunft" ), "Gleis-Tafel 6 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis6-re-Abfahrt" ), "Gleis-Tafel 6 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis6-re-Ankunft" ), "Gleis-Tafel 6 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis7-li-Abfahrt" ), "Gleis-Tafel 7 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis7-li-Ankunft" ), "Gleis-Tafel 7 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis7-re-Abfahrt" ), "Gleis-Tafel 7 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis7-re-Ankunft" ), "Gleis-Tafel 7 mit Pfeil rechts Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis8-li-Abfahrt" ), "Gleis-Tafel 8 mit Pfeil links Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis8-li-Ankunft" ), "Gleis-Tafel 8 mit Pfeil links Ankunft" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis8-re-Abfahrt" ), "Gleis-Tafel 8 mit Pfeil rechts Abfahrt" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Gleis8-re-Ankunft" ), "Gleis-Tafel 8 mit Pfeil" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Zug1" ), "Text 'Zug' und 1 in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Zug2" ), "Text 'Zug' und 2 in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Zug3" ), "Text 'Zug' und 3 in weißem Kreis" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Zug4" ), "Text 'Zug' und 4 in weißem Kreis" );
    }
    else if (functionType == FunctionType.Toggle) {
      //TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if (functionType == FunctionType.Light) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "auto" ), "Licht Automatik" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "front" ), "Frontlicht" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "back" ), "Rücklicht" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "both" ), "Licht beide" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Lampe aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on" ), "Lampe an" );
    }
    else if (functionType == FunctionType.Direction) {
      //TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if (functionType == FunctionType.Mode) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "direct" ), "Motorsteuerung direct" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "PID" ), "Motorsteuerung PID" );
    }
    else if (functionType == FunctionType.Auto) {
      //TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if (functionType == FunctionType.Disconnect) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Entkuppler aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "back" ), "Entkuppeln hinten" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "front" ), "Entkuppeln vorne" );
    }
    else if (functionType == FunctionType.Sound) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Sound aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on" ), "Sound an" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on1" ), "Sound 1 an" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on2" ), "Sound 2 an" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on3" ), "Sound 3 an" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "on4" ), "Sound 4 an" );
    }
    else if (functionType == FunctionType.Job) {
      //TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if (functionType == FunctionType.Left) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "left" ), "Linksweiche links" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "straight" ), "Linksweiche gerade" );
    }
    else if (functionType == FunctionType.Right) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "right" ), "Rechtsweiche rechts" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "straight" ), "Rechtsweiche gerade" );
    }
    else if (functionType == FunctionType.Three) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "left" ), "Dreiwegweiche links" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "right" ), "Dreiwegweiche rechts" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "straight" ), "Dreiwegweiche gerade" );
    }
    else if (functionType == FunctionType.Cross) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "cross" ), "Kreuzungsweiche kreuzen" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "notcross" ), "Kreuzungsweiche abzweigen" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "hor" ), "Kreuzungsweiche horizontal" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "left" ), "Kreuzungsweiche links" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "right" ), "Kreuzungsweiche rechts" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "vert" ), "Kreuzungsweiche vertikal" );
    }
    else if (functionType == FunctionType.Yturn) {
      // TODO
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "script" ), "Kein Icon verfügbar" );
    }
    else if (functionType == FunctionType.Stop) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Ampel aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "free" ), "Ampel grün" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "halt" ), "Ampel rot " );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "slow" ), "Ampel grün+gelb" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp0_Bar" ), "Flügelsignal rot (Hp0)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp1_Bar" ), "Flügelsignal grün (Hp1)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp2_Bar" ), "Flügelsignal langsam (Hp2) " );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "stop_Bar" ), "Flügelsignal rot+H (Hp0+H)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "reverse_Bar" ), "Flügelsignal rot+R (Hp0+R)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp0_Light" ), "Lichtsignal rot (Hp0)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp0Sh1_Light" ), "Lichtsignal rot+weiß (Hp0+Sh1)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp1_Light" ), "Lichtsignal grün (Hp1)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Hp2_Light" ), "Lichtsignal langsam (Hp2) " );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "stop_Light" ), "Lichtsignal rot+R (Hp0+R)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "reverse_Light" ), "Lichtsignal rot+R (Hp0+R)" );
    }
    else if (functionType == FunctionType.Warn) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "free" ), "Vorsignal grün" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "sp40" ), "Vorsignal Tempo 40" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "sp60" ), "Vorsignal Tempo 60" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr0_Bar" ), "Formvorsignal gelb (Vr0)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr0_Light" ), "Lichtvorsignal gelb (Vr0)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr1_Bar" ), "Formvorsignal grün (Vr1)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr1_Light" ), "Lichtvorsignal grün (Vr1)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr2_Bar" ), "Formvorsignal langsam (Vr2)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Vr2_Light" ), "Lichtvorsignal langsam (Vr2)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "warn" ), "Vorsignal gelb" );
    }
    else if (functionType == FunctionType.Sign) {
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "E" ), "Zeichen E" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "H" ), "Zeichen H" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "R" ), "Zeichen R" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "off" ), "Zeichen aus" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "Sh2" ), "Schutzhalt (Sh2)" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_1" ), "Limit 1" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_2" ), "Limit 2" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_3" ), "Limit 3" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_4" ), "Limit 4" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_5" ), "Limit 5" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_6" ), "Limit 6" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_7" ), "Limit 7" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_8" ), "Limit 8" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_9" ), "Limit 9" );
      nameIconTable.addStringEntry( PiRailFactory.NAMESPACE.getQName( "limit_10" ), "Limit 10" );
    }
    return nameIconTable;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectNameIconParams dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      TableController cmdTypeTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "nameIconTable" ) );
      QName selectedID = (QName) cmdTypeTable.getSelectedRow( 0 ).key();
      return new SelectNameIconParams( null, selectedID );
    }
    else {
      return new SelectNameIconParams( null, null );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
