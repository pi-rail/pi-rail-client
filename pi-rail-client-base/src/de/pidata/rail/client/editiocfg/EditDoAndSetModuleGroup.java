package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.Action;
import de.pidata.rail.model.CallCmd;
import de.pidata.rail.railway.RailAction;

public class EditDoAndSetModuleGroup extends ModuleGroup implements EventListener {

  @Override
  public void setModel( Model model ) {
    if (model instanceof CallCmd) {
      updateCallAction( (CallCmd) model );
      model.addListener( this );
    }
    super.setModel( model );
  }

  public static void updateCallAction( CallCmd callCmd ) {
    QName actionID = callCmd.getActionID();
    Model localAction = callCmd.getOwner().getCfg().get( null, actionID );
    if (localAction instanceof Action) {
      callCmd.setCallAction( (Action) localAction );
    }
    else {
      RailAction callAction = PiRail.getInstance().getModelRailway().getRailAction( null, callCmd.getActionID() );
      if (callAction == null) {
        callCmd.setCallAction( null );
      }
      else {
        callCmd.setCallAction( callAction.getAction() );
      }
    }
  }

  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source == getModel()) {
      if ((source instanceof CallCmd) && (modelID == CallCmd.ID_ACTIONID)) {
        updateCallAction( (CallCmd) source);
      }
    }
  }

  /**
   * Called to deactivate this controller. It now should stop
   * listening on its binding and detach from its UI.
   *
   * @param saveValue if true controller should save its value
   *                  if false controller should abort any value changes
   */
  @Override
  public void deactivate( boolean saveValue ) {
    Model model = getModel();
    if (model instanceof CallCmd) {
      model.removeListener( this );
    }
    saveValue();
    super.deactivate( saveValue );
  }
}
