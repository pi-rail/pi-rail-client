package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.service.base.ServiceException;

public class IoCfgTreeAction extends GuiOperation {

  public static final QName ID_ITEM_CONN_TABLE = NAMESPACE.getQName( "itemConnTable" );
  public static final QName ID_ITEM_PIN_TABLE = NAMESPACE.getQName( "itemPinTable" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {


    ButtonController testBtnCtrl = (ButtonController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "test_btn" ));
    ButtonController selectItemBtnCtrl = (ButtonController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "select_item_btn" ));
    if (((DefaultTreeController) sourceCtrl).getSelectedNode() == ((DefaultTreeController) sourceCtrl).getRootNode()) {
      selectItemBtnCtrl.setVisible( false );
      testBtnCtrl.setVisible( false );
    }
    else {
      selectItemBtnCtrl.setVisible( true );
      testBtnCtrl.setVisible( true );
    }


    TableController itemConnTab = (TableController) sourceCtrl.getControllerGroup().getController( ID_ITEM_CONN_TABLE );
    TableController itemPinTab = (TableController) sourceCtrl.getControllerGroup().getController( ID_ITEM_PIN_TABLE );
    if (dataContext instanceof OutCfg) {
      OutCfg outCfg = (OutCfg) dataContext;
      OutPin connPin = outCfg.getConnectedPin();
      if (connPin != null) {
        itemConnTab.selectRow( connPin.getItemConn() );
        itemPinTab.selectRow( connPin );
      }
      else {
        itemPinTab.selectRow( null );
      }
    }
    else if (dataContext instanceof InCfg) {
      InCfg inCfg = (InCfg) dataContext;
      InPin connPin = inCfg.getConnectedPin();
      if (connPin != null) {
        itemConnTab.selectRow( connPin.getItemConn() );
        itemPinTab.selectRow( connPin );
      }
      else {
        itemPinTab.selectRow( null );
      }
    }
    else if (dataContext instanceof PortCfg) {
      PortCfg portCfg = (PortCfg) dataContext;
      Port connPort = portCfg.getConnectedPort();
      if (connPort != null) {
        itemConnTab.selectRow( connPort.getItemConn() );
        itemPinTab.selectRow( connPort );
      }
      else {
        itemPinTab.selectRow( null );
      }
    }
  }
}
