package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class ActionPickerParamList extends AbstractParameterList {

  public static final QName SELECTED_ACTION_ID = ModelRailway.NAMESPACE.getQName( "selectedActionID" );
  public static final QName SELECTED_GROUP_NAME = ModelRailway.NAMESPACE.getQName( "selectedGroupName" );
  public static final QName REFERENCED_DEVICE_ID = ModelRailway.NAMESPACE.getQName( "referencedDeviceID" );
  public static final QName REFERENCED_ACTION_ID = ModelRailway.NAMESPACE.getQName( "referencedActionID" );

  public ActionPickerParamList() {
    super( ParameterType.QNameType, SELECTED_ACTION_ID, ParameterType.QNameType, SELECTED_GROUP_NAME, ParameterType.QNameType, REFERENCED_DEVICE_ID, ParameterType.QNameType, REFERENCED_ACTION_ID );
  }

  public ActionPickerParamList( QName selectedActionID, QName selectedGrouName, QName referencedDeviceID, QName referencedActionID ) {
    super( ParameterType.QNameType, SELECTED_ACTION_ID, ParameterType.QNameType, SELECTED_GROUP_NAME, ParameterType.QNameType, REFERENCED_DEVICE_ID, ParameterType.QNameType, REFERENCED_ACTION_ID );
    setQName( SELECTED_ACTION_ID, selectedActionID );
    setQName( SELECTED_GROUP_NAME, selectedGrouName );
    setQName( REFERENCED_DEVICE_ID, referencedDeviceID );
    setQName( REFERENCED_ACTION_ID, referencedActionID );
  }

  public void setReferencedDeviceId( QName referencedDeviceID ) {
    setQName( REFERENCED_DEVICE_ID, referencedDeviceID );
  }

  public void setReferencedActionId( QName referencedActionId ) {
    setQName( REFERENCED_ACTION_ID, referencedActionId );
  }

  public QName getSelectedActionId() {
    return getQName( SELECTED_ACTION_ID );
  }

  public QName getSelectedGroupName() {
    return getQName( SELECTED_GROUP_NAME );
  }

  public QName getReferencedDeviceId() {
    return getQName( REFERENCED_DEVICE_ID );
  }

  public QName getReferencedActionId() {
    return getQName( REFERENCED_ACTION_ID );
  }
}
