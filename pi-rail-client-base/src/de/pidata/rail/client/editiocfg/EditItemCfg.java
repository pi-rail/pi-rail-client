package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.ProductCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class EditItemCfg  extends GuiDelegateOperation<EditCfgDelegate> {

  private ItemConn itemConn;
  private String oldName;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TableController itemConnTable = (TableController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "itemConnTable" ) );
    itemConn = (ItemConn) itemConnTable.getSelectedRow( 0 );
    if (itemConn != null) {
      oldName = itemConn.getId().getName();
      int pos = oldName.toLowerCase().indexOf( "-conn" );
      if (pos > 0) {
        oldName = oldName.substring( 0, pos );
      }
      String title = SystemManager.getInstance().getLocalizedMessage( "editItemCfgRename_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editItemCfgRename_TXT", null, null );
      String btnOk = SystemManager.getInstance().getGlossaryString( "rename" );
      String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
      showInput( sourceCtrl, title, text, oldName, btnOk, btnCancel );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK && (itemConn != null)) {
      if (resultList instanceof QuestionBoxResult) {
        String newName = ((QuestionBoxResult) resultList).getInputValue();
        if (itemConn.getProduct() == ProductCfg.ID_IR_BALISE) {
          String msg = TrackPos.checkPosID( newName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "editItemCfgInvalidPosID_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "editItemCfgInvalidPosID_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            itemConn = null;
            return;
          }
        }
        else {
          String msg = Action.checkID( newName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "editItemCfgInvalidID_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "editItemCfgInvalidID_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            itemConn = null;
            return;
          }
        }

        EditCfgDelegate delegate = (EditCfgDelegate) parentDlgCtrl.getDelegate();
        Cfg cfg = delegate.getUiModel().getCfg();
        QName newConnID = itemConn.getId().getNamespace().getQName( newName + "-conn" );
        if (newConnID != itemConn.getId()) {
          ItemConn other = cfg.getItemConn( newConnID );
          if (other != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "editItemCfgDupName_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "editItemCfgDupName_TXT", null, params );
            parentDlgCtrl.showMessage( title, text );
          }
          else {
            ItemConn newItemConn = (ItemConn) itemConn.clone( newConnID, true, false );
            cfg.addItemConn( newItemConn );
            for (ModelIterator cfgEntryIter = cfg.iterator( null, null ); cfgEntryIter.hasNext(); ) {
              Model cfgEntry = cfgEntryIter.next();
              if (cfgEntry instanceof Action) {
                Action action = (Action) cfgEntry;
                if (action.getItemConn() == itemConn) {
                  action.setItemID( newConnID );
                  QName id = action.getId();
                  String name = id.getName();
                  String newActionName = name.replace( oldName, newName );
                  if (!name.equals( newActionName )) {
                    delegate.renameAction( (Action) cfgEntry, newActionName );
                  }
                }
              }
            }

            IoCfg ioCfg = delegate.getUiModel().getIoCfg();
            for (OutCfg outCfg : ioCfg.outCfgIter()) {
              OutPin connPin = outCfg.getConnectedPin();
              if ((connPin != null) && (connPin.getItemConn() == itemConn)) {
                outCfg.setConnectedPin( newItemConn.getOutPin( connPin.getId() ) );
              }
            }
            for (InCfg inCfg : ioCfg.inCfgIter()) {
              InPin connPin = inCfg.getConnectedPin();
              if ((connPin != null) && (connPin.getItemConn() == itemConn)) {
                inCfg.setConnectedPin( newItemConn.getInPin( connPin.getId() ) );
              }
            }
            for (PortCfg portCfg : ioCfg.portCfgIter()) {
              Port port = portCfg.getConnectedPort();
              if ((port != null) && (port.getItemConn() == itemConn)) {
                portCfg.setConnectedPort( newItemConn.getPort( port.getId() ) );
              }
            }

            cfg.removeItemConn( itemConn );
          }
        }
      }
    }
    itemConn = null;
  }
}
