package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.EnumAction;
import de.pidata.rail.model.StateScript;
import de.pidata.rail.railway.ActionGroup;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailAction;
import de.pidata.service.base.ParameterList;

public class ActionScriptPickerDelegate implements DialogControllerDelegate<ActionPickerParamList,ActionPickerParamList> {

  private static final QName ID_ACTION_SCRIPT_TABLE = GuiBuilder.NAMESPACE.getQName( "actionScriptTable" );
  private static final QName ID_ACTION_TABLE = GuiBuilder.NAMESPACE.getQName( "actionTable" );

  private ActionPickerParamList paramList;
  private DialogController dlgCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ActionPickerParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.paramList = parameterList;
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    ActionGroup actionGroup = modelRailway.getActionGroup( parameterList.getSelectedGroupName() );
    return actionGroup;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    ActionGroup actionGroup = null;
    RailAction railAction = null;
    QName refActionID = paramList.getReferencedActionId();
    railAction = PiRail.getInstance().getModelRailway().getRailAction( null, refActionID );
    if (railAction != null) {
      RailAction action = actionGroup.getRailAction( refActionID );
      TableController actionTable = (TableController) dlgCtrl.getController( ID_ACTION_TABLE );
      actionTable.selectRow( action );
    }
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ActionPickerParamList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      TableController scriptTable = (TableController) dlgCtrl.getController( ID_ACTION_SCRIPT_TABLE );
      StateScript stateScript = (StateScript) scriptTable.getSelectedRow( 0 );
      if (stateScript != null) {
        EnumAction enumAction = (EnumAction) stateScript.getParent( false );
        paramList.setReferencedActionId( enumAction.getId() );
        paramList.setReferencedDeviceId( stateScript.getId() );
      }
    }
    return paramList;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}

