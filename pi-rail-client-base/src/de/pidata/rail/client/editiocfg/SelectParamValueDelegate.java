package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.KeyAndValue;
import de.pidata.gui.guidef.StringTable;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Command;
import de.pidata.rail.model.Param;
import de.pidata.service.base.ParameterList;

public class SelectParamValueDelegate implements DialogControllerDelegate<SelectParamValueParams,SelectParamValueParams> {

  public static final QName PARAM_CMD_EXT = GuiBuilder.NAMESPACE.getQName( "cmd-ext" );

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectParamValueParams parameterList ) throws Exception {
    StringTable paramValueTable = new StringTable( GuiBuilder.NAMESPACE.getQName( "paramValueTable" ) );
    QName paramName = parameterList.getParamName();
    if ((paramName == Param.PARAM_CMD) || (paramName == PARAM_CMD_EXT)) {
      if (paramName == PARAM_CMD_EXT) {
        paramValueTable.addStringEntry( Param.NAMESPACE.getQName( "<Übergebener Wert>" ), "?" );
      }
      for (Param param : Command.idSenderCommands()) {
        paramValueTable.addStringEntry( param.getId(), param.getStringValue() );
      }
    }
    return paramValueTable;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectParamValueParams dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      TableController cmdTypeTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "paramValueTable" ) );
      KeyAndValue selectedRow = (KeyAndValue) cmdTypeTable.getSelectedRow( 0 );
      if (selectedRow != null) {
        return new SelectParamValueParams( null, selectedRow.getValue() );
      }
      else {
        return new SelectParamValueParams( null, null );
      }
    }
    else {
      return new SelectParamValueParams( null, null );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
