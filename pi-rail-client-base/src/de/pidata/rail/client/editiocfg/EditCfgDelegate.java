package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoaderListener;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.net.InetAddress;

public abstract class EditCfgDelegate implements DialogControllerDelegate<EditCfgParamList,ParameterList>, ConfigLoaderListener {

  public static final String CONFIG_DEFEKT = "Config Damaged";
  public static final String CONFIG_ERROR = "Config Error";
  public static final QName ID_ACTION_TREE = GuiOperation.NAMESPACE.getQName( "actionTree" );

  protected EditCfgUI uiModel;
  protected DialogController dlgCtrl;
  protected ModuleController actionCfgCtrl;
  protected QName deviceId;
  protected InetAddress ipAddress;

  public InetAddress getIpAddress() {
    return ipAddress;
  }

  public EditCfgUI getUiModel() {
    return uiModel;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  protected void initItemConn( ItemConn itemConn ) {
    for (OutPin outPin : itemConn.outPinIter()) {
      OutCfg outCfg = uiModel.findOutCfg( outPin.getIoID() );
      if (outCfg != null) {
        outCfg.setConnectedPin( outPin );
      }
    }
    for (InPin inPin : itemConn.inPinIter()) {
      InCfg inCfg = uiModel.findInCfg( inPin.getIoID() );
      if (inCfg != null) {
        inCfg.setConnectedPin( inPin );
      }
    }
    for (Port port : itemConn.portIter()) {
      PortCfg portCfg = uiModel.findPortCfg( port.getIoID() );
      if (portCfg != null) {
        portCfg.setConnectedPort( port );
      }
    }
  }

  public RailDevice getRailDevice() {
    return PiRail.getInstance().getModelRailway().getRailDevice( getIpAddress() );
  }

  protected TableController getItemPinTable() {
    return (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "itemPinTable" ) );
  }

  public TreeController getActionTree() {
    return (TreeController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionTree" ) );
  }

  public TableController getParamPinTable() {
    return (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "itemPinTableActions" ) );
  }

  public OutPin getSelectedOutPin() {
    TableController itemPinTable = getItemPinTable();
    Model selectedRow = itemPinTable.getSelectedRow( 0 );
    if (selectedRow instanceof OutPin) {
      return (OutPin) selectedRow;
    }
    else {
      return null;
    }
  }

  public InPin getSelectedInPin() {
    TableController itemPinTable = getItemPinTable();
    Model selectedRow = itemPinTable.getSelectedRow( 0 );
    if (selectedRow instanceof InPin) {
      return (InPin) selectedRow;
    }
    else {
      return null;
    }
  }

  public Port getSelectedPort() {
    TableController itemPinTable = getItemPinTable();
    Model selectedRow = itemPinTable.getSelectedRow( 0 );
    if (selectedRow instanceof Port) {
      return (Port) selectedRow;
    }
    else {
      return null;
    }
  }

  private QName createID( QName templateID, QName newID ) {
    String name = templateID.getName().replace( "{NAME}", newID.getName() );
    return newID.getNamespace().getQName( name );
  }

  private void processNewIfCmd( IfCmd ifCmd, QName newName ) {
    for (ModelIterator cmdIter = ifCmd.iterator( null, null ); cmdIter.hasNext(); ) {
      Model cmd = cmdIter.next();
      if (cmd instanceof CallCmd) {
        QName actionID = ((CallCmd) cmd).getActionID();
        ((CallCmd) cmd).setActionID( createID( actionID, newName ) );
      }
      else if (cmd instanceof IfCmd) {
        processNewIfCmd( (IfCmd) cmd, newName );
      }
    }
  }

  private void processNewStateScript( StateScript stateScript, QName newName ) {
    if (stateScript != null) {
      for (ModelIterator cmdIter = stateScript.iterator( null, null ); cmdIter.hasNext(); ) {
        Model cmd = cmdIter.next();
        if (cmd instanceof CallCmd) {
          QName actionID = ((CallCmd) cmd).getActionID();
          ((CallCmd) cmd).setActionID( createID( actionID, newName ) );
        }
        else if (cmd instanceof IfCmd) {
          processNewIfCmd( (IfCmd) cmd, newName );
        }
      }
    }
  }

  public ItemConn addItemConn( String name ) {
    Cfg cfg = uiModel.getCfg();
    QName itemConnID = getUniqueActionID( name );
    ItemConn itemConn = new ItemConn( itemConnID );
    cfg.addItemConn( itemConn );
    return itemConn;
  }

  public ItemConn addProduct( QName name, Product product, ProductCfg productCfg ) {
    Cfg cfg = uiModel.getCfg();
    ItemConn prodItemConn = product.getItemConn();
    QName itemConnID = createID( prodItemConn.getId(), name );
    ItemConn itemConn = (ItemConn) prodItemConn.clone( itemConnID, true, false );
    itemConn.setProduct( product.getId() );
    cfg.addItemConn( itemConn );
    for (MotorAction motorAction : productCfg.motorIter()) {
      QName newID = createID( motorAction.getId(), name );
      MotorAction newCfg = (MotorAction) motorAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      cfg.addMotor( newCfg );
    }
    for (EnumAction enumAction : productCfg.funcIter()) {
      QName newID = createID( enumAction.getId(), name );
      EnumAction newCfg = (EnumAction) enumAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      for (StateScript stateScript : newCfg.onStateIter()) {
        processNewStateScript( stateScript, name );
      }
      cfg.addFunc( newCfg );
    }
    for (TimerAction timerAction : productCfg.timerIter()) {
      QName newID = createID( timerAction.getId(), name );
      TimerAction newCfg = (TimerAction) timerAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      processNewStateScript( newCfg.getOnState(), name );
      cfg.addTimer( newCfg );
    }
    for (SensorAction sensorAction : productCfg.sensorIter()) {
      QName newID = createID( sensorAction.getId(), name );
      SensorAction newCfg = (SensorAction) sensorAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      for (StateScript stateScript : newCfg.onStateIter()) {
        processNewStateScript( stateScript, name );
      }
      cfg.addSensor( newCfg );
    }
    for (TriggerAction triggerAction : productCfg.triggerIter()) {
      QName newID = createID( triggerAction.getId(), name );
      TriggerAction newCfg = (TriggerAction) triggerAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      for (StateScript stateScript : newCfg.onStateIter()) {
        processNewStateScript( stateScript, name );
      }
      cfg.addTrigger( newCfg );
    }
    for (RangeAction rangeAction : productCfg.rangeIter()) {
      QName newID = createID( rangeAction.getId(), name );
      RangeAction newCfg = (RangeAction) rangeAction.clone( newID, true, false );
      newCfg.setItemID( itemConnID );
      cfg.addRange( newCfg );
    }
    TableController itemConnTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "itemConnTable" ) );
    if (itemConnTable != null) {
      itemConnTable.selectRow( itemConn );
    }
    return itemConn;
  }

  public void deleteProduct( ItemConn itemConn ) {
    Cfg cfg = uiModel.getCfg();
    QName itemID = itemConn.getId();
    for (EnumAction enumAction : cfg.funcIter()) {
      if (enumAction.getItemID() == itemID) {
        cfg.removeFunc( enumAction );
      }
    }
    for (TimerAction timerAction : cfg.timerIter()) {
      if (timerAction.getItemID() == itemID) {
        cfg.removeTimer( timerAction );
      }
    }
    for (SensorAction sensorAction : cfg.sensorIter()) {
      if (sensorAction.getItemID() == itemID) {
        cfg.removeSensor( sensorAction );
      }
    }
    for (TriggerAction triggerAction : cfg.triggerIter()) {
      if (triggerAction.getItemID() == itemID) {
        cfg.removeTrigger( triggerAction );
      }
    }
    for (RangeAction rangeAction : cfg.rangeIter()) {
      if (rangeAction.getItemID() == itemID) {
        cfg.removeRange( rangeAction );
      }
    }
    for (MotorAction motorAction : cfg.motorIter()) {
      if (motorAction.getItemID() == itemID) {
        cfg.removeMotor( motorAction );
      }
    }
    for (TrackMsg trackMsg : cfg.trackMsgIter()) {
      if (trackMsg.getItemID() == itemID) {
        cfg.removeTrackMsg( trackMsg );
      }
    }

    IoCfg ioCfg = uiModel.getIoCfg();
    if (ioCfg != null) {
      for (OutCfg outCfg : ioCfg.outCfgIter()) {
        OutPin connPin = outCfg.getConnectedPin();
        if ((connPin != null) && (connPin.getItemConn() == itemConn)) {
          outCfg.setConnectedPin( null );
        }
      }
      for (InCfg inCfg : ioCfg.inCfgIter()) {
        InPin connPin = inCfg.getConnectedPin();
        if ((connPin != null) && (connPin.getItemConn() == itemConn)) {
          inCfg.setConnectedPin( null );
        }
      }
      for (PortCfg portCfg : ioCfg.portCfgIter()) {
        Port port = portCfg.getConnectedPort();
        if ((port != null) && (port.getItemConn() == itemConn)) {
          portCfg.setConnectedPort( null );
        }
      }
    }

    cfg.removeItemConn( itemConn );
  }

  public QName getUniqueActionID( String baseName ) {
    Cfg cfg = uiModel.getCfg();
    Namespace ns = PiRailFactory.NAMESPACE;
    QName name = ns.getQName( baseName );
    Model cfgEntry = cfg.get( null, name );
    int index = 0;
    while (cfgEntry != null) {
      index++;
      name = ns.getQName( baseName + "-" + index );
      cfgEntry = cfg.get( null, name );
    }
    return name;
  }

  public void addTrigger( TriggerAction triggerAction ) {
    Cfg cfg = uiModel.getCfg();
    triggerAction.setType( ValueType.charVal );
    cfg.addTrigger( triggerAction );
  }

  public void addFunction( EnumAction enumAction ) {
    Cfg cfg = uiModel.getCfg();
    enumAction.setType( FunctionType.Switch );
    if (getRailDevice() instanceof SwitchBox) {
      enumAction.setGroup( PiRailFactory.NAMESPACE.getQName( "SignalTower" ));
    }
    cfg.addFunc( enumAction );
  }

  public void addRange( RangeAction rangeAction ) {
    Cfg cfg = uiModel.getCfg();
    if (getRailDevice() instanceof SwitchBox) {
      rangeAction.setGroup( PiRailFactory.NAMESPACE.getQName( "SignalTower" ));
    }
    cfg.addRange( rangeAction );
  }

  public void addTimer( TimerAction timerAction ) {
    Cfg cfg = uiModel.getCfg();
    timerAction.setType( TimerType.Script );
    cfg.addTimer( timerAction );
  }

  public void addSensor( SensorAction sensorAction ) {
    Cfg cfg = uiModel.getCfg();
    cfg.addSensor( sensorAction );
  }

  public void addExtension( PortCfg portCfg, QName extCfgID, Extension extension, int busAddr ) {
    ExtCfg extCfg = (ExtCfg) extension.getExtCfg().clone( extCfgID, true, false );
    extCfg.setPort( portCfg.getId() );
    extCfg.setBusAddr( Integer.valueOf( busAddr ) );
    getUiModel().getCfg().addExtCfg( extCfg );

    for (Product product : extension.productIter()) {
      ProductCfg productCfg = product.getProductCfg( null );
      ItemConn itemConn = addProduct( extCfgID, product, productCfg );
      initItemConn( itemConn );
    }
    portCfg.refreshExt();
  }

  public Action renameAction( Action action, String newName ) {
    QName newID = getUniqueActionID( newName );
    if (newID != action.getId()) {
      Action newAction = (Action) action.clone( newID, true, false );
      QName relID = action.getParentRelationID();
      Model parent = action.getParent( false );
      parent.remove( relID, action );
      parent.add( relID, newAction );
      return newAction;
    }
    else {
      return action;
    }
  }

  public ItemConn getSelectedItemConn() {
    TreeController actionTree = getActionTree();
    TreeNodePI treeNodePI = actionTree.getSelectedNode();
    Model nodeModel = treeNodePI.getNodeModel();
    if (nodeModel instanceof ItemConn) {
      return  (ItemConn) nodeModel;
    }
    else if (nodeModel instanceof RailAction) {
      return ((RailAction) nodeModel).getItemConn();
    }
    else if (nodeModel instanceof Action) {
      return ((Action) nodeModel).getItemConn();
    }
    else {
      return null;
    }
  }

  public Action getSelectedAction() {
    TreeController actionTree = getActionTree();
    TreeNodePI treeNodePI = actionTree.getSelectedNode();
    Model nodeModel = treeNodePI.getNodeModel();
    if (nodeModel instanceof Action) {
      return (Action) nodeModel;
    }
    else {
      return null;
    }
  }
}
