package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.RailBlock;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class RenameAction extends GuiDelegateOperation<EditCfgDelegate> {

  private Action action;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (dataContext instanceof Action) {
      action = (Action) dataContext;
      String title = SystemManager.getInstance().getLocalizedMessage( "renameActionRename_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "renameActionRename_TXT", null, null );
      String btnOk = SystemManager.getInstance().getGlossaryString( "rename" );
      String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
      showInput( sourceCtrl, title, text, action.getId().getName(), btnOk, btnCancel );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK && (action != null)) {
      if (resultList instanceof QuestionBoxResult) {
        String newName = ((QuestionBoxResult) resultList).getInputValue();
        if ((action instanceof TimerAction) && ((TimerAction) action).getType() == TimerType.IRSender) {
          String msg = TrackPos.checkPosID( newName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidPosID_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidPosID_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            action = null;
            return;
          }
        }
        else if ((action instanceof SensorAction) && ((SensorAction) action).getType() == InModeType.Block) {
          String msg = RailBlock.checkBlockID( newName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidBlockID_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidBlockID_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            action = null;
            return;
          }
        }
        else {
          String msg = Action.checkID( newName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", newName );
            String title = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidID_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "renameActionInvalidID_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            action = null;
            return;
          }
        }

        if (!Helper.isNullOrEmpty( newName )) {
          EditCfgDelegate delegate = (EditCfgDelegate) parentDlgCtrl.getDelegate();
          action = delegate.renameAction( action, newName );
        }
        TreeController actionTree = (TreeController) parentDlgCtrl.getController( EditCfgDelegate.ID_ACTION_TREE );
        actionTree.setSelectedModel( action );
      }
    }
    action = null;
  }
}
