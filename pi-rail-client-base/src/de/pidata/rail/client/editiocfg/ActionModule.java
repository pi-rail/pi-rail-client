package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;

public class ActionModule extends ModuleGroup implements EventListener {

  private TreeController actionTree;
  private TableController pinParamTable;
  private ModuleController detailsCtrl;

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    super.activateModule( moduleContainer );
    actionTree = (TreeController) getController( GuiBuilder.NAMESPACE.getQName( "actionTree" ) );
    pinParamTable = (TableController) getController( GuiBuilder.NAMESPACE.getQName( "itemPinTableActions" ) );
    detailsCtrl = (ModuleController) getController( GuiBuilder.NAMESPACE.getQName( "actionDetails" ) );
    pinParamTable.getSelection().getListBinding().setModel( null );
    actionTree.addListener( this );
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if ((source == actionTree) && (eventID == ID_MODEL_DATA_CHANGED)) {
      QName moduleID = null;
      detailsCtrl.setModel( null );
      ItemConn itemConn = null;
      if (newValue instanceof EnumAction) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "enum_details" );
        Cfg cfg = (Cfg) ((EnumAction) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((EnumAction) newValue).getItemID() );
      }
      else if (newValue instanceof TriggerAction) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "trigger_details" );
        Cfg cfg = (Cfg) ((TriggerAction) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((TriggerAction) newValue).getItemID() );
      }
      else if (newValue instanceof TimerAction) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "timer_details" );
        Cfg cfg = (Cfg) ((TimerAction) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((TimerAction) newValue).getItemID() );
      }
      else if (newValue instanceof SensorAction) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "sensor_details" );
        Cfg cfg = (Cfg) ((SensorAction) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((SensorAction) newValue).getItemID() );
      }
      else if (newValue instanceof RangeAction) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "range_details" );
        Cfg cfg = (Cfg) ((RangeAction) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((RangeAction) newValue).getItemID() );
      }
      else if (newValue instanceof Action) {
        Cfg cfg = (Cfg) ((Action) newValue).getParent( false );
        itemConn = cfg.getItemConn( ((Action) newValue).getItemID() );
      }
      else if (newValue instanceof ItemConn) {
        itemConn = (ItemConn) newValue;
      }
      if (moduleID == null) {
        detailsCtrl.setCurrentModule( null );
      }
      else {
        try {
          getDialogController().getControllerBuilder().loadModule( detailsCtrl, moduleID, actionTree, "." );
        }
        catch (Exception e) {
          Logger.error( "Error updating details module", e );
        }
      }
      pinParamTable.getSelection().getListBinding().setModel( itemConn );
    }
  }
}
