package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editDigital.EditDigitalDelegate;
import de.pidata.rail.model.ExtCfg;
import de.pidata.rail.model.PortCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class EditExtCfg extends GuiDelegateOperation<EditCfgDelegate> {

  private String title = "";
  private ExtCfg extCfg = null;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController actionTree = (TreeController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "ioCfgTree" ) );
    TreeNodePI selectedNode = actionTree.getSelectedNode();
    extCfg = null;
    if (selectedNode != null) {
      Model selectedModel = selectedNode.getNodeModel();
      if (selectedModel instanceof PortCfg) {
        if (delegate instanceof EditDigitalDelegate) {
          PortCfg portCfg = (PortCfg) selectedModel;
          Properties params = new Properties();
          params.put( "name", portCfg.getId().getName() );
          params.put( "type", portCfg.getType() );
          this.title = SystemManager.getInstance().getLocalizedMessage( "editExtCfgPort_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editExtCfgPort_TXT", null, params );
          String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
          String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
          showInput( sourceCtrl, title, text, portCfg.getId().getName(), btnOk, btnCancel );
        }
      }
      else if (selectedModel instanceof ExtCfg) {
        extCfg = (ExtCfg) selectedModel;
        int value = extCfg.getBusAddrInt();
        Properties params = new Properties();
        params.put( "name", extCfg.getLabel() );
        params.put( "type", extCfg.getType().toString() );
        this.title = SystemManager.getInstance().getLocalizedMessage( "editExtCfgExtCfg_H", null, params );
        String text = SystemManager.getInstance().getLocalizedMessage( "editExtCfgExtCfg_TXT", null, params );
        String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
        String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
        showInput( sourceCtrl, title, text, Integer.toString( value ), btnOk, btnCancel );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        QuestionBoxResult inputResult = (QuestionBoxResult) resultList;
        String title = inputResult.getTitle();
        if (title.equals( this.title )) {
          if (extCfg == null) {
            EditDigitalDelegate delegate = (EditDigitalDelegate) parentDlgCtrl.getDelegate();
            delegate.renameZ21Port( inputResult.getInputValue() );
          }
          else {
            int busAddr = inputResult.getInputValueInt( 3 );
            extCfg.setBusAddr( Integer.valueOf( busAddr ) );
          }
        }
      }
    }
  }
}

