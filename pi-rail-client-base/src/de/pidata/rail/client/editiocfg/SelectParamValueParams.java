package de.pidata.rail.client.editiocfg;

import de.pidata.qnames.QName;
import de.pidata.rail.model.FunctionType;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;
import de.pidata.string.Helper;

public class SelectParamValueParams extends AbstractParameterList {

  public static final QName SELECTED_VALUE = ModelRailway.NAMESPACE.getQName( "selectedValue" );
  public static final QName PARAM_NAME = ModelRailway.NAMESPACE.getQName( "paramName" );

  public SelectParamValueParams() {
    super( ParameterType.QNameType, PARAM_NAME, ParameterType.StringType, SELECTED_VALUE );
  }

  public SelectParamValueParams( QName paramName, String selectedValue ) {
    this();
    setQName( PARAM_NAME, paramName );
    setString( SELECTED_VALUE, selectedValue );
  }

  public QName getParamName() {
    return getQName( PARAM_NAME );
  }

  public String getSelectedValue() {
    return getString( SELECTED_VALUE );
  }
}
