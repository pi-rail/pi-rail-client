package de.pidata.rail.client.editiocfg;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.IoCfg;
import de.pidata.rail.model.ItemConn;
import de.pidata.rail.model.PortCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

public class EditIoCfgDelegate extends EditCfgDelegate {

  protected ModuleController connCfgCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.deviceId = parameterList.getDeviceId();
    this.ipAddress = parameterList.getIPAddress();
    connCfgCtrl = (ModuleController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "connCfgModule" ) );
    actionCfgCtrl = (ModuleController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionCfgModule" ) );
    this.uiModel = new EditCfgUI( parameterList.getDeviceName(), deviceId, ipAddress );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.IO_CFG_XML, this, false, false );
    return uiModel;
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    //--- Modules must be laoded
    ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
    try {
      builder.loadModule( connCfgCtrl, GuiBuilder.NAMESPACE.getQName( "conncfg_list_module" ), connCfgCtrl, "." );
      builder.loadModule( actionCfgCtrl, GuiBuilder.NAMESPACE.getQName( "actioncfg_list_module" ), actionCfgCtrl, "." );
    }
    catch (Exception e) {
      Logger.error( "Error loading modules" );
    }
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (CONFIG_DEFEKT.equals( result.getTitle() )) {
        if (resultOK) {
          Cfg cfg = new Cfg();
          uiModel.setCfg( cfg );
          initCfg( cfg );
        }
        else {
          dlgCtrl.close( false );
        }
      }
      else if (CONFIG_ERROR.equals( result.getTitle() )) {
        dlgCtrl.close( false );
      }
    }
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      Model config = configLoader.getConfigModel();
      if (config instanceof IoCfg) {
        uiModel.setIoCfg( (IoCfg) config );
        ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.CFG_XML, this, false, false );
      }
      else if (config instanceof Cfg){
        uiModel.setCfg( (Cfg) config );
        initCfg( (Cfg) config );
      }
    }
    else {
      if (uiModel.getIoCfg() != null) {
        String text = SystemManager.getInstance().getLocalizedMessage( "editIoCfgConfigDamaged_TXT", null, null );
        String btnYes = SystemManager.getInstance().getGlossaryString( "yes" );
        String btnNo = SystemManager.getInstance().getGlossaryString( "no" );
        dlgCtrl.showQuestion( CONFIG_DEFEKT, text, btnYes, btnNo, null );
      }
      else {
        String text = SystemManager.getInstance().getLocalizedMessage( "editIoCfgConfigError_TXT", null, null );
        dlgCtrl.showMessage( CONFIG_ERROR, text );
      }
    }
  }

  private void initCfg( Cfg config ) {
    connCfgCtrl.setModel( uiModel );
    actionCfgCtrl.setModel( uiModel );
    for (PortCfg portCfg : uiModel.getIoCfg().portCfgIter()) {
      portCfg.setCfg( config );
    }
    for (ItemConn itemConn : config.itemConnIter()) {
      initItemConn( itemConn );
    }
  }
}
