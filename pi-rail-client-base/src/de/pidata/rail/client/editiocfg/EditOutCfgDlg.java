package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.OutCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;

public class EditOutCfgDlg extends GuiDelegateOperation<EditCfgDelegate> {

  private OutCfg outCfg;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = source.getControllerGroup();
    TableController tableCtrl = (TableController) ctrlGroup.getController( ControllerBuilder.NAMESPACE.getQName( "outcfgTable" ) );
    outCfg = (OutCfg) tableCtrl.getSelectedRow( 0 );
    if (outCfg != null) {
      throw new RuntimeException("TODO"); // set pulse in outPin
      //ctrlGroup.getDialogController().showInput( "Max Pulse", "Max Impuls (ms)", Short.toString( outCfg.getMaxPulseShort() ), "OK", "Abbruch" );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        throw new RuntimeException("TODO"); // set pulse in outPin
        /*int value = ((QuestionBoxResult) resultList).getInputValueInt( outCfg.getMaxPulse() );
        if (value > 5000) {
          value = 5000;
        }
        outCfg.setMaxPulse( (short) value );*/
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
