package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.file.SelectAssetFileParamList;
import de.pidata.rail.client.selectProduct.SelectProductParamList;
import de.pidata.rail.railway.ModelRailwayCatalog;
import de.pidata.rail.railway.Product;
import de.pidata.rail.railway.ProductCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

public class AddItemCfg extends GuiDelegateOperation<EditCfgDelegate> {

  private QName type = null;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    String title = SystemManager.getInstance().getLocalizedMessage( "addItemCfgSelectAsset_H", null, null );
    String text = SystemManager.getInstance().getLocalizedMessage( "addItemCfgSelectAsset_TXT", null, null );
    SelectAssetFileParamList params = new SelectAssetFileParamList( SystemManager.STORAGE_CLASSPATH, "catalog", "*.xml", null, text );
    openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_asset_file" ), title, params );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof SelectAssetFileParamList) {
      if (resultOK) {
        SelectAssetFileParamList assetFileParamList = (SelectAssetFileParamList) resultList;
        String fileName = assetFileParamList.getFileName();
        if (!Helper.isNullOrEmpty( fileName )) {
          String title = SystemManager.getInstance().getLocalizedMessage( "addItemCfgSelectProduct_H", null, null );
          SelectProductParamList parameterList = new SelectProductParamList( assetFileParamList.getStorageType(), fileName );
          parentDlgCtrl.openChildDialog( ControllerBuilder.NAMESPACE.getQName( "select_product" ), title, parameterList );
        }
      }
    }
    else if (resultList instanceof SelectProductParamList) {
      if (resultOK) {
        SelectProductParamList paramList = (SelectProductParamList) resultList;
        QName itemID = paramList.getItemID();
        QName productID = paramList.getProductID();
        QName productCfgID = paramList.getProductCfgID();
        if ((itemID != null) && (productID != null) && (productCfgID != null)) {
          String catalogPath = paramList.getCatalogPath();
          Storage storage;
          String fileName;
          int pos = catalogPath.lastIndexOf( "/" );
          if (pos > 0) {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType(), catalogPath.substring( 0, pos ) );
            fileName = catalogPath.substring( pos+1 );
          }
          else {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType() );
            fileName = catalogPath;
          }
          ModelRailwayCatalog catModel = (ModelRailwayCatalog) XmlReader.loadData( storage, fileName );
          Product product = catModel.getProduct( productID );
          if (product != null) {
            ProductCfg productCfg = product.getProductCfg( productCfgID );
            if (productCfg != null) {
              ((EditCfgDelegate) parentDlgCtrl.getDelegate()).addProduct( itemID, product, productCfg );
            }
          }
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}

