package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editDigital.EditDigitalDelegate;
import de.pidata.rail.client.file.SelectAssetFileParamList;
import de.pidata.rail.client.selectExtension.SelectExtensionParamList;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.Extension;
import de.pidata.rail.railway.ModelRailwayCatalog;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddExtCfg extends GuiDelegateOperation<EditCfgDelegate> {

  public static final QName ID_IO_CFG_TREE = NAMESPACE.getQName( "ioCfgTree" );

  private PortCfg portCfg;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController ioCfgTree = (TreeController) sourceCtrl.getControllerGroup().getController( ID_IO_CFG_TREE );
    TreeNodePI selectedNode = ioCfgTree.getSelectedNode();
    if (selectedNode != null) {
      Model selectedModel = selectedNode.getNodeModel();
      if (selectedModel instanceof PortCfg) {
        portCfg = (PortCfg) selectedModel;
        Port port = portCfg.getConnectedPort();
        if (portCfg.getConnectedPort() == null) {
          String title = SystemManager.getInstance().getLocalizedMessage( "addExtCfgSelectAsset_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addExtCfgSelectAsset_TXT", null, null );
          SelectAssetFileParamList params = new SelectAssetFileParamList( SystemManager.STORAGE_CLASSPATH, "extension", "*.xml", null, text );
          openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_asset_file" ), title, params );
        }
        else {
          Properties params = new Properties();
          params.put( "portCfgName", portCfg.getId().getName() );
          params.put( "portName", port.getIoID().getName() );
          String title = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAlreadyConn_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAlreadyConn_TXT", null, params );
          showMessage( sourceCtrl, title, text  );
        }
      }
      else if (selectedModel instanceof IoCfg) {
        if (delegate instanceof EditDigitalDelegate) {
          IoCfg cfg = (IoCfg) selectedModel;
          if (cfg.portCfgCount() > 0) {
            String title = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAlreadyZ21_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAlreadyZ21_TXT", null, null );
            showMessage( sourceCtrl, title, text );
          }
          else {
            String title = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAddZ21_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "addExtCfgAddZ21_TXT", null, null );
            String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
            String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
            showInput( sourceCtrl, title, text,"", btnOk, btnCancel );
          }
        }
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof SelectAssetFileParamList) {
      if (resultOK) {
        SelectAssetFileParamList assetFileParamList = (SelectAssetFileParamList) resultList;
        SelectExtensionParamList parameterList = new SelectExtensionParamList( assetFileParamList.getStorageType(), assetFileParamList.getFileName() );
        String title = SystemManager.getInstance().getLocalizedMessage( "addExtCfgSelectExt_H", null, null );
        parentDlgCtrl.openChildDialog( ControllerBuilder.NAMESPACE.getQName( "select_extension" ), title, parameterList );
      }
    }
    else if (resultList instanceof SelectExtensionParamList) {
      if (resultOK) {
        SelectExtensionParamList paramList = (SelectExtensionParamList) resultList;
        QName extcfgId = paramList.getExtcfgId();
        QName extensionId = paramList.getExtensionId();
        int busAddr = paramList.getBusAddressInt();
        if ((extcfgId != null) && (extensionId != null)) {
          String catalogPath = paramList.getCatalogPath();
          Storage storage;
          String fileName;
          int pos = catalogPath.lastIndexOf( "/" );
          if (pos > 0) {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType(), catalogPath.substring( 0, pos ) );
            fileName = catalogPath.substring( pos+1 );
          }
          else {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType() );
            fileName = catalogPath;
          }
          ModelRailwayCatalog catModel = (ModelRailwayCatalog) XmlReader.loadData( storage, fileName );
          Extension extension = catModel.getExtension( extensionId );
          if (extension != null) {
            ((EditCfgDelegate) parentDlgCtrl.getDelegate()).addExtension( this.portCfg, extcfgId, extension, busAddr );
          }
        }
      }
      portCfg = null;
    }
    else if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        String addrStr = ((QuestionBoxResult) resultList).getInputValue();
        if (!Helper.isNullOrEmpty( addrStr )) {
          QName z21Addr = PiRailFactory.NAMESPACE.getQName( addrStr );
          ((EditDigitalDelegate) parentDlgCtrl.getDelegate()).addZ21Port( z21Addr );
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
