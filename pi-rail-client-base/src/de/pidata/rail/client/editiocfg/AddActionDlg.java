package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.selectProduct.SelectProductParamList;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

public class AddActionDlg extends GuiDelegateOperation<EditCfgDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController actionTree = (TreeController) sourceCtrl.getDialogController().getController( EditCfgDelegate.ID_ACTION_TREE );
    TreeNodePI treeNodePI = actionTree.getSelectedNode();
    if (treeNodePI == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "addActionDlgNoSelection_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "addActionDlgSelection_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    Model nodeModel = treeNodePI.getNodeModel();
    if (nodeModel instanceof Cfg) {
      SelectProductParamList parameterList = new SelectProductParamList( SystemManager.STORAGE_CLASSPATH, "auto/automation-products.xml" );
      String title = SystemManager.getInstance().getLocalizedMessage( "addActionDlgSelectProduct_H", null, null );
      openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "select_product" ), title, parameterList );
    }
    else if ((nodeModel instanceof ItemConn) || (nodeModel instanceof RailAction)) {
      ItemConn itemConn;
      if (nodeModel instanceof ItemConn) {
        itemConn = (ItemConn) nodeModel;
      }
      else {
        itemConn = ((RailAction) nodeModel).getItemConn();
      }
      if (itemConn.getProduct() == Product.PRODUCT_BLOCK) {
        SelectNameIconParams params = new SelectNameIconParams( FunctionType.Switch, null );
        String title = SystemManager.getInstance().getLocalizedMessage( "addActionDlgCreateRunningTrack_H", null, null );
        openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "select_name_icon" ), title, params );
      }
      else {
        String title = SystemManager.getInstance().getLocalizedMessage( "addActionDlgAddAction_H", null, null );
        sourceCtrl.getDialogController().showPopup( ControllerBuilder.NAMESPACE.getQName( "add_action" ), title, AbstractParameterList.EMPTY );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    EditCfgDelegate delegate = (EditCfgDelegate) parentDlgCtrl.getDelegate();
    if (resultList instanceof SelectNameIconParams) {
      if (resultOK) {
        //--- Create Fahrstraße
        ItemConn itemConn = delegate.getSelectedItemConn();
        EnumAction newCfg = createFahrstrassenGruppe( itemConn, (SelectNameIconParams) resultList );
        if (newCfg != null) {
          delegate.getActionTree().setSelectedModel( newCfg );
        }
      }
    }
    else if (resultList instanceof SelectProductParamList) {
      if (resultOK) {
        SelectProductParamList paramList = (SelectProductParamList) resultList;
        QName itemID = paramList.getItemID();
        QName productID = paramList.getProductID();
        QName productCfgID = paramList.getProductCfgID();
        if ((itemID != null) && (productID != null) && (productCfgID != null)) {
          String catalogPath = paramList.getCatalogPath();
          Storage storage;
          String fileName;
          int pos = catalogPath.lastIndexOf( "/" );
          if (pos > 0) {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType(), catalogPath.substring( 0, pos ) );
            fileName = catalogPath.substring( pos+1 );
          }
          else {
            storage = SystemManager.getInstance().getStorage( paramList.getStorageType() );
            fileName = catalogPath;
          }
          ModelRailwayCatalog catModel = (ModelRailwayCatalog) XmlReader.loadData( storage, fileName );
          Product product = catModel.getProduct( productID );
          if (product != null) {
            ProductCfg productCfg = product.getProductCfg( productCfgID );
            if (productCfg != null) {
              ItemConn newItemConn = ((EditCfgDelegate) parentDlgCtrl.getDelegate()).addProduct( itemID, product, productCfg );
              delegate.getActionTree().setSelectedModel( newItemConn );
            }
          }
        }
      }
    }
    else {
      // AddAction already created Action, so nothing else to do
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }

  public static EnumAction createFahrstrassenGruppe( ItemConn itemConn, SelectNameIconParams resultList ) {
    QName name = resultList.getSelectedID();
    if (name == null) {
      return null;
    }
    else {
      String blockName = itemConn.getName();
      EnumAction newCfg = new EnumAction( name.getNamespace().getQName( blockName +  "-" + name.getName() ) );
      newCfg.setItemID( itemConn.getId() );
      newCfg.setType( FunctionType.Path );
      newCfg.setGroup( ModelRailway.GROUP_FAHRSTRASSE );
      newCfg.setDefault( "0" );
      newCfg.setStartPos( new TrackPos( PiRailFactory.NAMESPACE.getQName( itemConn.getName() ) ) );
      StateScript offScript = new StateScript( PiRailFactory.NAMESPACE.getQName( "off" ), '0', 0 );
      newCfg.addOnState( offScript );
      StateScript stateScript = new StateScript( name, 'A', 1 );
      newCfg.addOnState( stateScript );
      itemConn.getCfg().addFunc( newCfg );
      return newCfg;
    }
  }
}
