package de.pidata.rail.client.editiocfg;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.ItemConn;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class DeleteItemCfg extends GuiDelegateOperation<EditCfgDelegate> {

  private ItemConn itemConn;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    TableController itemConnTable = (TableController) source.getControllerGroup().getController( NAMESPACE.getQName( "itemConnTable" ) );
    itemConn = (ItemConn) itemConnTable.getSelectedRow( 0 );
    if (itemConn != null) {
      Properties params = new Properties();
      params.put( "cfgName", itemConn.getId().getName() );
      String title = SystemManager.getInstance().getLocalizedMessage( "deleteItemCfgConfirm_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "deleteItemCfgConfirm_TXT", null, params );
      String btnYes = SystemManager.getInstance().getGlossaryString( "remove" );
      String btnNo = SystemManager.getInstance().getGlossaryString( "cancel" );
      showQuestion( source, title, text,btnYes, btnNo, null );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      if (resultOK) {
        if (itemConn != null) {
          ((EditCfgDelegate) parentDlgCtrl.getDelegate()).deleteProduct( itemConn );
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
