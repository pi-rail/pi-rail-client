package de.pidata.rail.client;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.*;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.*;
import java.net.InetAddress;
import java.net.URI;
import java.util.Locale;
import java.util.Properties;

public class ConfigBackup extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    String path = SystemManager.getInstance().getStorage( SystemManager.STORAGE_APPDATA ).getPath( "." );
    FileChooserParameter params = new FileChooserParameter( path, null, FileChooserParameter.TYPE_OPEN_DIR );
    String title = SystemManager.getInstance().getLocalizedMessage( "configBackupChooseFile_H", null, null );
    sourceCtrl.getDialogController().showFileChooser( title, params );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof FileChooserResult) {
      if (resultOK) {
        String path = ((FileChooserResult) resultList).getFilePath();
        if (path != null) {
          File backupDir = new File( path );
          parentDlgCtrl.getDialogComp().showBusy( true );
          new Thread( new Runnable() {
            @Override
            public void run() {
              int count = 0;
              int moduleCount = 0;
              try {
                ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
                for (SwitchBox switchBox : modelRailway.switchBoxIter()) {
                  count += downloadConfigFiles( switchBox, backupDir );
                  moduleCount++;
                }
                for (Locomotive locomotive : modelRailway.locoIter()) {
                  count += downloadConfigFiles( locomotive, backupDir );
                  moduleCount++;
                }
              }
              catch (Exception ex) {
                Logger.error( "Internal error while download config files", ex );
                parentDlgCtrl.getDialogComp().showBusy( false );
                String title = SystemManager.getInstance().getLocalizedMessage( "internalError_H", null, null );
                Properties params = new Properties();
                params.put( "exMsg", ex.getLocalizedMessage() );
                String text = SystemManager.getInstance().getLocalizedMessage( "internalError_TXT", null, params );
                parentDlgCtrl.showMessage( title, text );
                return;
              }
              parentDlgCtrl.getDialogComp().showBusy( false );
              String title = SystemManager.getInstance().getLocalizedMessage( "configBackupSuccess_H", null, null );
              Properties params = new Properties();
              params.put( "count", Integer.toString( count ) );
              params.put( "moduleCount", Integer.toString( moduleCount ) );
              String text = SystemManager.getInstance().getLocalizedMessage( "configBackupSuccess_TXT", null, params );
              parentDlgCtrl.showMessage( title, text );
            }
          }, "ConfigBackup" ).start();
        }
      }
    }
  }

  private int downloadConfigFiles( RailDevice railDevice, File backupDir ) {
    int count = 0;
    RailDeviceAddress deviceAddr = railDevice.getAddress();
    if (deviceAddr != null) {
      InetAddress inetAddress = deviceAddr.getInetAddress();
      if (inetAddress != null) {
        File deviceDir = new File( backupDir, toValidFileName( railDevice.getDisplayName() ) );
        deviceDir.mkdirs();
        if (saveUrl( inetAddress, deviceDir, "cfg.xml", false )) {
          count++;
        }
        if (saveUrl( inetAddress, deviceDir, "ioCfg.xml", false )) {
          count++;
        }
        if (saveUrl( inetAddress, deviceDir, "trackCfg.xml", true )) {
          count++;
        }
      }
    }
    return count;
  }

  private String toValidFileName( String name ) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < name.length(); i++) {
      char ch = name.charAt( i );
      if ((ch == '-') || (ch == '_') || (ch == '+')) {
        builder.append( ch );
      }
      else if (((ch >= '0') && ( ch <= '9')) || ((ch >= 'A') && ( ch <= 'Z')) || ((ch >= 'a') && ( ch <= 'z'))) {
        builder.append( ch );
      }
      else {
        builder.append( '_' );
      }
    }
    return builder.toString();
  }

  public boolean saveUrl( InetAddress inetAddress, File dir, String filename, boolean optional ) {
    BufferedInputStream in = null;
    FileOutputStream fout = null;
    try {
      String url = "http://" + inetAddress.getHostAddress() + "/" + filename;
      URI uri = URI.create( url );
      in = new BufferedInputStream( uri.toURL().openStream() );
      fout = new FileOutputStream( new File( dir, filename ) );

      final byte data[] = new byte[1024];
      int count;
      while ((count = in.read(data, 0, 1024)) != -1) {
        fout.write(data, 0, count);
      }
    }
    catch (FileNotFoundException fex) {
      if (!optional) {
        Logger.error( "Error downloading '"+filename+"' from "+inetAddress+" to "+dir.getAbsolutePath(), fex );
      }
      return false;
    }
    catch (IOException ex) {
      Logger.error( "Error downloading '"+filename+"' from "+inetAddress+" to "+dir.getAbsolutePath(), ex );
      return false;
    }
    finally {
      StreamHelper.close( in );
      StreamHelper.close( fout );
    }
    return true;
  }
}
