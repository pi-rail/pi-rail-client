/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ControllerGroup;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class ShowSignalTower extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    PiRailDelegate delegate = null;
    if (ctrlGroup instanceof PiRailDelegate) { //click on actionBar
      delegate = (PiRailDelegate) ctrlGroup;
    }
    else { //click on list item
      ControllerGroup moduleGroup = ctrlGroup.getParentGroup().getParentGroup();
      if (moduleGroup instanceof PiRailDelegate) {
        delegate = (PiRailDelegate) moduleGroup;
      }
    }

    if (delegate != null) {
      try {
        delegate.switchTab( PiRailDelegate.TURNOUT_FRAGMENT, delegate.getModelRailway() );
      }
      catch (Exception e) {
        e.printStackTrace();
      }

    }
  }
}
