package de.pidata.rail.client.editauto;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.model.*;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

public class EditAutomationDelegate extends EditCfgDelegate {

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.deviceId = parameterList.getDeviceId();
    this.ipAddress = parameterList.getIPAddress();
    this.actionCfgCtrl = (ModuleController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionCfgModule" ) );
    this.uiModel = new EditCfgUI( parameterList.getDeviceName(), deviceId, ipAddress );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.TRACK_CFG_XML, this, false, false );
    return uiModel;
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    //--- Modules must be laoded
    ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
    try {
      builder.loadModule( actionCfgCtrl, GuiBuilder.NAMESPACE.getQName( "actioncfg_list_module" ), actionCfgCtrl, "." );
    }
    catch (Exception e) {
      Logger.error( "Error loading modules" );
    }
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (CONFIG_DEFEKT.equals( result.getTitle() )) {
        if (resultOK) {
          Cfg cfg = new Cfg();
          uiModel.setCfg( cfg );
          initCfg( cfg );
        }
        else {
          dlgCtrl.close( false );
        }
      }
      else if (CONFIG_ERROR.equals( result.getTitle() )) {
        dlgCtrl.close( false );
      }
    }
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      Model config = configLoader.getConfigModel();
      if (config instanceof TrackCfg){
        TrackCfg trackCfg = (TrackCfg) config;
        uiModel.setTrackCfg( trackCfg );
        Cfg actionCfg = trackCfg.getActionCfg();
        if (actionCfg == null) {
          actionCfg = new Cfg();
          actionCfg.setId( trackCfg.getId() );
          trackCfg.setActionCfg( actionCfg );
        }
        uiModel.setCfg( (Cfg) actionCfg.clone( null, true, false ) );
        initCfg( uiModel.getCfg() );
      }
    }
    else {
      String text = SystemManager.getInstance().getLocalizedMessage( "editAutomationLoadError_TXT", null, null );
      dlgCtrl.showMessage( CONFIG_ERROR, text );
    }
  }

  private void initCfg( Cfg config ) {
    actionCfgCtrl.setModel( uiModel );
    for (ItemConn itemConn : config.itemConnIter()) {
      initItemConn( itemConn );
    }
  }
}
