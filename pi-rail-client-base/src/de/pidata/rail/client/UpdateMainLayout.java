package de.pidata.rail.client;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.MobileMainDelegate;
import de.pidata.service.base.ServiceException;

public class UpdateMainLayout extends GuiDelegateOperation<MobileMainDelegate> {

  private QName ONE_EVENT = ControllerBuilder.NAMESPACE.getQName( "onePanel" );
  private QName TWO_EVENT = ControllerBuilder.NAMESPACE.getQName( "twoPanel" );
  private QName THREE_EVENT = ControllerBuilder.NAMESPACE.getQName( "threePanel" );
  private QName ONE_THIRD_EVENT = ControllerBuilder.NAMESPACE.getQName( "oneThirdPanel" );
  private QName TWO_THIRD_EVENT = ControllerBuilder.NAMESPACE.getQName( "twoThirdPanel" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a table or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, MobileMainDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (eventID == ONE_EVENT) {
      delegate.updateLayout( true, false, false, false );
    }
    else if (eventID == TWO_EVENT) {
      delegate.updateLayout( true, true, false, false );
    }
    else if (eventID == THREE_EVENT) {
      delegate.updateLayout( true, true, true, false );
    }
    else if (eventID == ONE_THIRD_EVENT) {
      delegate.updateLayout( true, false, false, true );
    }
    else if (eventID == TWO_THIRD_EVENT) {
      delegate.updateLayout( false, false, true, true );
    }
    else {
      Logger.error( "Unsupported eventID in UpdateMainLayout: " + eventID );
    }
  }
}
