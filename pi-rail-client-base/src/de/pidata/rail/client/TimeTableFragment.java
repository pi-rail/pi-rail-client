package de.pidata.rail.client;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;

public class TimeTableFragment extends ModuleGroup {

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    setModel( modelRailway );
    super.activateModule( moduleContainer );
    Controller ctrl = getController( GuiBuilder.NAMESPACE.getQName( "playPauseTime" ) );
    ctrl.setValue( GuiBuilder.NAMESPACE.getQName( "icons/actions/Loco/button_stop.png" ) );
  }


}
