package de.pidata.rail.client.selectExtension;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TableController;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.AddConfigUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.EnumAction;
import de.pidata.rail.model.ExtCfg;
import de.pidata.rail.railway.Extension;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class SelectExtension extends GuiDelegateOperation<SelectExtensionDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, SelectExtensionDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    AddConfigUI model = (AddConfigUI) dataContext;
    QName newId = EnumAction.NAMESPACE.getQName( model.getId() );
    if (newId == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectExtensionNameEmpty_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectExtensionNameEmpty_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    TableController productTableCtrl = (TableController) sourceCtrl.getControllerGroup().getController( GuiBuilder.NAMESPACE.getQName( "extensionTable" ) );
    Extension selectedRow = (Extension) productTableCtrl.getSelectedRow( 0 );
    if (selectedRow == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectExtensionSelectExt_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectExtensionSelectExt_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    Integer busAddr = model.getBusAddr();
    if (busAddr == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectExtensionBusEmpty_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectExtensionBusEmpty_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    delegate.selected( newId, selectedRow, busAddr.intValue() );
  }
}
