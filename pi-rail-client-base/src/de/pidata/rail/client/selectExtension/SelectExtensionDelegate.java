package de.pidata.rail.client.selectExtension;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.AddConfigUI;
import de.pidata.rail.railway.Extension;
import de.pidata.rail.railway.ModelRailwayCatalog;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

public class SelectExtensionDelegate implements DialogControllerDelegate<SelectExtensionParamList, SelectExtensionParamList> {

  private DialogController dlgCtrl;
  private String storageType;
  private String catalogPath;
  private SelectExtensionParamList resultList;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectExtensionParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.resultList = parameterList;
    this.storageType = parameterList.getStorageType();
    this.catalogPath = parameterList.getCatalogPath();
    AddConfigUI model = new AddConfigUI();
    try {
      Storage storage;
      String fileName;
      int pos = catalogPath.lastIndexOf( "/" );
      if (pos > 0) {
        String dirPath = catalogPath.substring( 0, pos );
        fileName = catalogPath.substring( pos+1 );
        Logger.info( "Loading extensions from storage type="+storageType+", dir="+dirPath+", filename="+fileName );
        storage = SystemManager.getInstance().getStorage( storageType, dirPath );
      }
      else {
        fileName = catalogPath;
        Logger.info( "Loading extensions from storage type="+storageType+", filename="+fileName );
        storage = SystemManager.getInstance().getStorage( storageType);
      }
      Model catModel = XmlReader.loadData( storage, fileName );
      if (catModel instanceof ModelRailwayCatalog) {
        model.setModelRailwayCatalog( (ModelRailwayCatalog) catModel );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error loading extension from "+catalogPath );
    }
    return model;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {

  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {

  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {

  }

  public void selected( QName newId, Extension selectedRow, int busAddr ) {
    resultList = new SelectExtensionParamList( newId, storageType, catalogPath, selectedRow.getId(), busAddr );
    dlgCtrl.close( true );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectExtensionParamList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return resultList;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    //  do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
