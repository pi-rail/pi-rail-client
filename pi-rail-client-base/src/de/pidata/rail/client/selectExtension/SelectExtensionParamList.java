package de.pidata.rail.client.selectExtension;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectExtensionParamList extends AbstractParameterList {

  public static final QName EXTCFG_ID = ModelRailway.NAMESPACE.getQName( "extCfgID" );
  public static final QName STORAGE_TYPE = ModelRailway.NAMESPACE.getQName( "storageType" );
  public static final QName CATALOG_PATH = ModelRailway.NAMESPACE.getQName( "catalogPath" );
  public static final QName BUS_ADDRESS = ModelRailway.NAMESPACE.getQName( "busAddress" );
  public static final QName EXTENSION_ID = ModelRailway.NAMESPACE.getQName( "extensionID" );

  public SelectExtensionParamList() {
    super(5 );
    defineParam( 0, ParameterType.QNameType, EXTCFG_ID, null );
    defineParam( 1, ParameterType.StringType, STORAGE_TYPE, null );
    defineParam( 2, ParameterType.StringType, CATALOG_PATH, null );
    defineParam( 3, ParameterType.IntegerType, BUS_ADDRESS, null );
    defineParam( 4, ParameterType.QNameType, EXTENSION_ID, null );
  }

  public SelectExtensionParamList( String storageType, String catalogPath ) {
    this();
    setString( STORAGE_TYPE, storageType );
    setString( CATALOG_PATH, catalogPath );
  }

  public SelectExtensionParamList( QName itemID, String storageType, String catalogPath, QName extensionID, int busAddr ) {
    this();
    setQName( EXTCFG_ID, itemID );
    setString( STORAGE_TYPE, storageType );
    setString( CATALOG_PATH, catalogPath );
    setQName( EXTENSION_ID, extensionID );
    setInteger( BUS_ADDRESS, Integer.valueOf( busAddr ) );
  }

  public QName getExtcfgId() {
    return getQName( EXTCFG_ID );
  }

  public Integer getBusAddress() {
    return getInteger( BUS_ADDRESS );
  }

  public int getBusAddressInt() {
    Integer busAddr = getInteger( BUS_ADDRESS );
    if (busAddr == null) {
      return 0;
    }
    else {
      return busAddr.intValue();
    }
  }

  public QName getExtensionId() {
    return getQName( EXTENSION_ID );
  }

  public String getStorageType() {
    return getString( STORAGE_TYPE );
  }

  public String getCatalogPath() {
    return getString( CATALOG_PATH );
  }
}
