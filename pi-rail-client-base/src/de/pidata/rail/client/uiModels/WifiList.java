// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.client.uiModels;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Collection;
import java.util.Hashtable;

public class WifiList extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail-ui.xsd" );

  public static final QName ID_WIFINET = NAMESPACE.getQName( "wifiNet" );

  private final Collection<WifiNet> wifiNets = new ModelCollection<>( ID_WIFINET, this );

  public WifiList() {
    super( null, RailUiFactory.WIFILIST_TYPE, null, null, null );
  }

  public WifiList( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, RailUiFactory.WIFILIST_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected WifiList( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the wifiNet element identified by the given key.
   *
   * @return the wifiNet element identified by the given key
   */
  public WifiNet getWifiNet( Key wifiNetID ) {
    return (WifiNet) get( ID_WIFINET, wifiNetID );
  }

  /**
   * Adds the wifiNet element.
   *
   * @param wifiNet the wifiNet element to add
   */
  public void addWifiNet( WifiNet wifiNet ) {
    add( ID_WIFINET, wifiNet );
  }

  /**
   * Removes the wifiNet element.
   *
   * @param wifiNet the wifiNet element to remove
   */
  public void removeWifiNet( WifiNet wifiNet ) {
    remove( ID_WIFINET, wifiNet );
  }

  /**
   * Returns the wifiNet iterator.
   *
   * @return the wifiNet iterator
   */
  public ModelIterator<WifiNet> wifiNetIter() {
    return iterator( ID_WIFINET, null );
  }

  /**
   * Returns the number of wifiNets.
   *
   * @return the number of wifiNets
   */
  public int wifiNetCount() {
    return childCount( ID_WIFINET );
  }

  /**
   * Returns the wifiNet collection.
   *
   * @return the wifiNet collection
   */
  public Collection<WifiNet> getWifiNets() {
    return wifiNets;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public WifiNet getWifiNet( String ssid ) {
    for (WifiNet wifiNet : wifiNetIter()) {
      if (ssid.equals( wifiNet.getSsid() )) {
        return wifiNet;
      }
    }
    return null;
  }
}
