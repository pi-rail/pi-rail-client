package de.pidata.rail.client;

import de.pidata.gui.controller.base.ButtonController;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.system.base.SystemManager;

public class VersionInfoController extends ButtonController {

  @Override
  public void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );

    setValue( SystemManager.getInstance().getProgramVersion() + " - " + SystemManager.getInstance().getProgramDate());
  }

}
