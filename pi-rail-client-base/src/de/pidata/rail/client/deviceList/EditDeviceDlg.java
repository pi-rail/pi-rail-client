/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.deviceList;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.railway.RailDeviceAddress;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;

public class EditDeviceDlg extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    if (dataContext instanceof RailDevice) {
      RailDevice railDevice = (RailDevice) dataContext;
      Logger.info( "selected device: " + railDevice.getDisplayName() );
      String deviceType = railDevice.getDeviceType();
      InetAddress deviceAddr = null;
      RailDeviceAddress addr = railDevice.getAddress();
      if (addr == null) {
        RailDevice cfgDevice = railDevice.getConfigDevice();
        if (cfgDevice != null) {
          addr = cfgDevice.getAddress();
          railDevice = cfgDevice;
        }
      }
      if (addr != null) {
        EditCfgParamList parameterList = new EditCfgParamList( railDevice.getId(), railDevice.getDisplayName(), addr.getInetAddress(), deviceType );
        if (railDevice instanceof SwitchBox) {
          String title = SystemManager.getInstance().getLocalizedMessage( "editDeviceDlgSwitchbox_H", null, null );
          ctrlGroup.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_switchbox" ), title, parameterList );
        }
        else if (railDevice instanceof Locomotive) {
          String title = SystemManager.getInstance().getLocalizedMessage( "editDeviceDlgLoco_H", null, null );
          ctrlGroup.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_loco" ), title, parameterList );
        }
        else {
          String title = SystemManager.getInstance().getLocalizedMessage( "editDeviceDlgInit_H", null, null );
          ctrlGroup.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "init_device" ), title, parameterList );
        }
      }
      else {
        String title = SystemManager.getInstance().getLocalizedMessage( "editDeviceDlgUnknowAddr_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "editDeviceDlgUnknowAddr_TXT", null, null );
        showMessage( sourceCtrl, title, text );
      }
    }
  }
}
