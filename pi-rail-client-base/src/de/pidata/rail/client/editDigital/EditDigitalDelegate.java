package de.pidata.rail.client.editDigital;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.model.*;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EditDigitalDelegate extends EditCfgDelegate implements EventListener {

  protected TextController zentraleIpCtrl;
  protected ModuleController connCfgCtrl;
  protected SelectionController dccLocoCtrl;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    this.deviceId = parameterList.getDeviceId();
    this.ipAddress = parameterList.getIPAddress();
    this.zentraleIpCtrl = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "zentraleIP" ) );
    this.dccLocoCtrl = (SelectionController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "dccLoco" ) );
    this.connCfgCtrl = (ModuleController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "connCfgModule" ) );
    this.actionCfgCtrl = (ModuleController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionCfgModule" ) );
    this.uiModel = new EditCfgUI( parameterList.getDeviceName(), deviceId, ipAddress );
    IoCfg dummyIoCfg = new IoCfg();
    this.uiModel.setIoCfg( dummyIoCfg );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.TRACK_CFG_XML, this, false, false );
    return uiModel;
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    //--- Modules must be laoded
    ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
    try {
      builder.loadModule( connCfgCtrl, GuiBuilder.NAMESPACE.getQName( "conncfg_list_module" ), connCfgCtrl, "." );
      builder.loadModule( actionCfgCtrl, GuiBuilder.NAMESPACE.getQName( "actioncfg_list_module" ), actionCfgCtrl, "." );
    }
    catch (Exception e) {
      Logger.error( "Error loading modules" );
    }
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (CONFIG_DEFEKT.equals( result.getTitle() )) {
        if (resultOK) {
          Cfg cfg = new Cfg();
          uiModel.setCfg( cfg );
          initCfg( cfg );
        }
        else {
          dlgCtrl.close( false );
        }
      }
      else if (CONFIG_ERROR.equals( result.getTitle() )) {
        dlgCtrl.close( false );
      }
    }
  }

  public TextController getZentraleIpCtrl() {
    return zentraleIpCtrl;
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      Model config = configLoader.getConfigModel();
      if (config instanceof TrackCfg){
        TrackCfg trackCfg = (TrackCfg) config;
        uiModel.setTrackCfg( trackCfg );
        this.dccLocoCtrl.activate( this.dccLocoCtrl.getDialogController().getDialogComp() );
        Cfg digitalCfg = trackCfg.getDigital(null );
        if (digitalCfg != null) {
          initCfg( digitalCfg );
          this.dccLocoCtrl.selectRow( digitalCfg );
        }
        this.dccLocoCtrl.addListener( this );
      }
    }
    else {
      String text = SystemManager.getInstance().getLocalizedMessage( "editDigitalErrorLoad_TXT", null, null );
      dlgCtrl.showMessage( CONFIG_ERROR, text );
    }
  }

  public PortCfg addZ21Port( QName ipAddr ) {
    String zentraleIpStr = ipAddr.getName();
    if (Helper.isNullOrEmpty( zentraleIpStr )) {
      String title = SystemManager.getInstance().getLocalizedMessage( "editDigitalIPInvalid_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editDigitalIPInvalid_TXT", null, null );
      dlgCtrl.showMessage( title, text );
      return null;
    }
    try {
      InetAddress zentraleIP = InetAddress.getByName( zentraleIpStr );
    }
    catch (UnknownHostException e) {
      String title = SystemManager.getInstance().getLocalizedMessage( "editDigitalIPInvalid_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editDigitalIPInvalid_TXT", null, null );
      dlgCtrl.showMessage( title, text + "\nmsg=" + e.getLocalizedMessage() );
      return null;
    }
    PortCfg z21PortCfg = new PortCfg( ipAddr );
    z21PortCfg.setType( PortType.CustomPort );
    uiModel.getIoCfg().addPortCfg( z21PortCfg );
    z21PortCfg.setCfg( uiModel.getCfg() );
    z21PortCfg.refreshExt();
    return z21PortCfg;
  }

  private void initCfg( Cfg digitalCfg ) {
    Cfg config = (Cfg) digitalCfg.clone( null, true, false );
    uiModel.setCfg( config );
    for (ExtCfg extCfg : config.extCfgIter()) {
      addZ21Port( extCfg.getPort() );
    }
    for (ItemConn itemConn : config.itemConnIter()) {
      initItemConn( itemConn );
    }
    connCfgCtrl.setModel( uiModel );
    actionCfgCtrl.setModel( uiModel );
  }

  public void updateDigitalCfg( EditCfgUI uiModel, TrackCfg trackCfg ) {
    Cfg currentCfg = uiModel.getCfg();
    if (currentCfg != null) {
      Cfg oldDigital = trackCfg.findDigital( currentCfg.getId() );
      if (oldDigital == null) {
        trackCfg.addDigital( (Cfg) currentCfg.clone( null, true, false ) );
      }
      else {
        oldDigital.removeAll( null );
        for (Object child : currentCfg.iterator( null, null )) {
          Model newChild = (Model) child;
          oldDigital.add( newChild.getParentRelationID(), newChild.clone( null, true, false ) );
        }
      }
    }
  }

  private void switchActiveLoco( Cfg digitalCfg ) {
    connCfgCtrl.setModel( null );
    actionCfgCtrl.setModel( null );
    updateDigitalCfg( uiModel, uiModel.getTrackCfg() );
    uiModel.getIoCfg().removeAll( IoCfg.ID_PORTCFG );
    initCfg( digitalCfg );
  }

  public void addZ21Lok( String name ) {
    Cfg digitalCfg = new Cfg();
    digitalCfg.setId( PiRailFactory.NAMESPACE.getQName( name ) );
    uiModel.getTrackCfg().addDigital( digitalCfg );
    switchActiveLoco( digitalCfg );
    this.dccLocoCtrl.selectRow( digitalCfg );
  }

  public void renameZ21Port( String inputValue ) {
    uiModel.getIoCfg().removeAll( IoCfg.ID_PORTCFG );
    PortCfg newPort = addZ21Port( PiRailFactory.NAMESPACE.getQName( inputValue ) );
    ExtCfg extCfg = uiModel.getCfg().getExtCfg( null );
    extCfg.setPort( newPort.getId() );
  }

  public void deleteZ21Port() {
    connCfgCtrl.setModel( null );
    actionCfgCtrl.setModel( null );
    uiModel.getCfg().removeAll( null );
    uiModel.getIoCfg().removeAll( IoCfg.ID_PORTCFG );
    connCfgCtrl.setModel( uiModel );
    actionCfgCtrl.setModel( uiModel );
  }

  public void deleteZ21Loco() {
    connCfgCtrl.setModel( null );
    actionCfgCtrl.setModel( null );
    uiModel.getCfg().removeAll( null );
    uiModel.getIoCfg().getPortCfg( null ).refreshExt();
    connCfgCtrl.setModel( uiModel );
    actionCfgCtrl.setModel( uiModel );
  }

  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (source == this.dccLocoCtrl) {
      if (newValue != null) {
        switchActiveLoco( (Cfg) newValue );
      }
    }
  }
}
