package de.pidata.rail.client.editDigital;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Cfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddDCCLoco extends GuiDelegateOperation<EditDigitalDelegate> {

  private String title = "";

  @Override
  protected void execute( QName eventID, EditDigitalDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    this.title = SystemManager.getInstance().getLocalizedMessage( "addDCCLocoName_H", null, null );
    String text = SystemManager.getInstance().getLocalizedMessage( "addDCCLocoName_TXT", null, null );
    String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
    String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
    showInput( sourceCtrl, this.title, text,"", btnOk, btnCancel );
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && title.equals( result.getTitle() )) {
        String name = result.getInputValue();
        String msg = Cfg.checkID( name );
        if (msg != null) {
          Properties params = new Properties();
          params.put( "name", name );
          this.title = SystemManager.getInstance().getLocalizedMessage( "addDCCLocoInvalid_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addDCCLocoInvalid_TXT", null, null );
          parentDlgCtrl.showMessage( title, text + "\n" + msg );
          return;
        }
        ((EditDigitalDelegate) parentDlgCtrl.getDelegate()).addZ21Lok( name );
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
