package de.pidata.rail.client.editDigital;

import de.pidata.gui.controller.base.Controller;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.ExtCfg;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.track.TrackCfg;
import de.pidata.rail.z21.Z21Config;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EditDigitalUpload extends SaveCfgOperation<EditDigitalDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditDigitalDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditCfgUI uiModel = (EditCfgUI) dataContext;
    if (uiModel == null) {
      Logger.info( "Could not upload SwitchBox configuration, UI Model is null" );
      return;
    }
    TrackCfg trackCfg = uiModel.getTrackCfg();
    if (trackCfg == null) {
      Logger.info( "Could not upload SwitchBox configuration, TrackCfg is null" );
      return;
    }
    delegate.updateDigitalCfg( uiModel, trackCfg );

    source.getDialogController().getDialogComp().showBusy( true );
    new Thread( new Runnable() {
      @Override
      public void run() {
        if (uploadTrackCfg( uiModel.getInetAddress(), trackCfg, source.getDialogController() )) {
          source.getDialogController().close( true );
        }
        source.getDialogController().getDialogComp().showBusy( false );
      }
    } ).start();
  }
}
