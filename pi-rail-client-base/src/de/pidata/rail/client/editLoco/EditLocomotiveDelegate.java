/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.editLoco;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.rail.client.editcfg.AbstractEditConfig;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

public class EditLocomotiveDelegate extends AbstractEditConfig {

  private Locomotive locomotive;

  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.deviceId = parameterList.getDeviceId();
    this.deviceAddress = parameterList.getIPAddress();
    ModelRailway model = PiRail.getInstance().getModelRailway();
    locomotive = model.getLoco( deviceId );
    return model.getLoco( deviceId );
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    TextController controller = (TextController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "PicText" ) );
    controller.setValue( "icon.jpg" );
    updaterOutput = (TextEditorController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "updaterOutput" ) );
    locomotive.setDataListener( this );
  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    locomotive.setDataListener( null );
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
