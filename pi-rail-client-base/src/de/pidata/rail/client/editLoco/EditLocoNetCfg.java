/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.editLoco;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditLocoNetCfg extends GuiDelegateOperation<EditLocomotiveDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditLocomotiveDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive locomotive = (Locomotive) dataContext;
    EditCfgParamList parameterList = new EditCfgParamList( locomotive.getId(), locomotive.getDisplayName(), locomotive.getAddress().getInetAddress(), "Loco" );
    String title = SystemManager.getInstance().getLocalizedMessage( "editLocoNetCfg_H", null, null );
    sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_wificfg" ), title, parameterList );
  }
}
