package de.pidata.rail.client.blocks;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ActionGroup;
import de.pidata.rail.railway.ModelRailway;

public class BlockListFragment extends ModuleGroup {

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   * @param moduleContainer
   */
  @Override
  public void activateModule( UIContainer moduleContainer ) {
    ActionGroup blockGroup = PiRail.getInstance().getModelRailway().getOrCreateActionGroup( ModelRailway.GROUP_BLOCK );
    setModel( blockGroup );
    super.activateModule( moduleContainer );
  }

  @Override
  public synchronized void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
  }
}
