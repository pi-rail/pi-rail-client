/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.ListController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ActionGroup;
import de.pidata.rail.railway.ModelRailway;

public class TrackActionFragment extends ModuleGroup {

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    setModel( modelRailway );
    ListController turnoutGroup = (ListController) getController( GuiBuilder.NAMESPACE.getQName( "turnoutGroup" ) );
    ActionGroup actionGroup = modelRailway.getOrCreateActionGroup( ModelRailway.GROUP_SIGNAL_TOWER );
    turnoutGroup.selectRow( actionGroup );
    super.activateModule( moduleContainer );
  }
}
