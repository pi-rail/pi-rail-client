package de.pidata.rail.client.ioboard;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;

public class IOAction extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if ((dataContext != null) && (dataContext instanceof SwitchBox)) {
      SwitchBox switchBox = (SwitchBox) dataContext;
      String evName = eventID.getName();
      String id = evName.substring( 0, evName.length()-2 );
      char value = evName.charAt( evName.length()-1 );
      throw new RuntimeException("TODO");
      /*Function function = switchBox.getFunction( id );
      if (function != null) {
        PiRail.getInstance().sendSwitchCommand( function, value );
      } */
    }
  }
}
