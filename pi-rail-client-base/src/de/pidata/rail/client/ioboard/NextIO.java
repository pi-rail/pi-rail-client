package de.pidata.rail.client.ioboard;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;

public class NextIO extends GuiOperation {

  public static final QName PREV_IO = NAMESPACE.getQName( "PrevIO" );
  public static final QName NEXT_IO = NAMESPACE.getQName( "NextIO" );


  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    if (eventID == PREV_IO) {
      SwitchBox tower = (SwitchBox) dataContext;
      SwitchBox prevTower;
      if (tower == null) {
        prevTower = (SwitchBox) modelRailway.lastChild( ModelRailway.ID_SWITCHBOX );
      }
      else {
        prevTower = (SwitchBox) tower.prevSibling( tower.getParentRelationID() );
        if (prevTower == null) {
          prevTower = (SwitchBox) modelRailway.lastChild( ModelRailway.ID_SWITCHBOX );
        }
      }
      if (prevTower != null) {
        sourceCtrl.getControllerGroup().setModel( prevTower );
      }
    }
    else if (eventID == NEXT_IO) {
      SwitchBox tower = (SwitchBox) dataContext;
      SwitchBox nextTower;
      if (tower == null) {
        nextTower = (SwitchBox) modelRailway.firstChild( ModelRailway.ID_SWITCHBOX );
      }
      else {
        nextTower = (SwitchBox) tower.nextSibling( tower.getParentRelationID() );
        if (nextTower == null) {
          nextTower = (SwitchBox) modelRailway.firstChild( ModelRailway.ID_SWITCHBOX );
        }
      }
      if (nextTower != null) {
        sourceCtrl.getControllerGroup().setModel( nextTower );
      }
    }
  }
}
