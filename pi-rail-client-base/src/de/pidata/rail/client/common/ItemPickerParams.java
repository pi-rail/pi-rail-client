package de.pidata.rail.client.common;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class ItemPickerParams extends AbstractParameterList {

  public static final QName NAME = ModelRailway.NAMESPACE.getQName( "name" );
  public static final QName ITEM_RELATION_ID = ModelRailway.NAMESPACE.getQName( "itemRelationID" );
  public static final QName SELECTED_ID = ModelRailway.NAMESPACE.getQName( "selectedID" );
  public static final QName ATTRIBUTE_NAME = ModelRailway.NAMESPACE.getQName( "attributeName" );

  public ItemPickerParams() {
    super( ParameterType.StringType, NAME, ParameterType.QNameType, ITEM_RELATION_ID,
           ParameterType.QNameType, SELECTED_ID, ParameterType.QNameType, ATTRIBUTE_NAME );
  }

  public ItemPickerParams( String name, QName itemRelationID, QName selectedID, QName attributeName ) {
    this();
    setString( NAME, name );
    setQName( ITEM_RELATION_ID, itemRelationID );
    setQName( SELECTED_ID, selectedID );
    setQName( ATTRIBUTE_NAME, attributeName );
  }

  public String getName() {
    return getString( NAME );
  }

  public QName getItemRelationID() {
    return getQName( ITEM_RELATION_ID );
  }

  public QName getSelectedID() {
    return getQName( SELECTED_ID );
  }

  public QName getAttributeName() {
    return getQName( ATTRIBUTE_NAME );
  }

  public void setSelectedID( QName selectedID ) {
    setQName( SELECTED_ID, selectedID );
  }
}
