package de.pidata.rail.client.common;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.ItemPickerUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailwayFactory;
import de.pidata.service.base.ParameterList;

public class ItemPickerDelegate implements DialogControllerDelegate<ItemPickerParams,ItemPickerParams> {

  private ItemPickerUI itemPickerUI;
  private ItemPickerParams parameterList;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ItemPickerParams parameterList ) throws Exception {
    this.parameterList = parameterList;
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    itemPickerUI = new ItemPickerUI();
    itemPickerUI.addItems( modelRailway.iterator( parameterList.getItemRelationID(), null ) );
    Model item = itemPickerUI.getItem( parameterList.getSelectedID() );
    if (item == null) {
      itemPickerUI.setSelectedID( null );
    }
    else {
      itemPickerUI.setSelectedID( (QName) item.key() );
    }
    return itemPickerUI;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ItemPickerParams dialogClosing( DialogController dlgCtrl, boolean ok ) {
    if (ok) {
      TableController itemTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "itemTable" ) );
      Model selItem = itemTable.getSelectedRow( 0 );
      if (selItem == null) {
        parameterList.setSelectedID( null );
      }
      else {
        parameterList.setSelectedID( (QName) selItem.get( RailwayFactory.NAMESPACE.getQName( "id" ) ) );
      }
    }
    return parameterList;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
