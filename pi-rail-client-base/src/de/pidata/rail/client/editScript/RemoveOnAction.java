package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.model.TriggerAction;
import de.pidata.service.base.ServiceException;

public class RemoveOnAction extends GuiDelegateOperation<EditCfgDelegate> {

  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TriggerAction triggerAction = (TriggerAction) dataContext;
    triggerAction.setOnAction( null );
    triggerAction.setOnDevice( null );
    triggerAction.setOnSrcID( null );
  }
}
