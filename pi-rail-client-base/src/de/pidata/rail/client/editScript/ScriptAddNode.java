package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.editiocfg.SelectCmdTypeParams;
import de.pidata.rail.client.editiocfg.SelectNameIconParams;
import de.pidata.rail.model.*;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class ScriptAddNode extends GuiDelegateOperation<EditCfgDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController scriptTree = (TreeController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "scriptTree" ) );
    Model selectedModel = scriptTree.getSelectedNode().getNodeModel();
    if ((selectedModel instanceof StateScript) || (selectedModel instanceof IfCmd)) {
      String title = SystemManager.getInstance().getLocalizedMessage( "scriptAddNodeCommand_H", null, null );
      openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_cmd_type" ), title, AbstractParameterList.EMPTY );
    }
    else if (selectedModel instanceof EnumAction) {
      EnumAction enumAction = (EnumAction) selectedModel;
      SelectNameIconParams params = new SelectNameIconParams( enumAction.getType(), null );
      String title = SystemManager.getInstance().getLocalizedMessage( "scriptAddNodeRunningTrack_H", null, null );
      openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "select_name_icon" ), title, params );
    }
    else if (selectedModel instanceof TriggerAction) {
      StateScript newScript = new StateScript();
      newScript.setId( NAMESPACE.getQName( "new" ) );
      ((TriggerAction) selectedModel).addOnState( newScript );
    }
    else if (selectedModel instanceof TimerAction) {
      StateScript stateScript = ((TimerAction) selectedModel).getOnState();
      if (stateScript == null) {
        StateScript newScript = new StateScript();
        newScript.setId( NAMESPACE.getQName( "onTimer" ) );
        ((TimerAction) selectedModel).setOnState( newScript );
      }
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    EditCfgDelegate delegate = (EditCfgDelegate) parentDlgCtrl.getDelegate();
    if (resultOK) {
      if (resultList instanceof SelectNameIconParams) {
        Action action = delegate.getSelectedAction();
        if (action instanceof EnumAction) {
          addStateScript( (EnumAction) action, (SelectNameIconParams) resultList );
        }
      }
      else if (resultList instanceof SelectCmdTypeParams) {
        QName selectedID = ((SelectCmdTypeParams) resultList).getSelectedID();
        if (selectedID != null) {
          TreeController scriptTree = (TreeController) parentDlgCtrl.getController( NAMESPACE.getQName( "scriptTree" ) );
          Model selectedModel = scriptTree.getSelectedNode().getNodeModel();
          Model newCmd = null;
          if (selectedID == StateScript.ID_SETIO) {
            newCmd = new SetIO();
          }
          else if (selectedID == StateScript.ID_SET_CHAR) {
            newCmd = new SetCmd( ValueType.charVal );
            selectedID = StateScript.ID_SET;
          }
          else if (selectedID == StateScript.ID_SET_INT) {
            newCmd = new SetCmd( ValueType.intVal );
            selectedID = StateScript.ID_SET;
          }
          else if (selectedID == StateScript.ID_CALL) {
            newCmd = new CallCmd();
          }
          else if (selectedID == StateScript.ID_IF) {
            newCmd = new IfCmd();
          }
          else if (selectedID == StateScript.ID_DELAY) {
            newCmd = new DelayCmd();
          }
          else if (selectedID == StateScript.ID_ON) {
            newCmd = new MsgState();
          }
          if (newCmd != null) {
            selectedModel.add( selectedID, newCmd );
            scriptTree.setSelectedModel( newCmd );
          }
        }
      }
    }
  }

  public static void addStateScript( EnumAction enumAction, SelectNameIconParams resultList ) {
    QName name = resultList.getSelectedID();
    if (name == null) {
      StateScript newScript = new StateScript();
      newScript.setId( NAMESPACE.getQName( "new" ) );
      enumAction.addOnState( newScript );
    }
    else {
      int count = enumAction.onStateCount();
      char ch = (char) ('A' + count);
      StateScript stateScript = new StateScript( name, ch, count+1 );
      enumAction.addOnState( stateScript );
    }
  }
}
