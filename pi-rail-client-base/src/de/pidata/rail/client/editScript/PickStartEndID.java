package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.ActionPickerParamList;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class PickStartEndID extends GuiDelegateOperation<EditCfgDelegate> {

  private QName eventID;
  private StateScript stateScript;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    EnumAction enumAction;
    QName currentID = null;
    String title;
    this.eventID = eventID;
    this.stateScript = null;
    if (eventID == NAMESPACE.getQName("btnEditStart")) {
      enumAction = (EnumAction) dataContext;
      TrackPos startPos = enumAction.getStartPos();
      if (startPos != null) {
        currentID = startPos.getPosID();
      }
      title = SystemManager.getInstance().getLocalizedMessage( "pickStartEndIDStartBlock_TXT", null, null );
    }
    else {
      stateScript = (StateScript) dataContext;
      enumAction = (EnumAction) stateScript.getParent( false );
      currentID = stateScript.getEndPos();
      title = SystemManager.getInstance().getLocalizedMessage( "pickStartEndIDEndBlock_TXT", null, null );
    }
    ActionPickerParamList paramList = new ActionPickerParamList( enumAction.getId(), ModelRailway.GROUP_BLOCK, null, currentID );
    openChildDialog( sourceCtrl, NAMESPACE.getQName( "action_picker" ), title, paramList );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof ActionPickerParamList) {
      if (resultOK) {
        ActionPickerParamList result = (ActionPickerParamList) resultList;
        EditCfgUI model = (EditCfgUI) parentDlgCtrl.getModel();
        EnumAction enumAction = model.getCfg().getFunc( result.getSelectedActionId() );
        if (eventID == NAMESPACE.getQName("btnEditStart")) {
          TrackPos startPos = enumAction.getStartPos();
          if (startPos == null) {
            startPos = new TrackPos();
            enumAction.setStartPos( startPos );
          }
          startPos.setPosID( result.getReferencedActionId() );
        }
        else {
          stateScript.setEndPos( result.getReferencedActionId() );
        }
      }
    }
    this.eventID = null;
  }
}

