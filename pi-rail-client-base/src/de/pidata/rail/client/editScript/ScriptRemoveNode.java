package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.model.Action;
import de.pidata.service.base.ServiceException;

public class ScriptRemoveNode extends GuiDelegateOperation<EditCfgDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TreeController scriptTree = (TreeController) sourceCtrl.getControllerGroup().getController( NAMESPACE.getQName( "scriptTree" ) );
    Model selectedModel = scriptTree.getSelectedNode().getNodeModel();
    if (!(selectedModel instanceof Action)) {
      selectedModel.getParent( false ).remove( selectedModel.getParentRelationID(), selectedModel );
    }
  }
}
