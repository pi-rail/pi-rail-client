package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.editiocfg.SelectParamValueDelegate;
import de.pidata.rail.client.editiocfg.SelectParamValueParams;
import de.pidata.rail.model.Param;
import de.pidata.rail.model.SetCmd;
import de.pidata.rail.model.ValueType;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class SetCmdEditParam extends GuiDelegateOperation<EditCfgDelegate> {

  private SetCmd setCmd;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    ControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    Model selModel = ctrlGroup.getModel();
    if (selModel instanceof SetCmd) {
      setCmd = (SetCmd) selModel;
      QName paramID = setCmd.getId();
      if (paramID == null) {
        String title = SystemManager.getInstance().getLocalizedMessage( "setCmdEditParamMissing_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "setCmdEditParamMissing_TXT", null, null );
        showMessage( sourceCtrl, title, text );
      }
      else if (paramID == Param.PARAM_CMD) {
        SelectParamValueParams params = new SelectParamValueParams( SelectParamValueDelegate.PARAM_CMD_EXT, setCmd.getValue() );
        String title = SystemManager.getInstance().getLocalizedMessage( "setCmdEditParamSelectCmd_H", null, null );
        openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_param_value" ), title, params );
      }
      else {
        Properties params = new Properties();
        params.put( "name", paramID.getName() );
        params.put( "type", ValueType.charVal );
        String title = SystemManager.getInstance().getLocalizedMessage( "setCmdEditParamEdit_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "setCmdEditParamEdit_TXT", null, params );
        String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
        String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
        showInput( sourceCtrl, title, text, setCmd.getValue(), btnOk, btnCancel );
      }
    }
    else {
      setCmd = null;
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK) {
      if (setCmd != null) {
        if (resultList instanceof QuestionBoxResult) {
          setCmd.setValue( ((QuestionBoxResult) resultList).getInputValue() );
        }
        else if (resultList instanceof SelectParamValueParams) {
          String selectedValue =  ((SelectParamValueParams) resultList).getSelectedValue();
          setCmd.setValue( selectedValue );
        }
      }
    }
    setCmd = null;
  }
}

