package de.pidata.rail.client.editScript;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.SelectNameIconParams;
import de.pidata.rail.model.*;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class EditScriptID extends GuiOperation {

  private String title = "";

  private StateScript stateScript;

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (dataContext instanceof StateScript) {
      this.stateScript = (StateScript) dataContext;
      Model owner = stateScript.getParent( false );
      if (owner instanceof EnumAction) {
        EnumAction enumAction = (EnumAction) owner;
        SelectNameIconParams params = new SelectNameIconParams( enumAction.getType(), null );
        this.title = SystemManager.getInstance().getLocalizedMessage( "editScriptIDRename_H", null, null );
        openChildDialog( sourceCtrl, ControllerBuilder.NAMESPACE.getQName( "select_name_icon" ), this.title, params );
      }
      else {
        this.title = SystemManager.getInstance().getLocalizedMessage( "editScriptIDRename_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "editScriptIDRename_TXT", null, null );
        String btnOk = SystemManager.getInstance().getGlossaryString( "take" );
        String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
        showInput( sourceCtrl, this.title, text, stateScript.getId().getName(), btnOk, btnCancel );
      }
    }
    else {
      this.stateScript = null;
    }
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK && (stateScript != null)) {
      if (resultList instanceof SelectNameIconParams) {
        QName id = ((SelectNameIconParams) resultList).getSelectedID();
        if (id != null) {
          stateScript.setId( id );
        }
      }
      else if (resultList instanceof QuestionBoxResult) {
        QuestionBoxResult result = (QuestionBoxResult) resultList;
        if (title.equals( result.getTitle() )) {
          String scriptName = ((QuestionBoxResult) resultList).getInputValue();
          String msg = StateScript.checkName( scriptName );
          if (msg != null) {
            Properties params = new Properties();
            params.put( "name", scriptName );
            String title = SystemManager.getInstance().getLocalizedMessage( "editScriptIDInvalidName_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "editScriptIDInvalidName_TXT", null, params );
            parentDlgCtrl.showMessage( title, text + "\n" + msg );
            stateScript = null;
            return;
          }
          QName id = PiRailFactory.NAMESPACE.getQName( scriptName );
          stateScript.setId( id );
          Model owner = stateScript.getParent( false );
          if (owner instanceof EnumAction) {
            FunctionType functionType = ((EnumAction) owner).getType();
            String iconName = stateScript.getIconName( functionType, "active" );
            QName imageID = GuiBuilder.NAMESPACE.getQName( "icons/actions/" + iconName + ".png" );
            ComponentBitmap bitmap = de.pidata.gui.component.base.Platform.getInstance().getBitmap( imageID );
            if (bitmap == null) {
              Properties params = new Properties();
              params.put( "name", scriptName );
              String title = SystemManager.getInstance().getLocalizedMessage( "editScriptIDNoIcon_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "editScriptIDNoIcon_TXT", null, params );
              parentDlgCtrl.showMessage( title, text );
            }
          }
        }
      }
    }
    stateScript = null;
  }
}
