package de.pidata.rail.client.editScript;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.ActionPickerParamList;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.TriggerAction;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.RailMessage;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class PickSrcID extends GuiDelegateOperation<EditCfgDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditCfgDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TriggerAction triggerAction = (TriggerAction) dataContext;
    ActionPickerParamList paramList = new ActionPickerParamList( triggerAction.getId(), ModelRailway.GROUP_BALISE, triggerAction.getOnDevice(), triggerAction.getOnSrcID() );
    String title = SystemManager.getInstance().getLocalizedMessage( "pickSrcIDSelect_H", null, null );
    openChildDialog( sourceCtrl, NAMESPACE.getQName( "action_picker" ), title, paramList );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof ActionPickerParamList) {
      if (resultOK) {
        ActionPickerParamList result = (ActionPickerParamList) resultList;
        QName triggerActionID = result.getSelectedActionId();
        QName selectedID = result.getReferencedActionId();
        if ((triggerActionID != null) && (selectedID != null)) {
          EditCfgUI model = (EditCfgUI) parentDlgCtrl.getModel();
          TriggerAction triggerAction = model.getCfg().getTrigger( triggerActionID );
          ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
          RailAction refAction = modelRailway.getRailAction( null, selectedID );
          if (refAction instanceof RailMessage) {
            triggerAction.setOnSrcID( ((RailMessage) refAction).getPosID() );
          }
          else {
            triggerAction.setOnSrcID( selectedID );
          }
          if ((triggerAction.getOnAction() == null) && (triggerAction.getOnDevice() == null)) {
            triggerAction.setOnDevice( TriggerAction.ON_DEVICE_ANY );
          }
        }
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
