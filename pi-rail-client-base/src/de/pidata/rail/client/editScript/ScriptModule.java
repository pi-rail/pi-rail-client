package de.pidata.rail.client.editScript;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editauto.EditAutomationDelegate;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.model.*;
import de.pidata.string.Helper;

public class ScriptModule extends ModuleGroup implements EventListener {

  private TreeController scriptTreeCtrl;
  private ModuleController detailsModule;

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    super.activateModule( moduleContainer );
    this.detailsModule = (ModuleController) getController( GuiBuilder.NAMESPACE.getQName( "scriptDetails" ) );
    this.scriptTreeCtrl = (TreeController) getController( GuiBuilder.NAMESPACE.getQName( "scriptTree" ) );
    this.scriptTreeCtrl.addListener( this );
  }

  @Override
  public synchronized void deactivateModule() {
    super.deactivateModule();
    if (this.scriptTreeCtrl != null) {
      this.scriptTreeCtrl.removeListener( this );
      this.scriptTreeCtrl = null;
    }
    this.detailsModule = null;
  }

  @Override
  public void deactivate( boolean saveValue ) {
    super.deactivate( true );
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    QName moduleID = null;
    if (newValue instanceof StateScript) {
      moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_statescript_module" );
    }
    else if (newValue instanceof SetIO) {
      moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_set_io_module" );
    }
    else if (newValue instanceof SetCmd) {
      SetCmd  setCmd = (SetCmd) newValue;
      if (setCmd.isIntValue()) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_setint_module" );
      }
      else {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_set_module" );
      }
    }
    else if (newValue instanceof DelayCmd) {
      moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_delay_module" );
    }
    else if (newValue instanceof IfCmd) {
      moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_if_module" );
    }
    else if (newValue instanceof CallCmd) {
      DialogControllerDelegate dcd = getDialogController().getDelegate();
      if (dcd instanceof EditAutomationDelegate) {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_call_global" );
      }
      else {
        moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_call_module" );
      }
    }
    else if (newValue instanceof MsgState) {
      moduleID = ControllerBuilder.NAMESPACE.getQName( "edit_on_module" );
    }
    try {
      if (moduleID == null) {
        detailsModule.setModuleGroup( null, ViewAnimation.NONE );
      }
      else {
        // Since we are having different modules for different selected Models we must not use scriptTreeCtrl as modelSource,
        // because that will lead to inconsistency between Module and Model while processing tree's selection event.
        detailsModule.getDialogController().getControllerBuilder().loadModule( detailsModule, moduleID, (Model) newValue );
      }
    }
    catch (Exception e) {
      Logger.error( "Error updating details module", e );
    }
  }
}
