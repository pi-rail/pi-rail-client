/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.wifi;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.ConfigLoaderListener;
import de.pidata.rail.model.NetCfg;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.net.InetAddress;

public class EditWifiDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, ConfigLoaderListener {

  private EditCfgUI uiModel;
  private DialogController dlgCtrl;

  public EditCfgUI getUiModel() {
    return uiModel;
  }

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    QName deviceId = parameterList.getDeviceId();
    InetAddress ipAddress = parameterList.getIPAddress();
    this.uiModel = new EditCfgUI( parameterList.getDeviceName(), deviceId, ipAddress );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.NET_CFG_XML, this, false, false );
    return uiModel;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {

  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      uiModel.setNetCfg( (NetCfg) configLoader.getConfigModel() );
      dlgCtrl.activate( dlgCtrl.getDialogComp() );
    }
  }
}
