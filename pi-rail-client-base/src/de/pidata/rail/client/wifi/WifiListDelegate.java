/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.wifi;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.TextController;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.NetCfg;
import de.pidata.rail.model.WifiCfg;
import de.pidata.system.base.SystemManager;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.rail.client.uiModels.WifiList;
import de.pidata.rail.client.uiModels.WifiNet;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.util.List;

public class WifiListDelegate implements DialogControllerDelegate, Runnable {

  private WifiList wifiList;
  private boolean running;
  private boolean paused;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ParameterList parameterList ) throws Exception {
    wifiList = new WifiList();
    NetCfg netCfg = PiRail.getInstance().getModelRailway().getNetCfg();
    String ssid = null;
    String passwd = null;
    if (netCfg != null) {
      WifiCfg wifiCfg = netCfg.getWifiCfg( null );
      if (wifiCfg != null) {
        ssid = wifiCfg.getSsid();
        passwd = wifiCfg.getPass();
      }
    }
    TextController textSsid = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "railSSID" ) );
    textSsid.setValue( ssid );
    TextController textPwd = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "railPassword" ) );
    textPwd.setValue( passwd );
    TextController textName = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "railHostname" ) );
    textName.setValue( null );

    new Thread( this ).start();
    return wifiList;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    running = false;
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public void setPaused( boolean paused ) {
    this.paused = paused;
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used
   * to create a thread, starting the thread causes the object's
   * <code>run</code> method to be called in that separately executing
   * thread.
   * <p>
   * The general contract of the method <code>run</code> is that it may
   * take any action whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    running = true;
    paused = false;
    while (running) {
      if (!paused) {
        try {
          List<String> scanResult = SystemManager.getInstance().getWifiManager().scanWiFi();
          for (String ssid : scanResult) {
            WifiNet wifiNet = wifiList.getWifiNet( ssid );
            if (wifiNet == null) {
              wifiNet = new WifiNet( ssid );
              wifiList.addWifiNet( wifiNet );
            }
          }
        }
        catch (Exception ex) {
          Logger.error( "Error scanning WiFi" );
        }
      }
      try {
        Thread.sleep( 3000 );
      }
      catch (InterruptedException e) {
        Logger.info( "Scanning WiFi interrupted" );
        running = false;
      }
    }
  }
}
