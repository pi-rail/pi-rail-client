/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.wifi;

import de.pidata.gui.component.base.Platform;
import de.pidata.models.tree.Model;
import de.pidata.rail.comm.PiRailComm;
import de.pidata.system.base.SystemManager;
import de.pidata.gui.controller.base.*;
import de.pidata.log.Logger;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.WifiNet;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.PiRailComm;
import de.pidata.rail.model.NetCfg;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.service.base.ServiceException;

import java.net.InterfaceAddress;
import java.util.List;
import java.net.InetAddress;
import java.util.Properties;

public class DeviceWifiSetup extends GuiDelegateOperation<WifiListDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, WifiListDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = source.getControllerGroup();
    TableController wifiTab = (TableController) ctrlGroup.getController( NAMESPACE.getQName( "wifiTab" ) );
    WifiNet wifiNet = (WifiNet) wifiTab.getSelectedRow( 0 );
    if (wifiNet != null) {
      delegate.setPaused( true ); // stop WiFi scanning
      TextController textSsid = (TextController) ctrlGroup.getController( NAMESPACE.getQName( "railSSID" ) );
      TextController textPwd = (TextController) ctrlGroup.getController( NAMESPACE.getQName( "railPassword" ) );
      TextController textName = (TextController) ctrlGroup.getController( NAMESPACE.getQName( "railHostname" ) );
      NetCfg netCfg = new NetCfg( textSsid.getStringValue(), textPwd.getStringValue(), textName.getStringValue() );
      uploadNetCfg( wifiNet, netCfg, source.getDialogController(), delegate );
    }
  }

  private boolean isConnected( String ipPrefix ) {
    try {
      List<InterfaceAddress> myAddressList = PiRailComm.updateMyAddressList();
      for (InterfaceAddress addr : myAddressList) {
        if (addr.getAddress().toString().contains( ipPrefix )) {
          return true;
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in isConnected()", ex );
    }
    return false;
  }

  private void uploadNetCfg( WifiNet wifiNet, NetCfg netCfg, DialogController dlgCtrl, WifiListDelegate delegate ) {
    dlgCtrl.getDialogComp().showBusy( true );
    new Thread( new Runnable() {
      @Override
      public void run() {
        String ssid = wifiNet.getSsid();
        String ip = "192.168.4.1";
        Properties params = new Properties();
        params.put( "netCfg", ConfigLoader.NET_CFG_XML );
        params.put( "ip", ip );
        params.put( "ssid", ssid );
        try {
          //--- Step 1: Connect to module WiFi
          String result = SystemManager.getInstance().getWifiManager().setActiveWifi( ssid );
          Logger.info( "Connect to Wifi, msg="+result );
          int timeout = 15;
          String ipPrefix = "192.168.4.";
          while (!isConnected( ipPrefix )) {
            if (timeout <= 0) {
              dlgCtrl.getDialogComp().showBusy( false );
              Logger.info( "Timout waiting for module WiFi (IP-prefix="+ipPrefix+")" );
              String title = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupTimeout_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupTimeout_TXT", null, params );
              dlgCtrl.showMessage( title, text );
              delegate.setPaused( false ); // continue WiFi scanning
              return;
            }
            Thread.sleep( 1000 );
            timeout--;
          }

          //--- Step 2: Upload netCfg.xml
          InetAddress deviceAddress = InetAddress.getByName( ip );
          XmlWriter xmlWriter = new XmlWriter( null );
          StringBuilder stringBuffer = xmlWriter.writeXML( netCfg, PiRailFactory.ID_NETCFG, true );
          int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.NET_CFG_XML );
          dlgCtrl.getDialogComp().showBusy( false );
          if ((rc >= 200) &&  (rc < 300)) {
            Logger.info( "Successfully updated "+ConfigLoader.NET_CFG_XML+" on "+deviceAddress );
            String title = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupSuccess_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupSuccess_TXT", null, params );
            dlgCtrl.showMessage( title, text );
          }
          else {
            Logger.error( "Error updating " + ConfigLoader.NET_CFG_XML + " on " + deviceAddress + ", responseCode=" + rc );
            String title = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupError_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupError_TXT", null, params );
            dlgCtrl.showMessage( title, text );
          }
          String railSSID = netCfg.getWifiCfg( null ).getSsid();
          Logger.info( "Switching back to railway WiFi, SSID="+railSSID );
          SystemManager.getInstance().getWifiManager().setActiveWifi( railSSID );
        }
        catch (Exception ex) {
          dlgCtrl.getDialogComp().showBusy( false );
          String title = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupIOError_H", null, null );
          params.put( "ip", ip );
          String text = SystemManager.getInstance().getLocalizedMessage( "deviceWifiSetupIOError_TXT", null, params );
          dlgCtrl.showMessage( title, text );
        }
        delegate.setPaused( false ); // continue WiFi scanning
      }
    } ).start();
  }
}
