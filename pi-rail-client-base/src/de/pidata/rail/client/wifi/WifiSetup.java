/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.wifi;

import de.pidata.gui.controller.base.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.model.*;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

public class WifiSetup extends GuiDelegateOperation<EditWifiDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditWifiDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditCfgUI uiModel = delegate.getUiModel();
    NetCfg netCfg = uiModel.getNetCfg();
    String moduleName = netCfg.getId();
    if (!Helper.isNullOrEmpty( moduleName )) {
      String msg = Cfg.checkID( moduleName );
      if (msg != null) {
        Properties params = new Properties();
        params.put( "moduleName", moduleName );
        String title = SystemManager.getInstance().getLocalizedMessage( "wifiSetupInvalidName_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "wifiSetupInvalidName_TXT", null, params );
        showMessage( source, title, text+"\n" + msg );
        return;
      }
    }
    InetAddress deviceAddress = uiModel.getInetAddress();
    try {
      WifiCfg wifiCfg = netCfg.getWifiCfg( null );
      if (wifiCfg.getMode() == WifiMode.AP) {
        // TODO Konfigurierbar machen. Achtung alte WifiCfg kennt den Mode nicht
        wifiCfg.setMode( WifiMode.STA );
      }
      uploadNetCfg( deviceAddress, netCfg, source.getDialogController() );
    }
    catch (Exception ex) {
      Properties params = new Properties();
      params.put( "fileName", ConfigLoader.NET_CFG_XML );
      params.put( "ip", deviceAddress );
      String title = SystemManager.getInstance().getLocalizedMessage( "wifiSetupIOEx_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "wifiSetupIOEx_TXT", null, params );
      source.getDialogController().showMessage( title, text+"\n" + ex.getMessage() );
    }
  }

  protected void uploadNetCfg( InetAddress deviceAddress, NetCfg netCfg, DialogController dlgCtrl ) throws IOException {
    XmlWriter xmlWriter = new XmlWriter( null );
    StringBuilder stringBuffer = xmlWriter.writeXML( netCfg, PiRailFactory.ID_NETCFG, true );
    int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.NET_CFG_XML );
    if ((rc >= 200) &&  (rc < 300)) {
      Logger.info( "Successfully updated "+ConfigLoader.NET_CFG_XML+" on "+deviceAddress );
      Properties params = new Properties();
      params.put( "fileName", ConfigLoader.NET_CFG_XML );
      params.put( "ip", deviceAddress );
      String title = SystemManager.getInstance().getLocalizedMessage( "wifiSetupSuccess_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "wifiSetupSuccess_TXT", null, params );
      dlgCtrl.showMessage( title, text );
    }
    else {
      Logger.error( "Error updating " + ConfigLoader.NET_CFG_XML + " on " + deviceAddress + ", responseCode=" + rc );
      Properties params = new Properties();
      params.put( "fileName", ConfigLoader.NET_CFG_XML );
      params.put( "ip", deviceAddress );
      String title = SystemManager.getInstance().getLocalizedMessage( "wifiSetupError_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "wifiSetupError_TXT", null, params );
      dlgCtrl.showMessage( title, text+"\nHTTP rc="+rc );
    }
  }
}
