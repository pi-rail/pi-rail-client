package de.pidata.rail.client.editSwitchbox;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditSwitchboxCfg extends GuiDelegateOperation<EditSwitchboxDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditSwitchboxDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    SwitchBox tower = (SwitchBox) dataContext;
    EditCfgParamList parameterList = new EditCfgParamList( tower.getId(), tower.getDisplayName(), tower.getAddress().getInetAddress(), "Loco" );
    String title = SystemManager.getInstance().getLocalizedMessage( "editSwitchBoxCfgEdit_H", null, null );
    sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_cfg" ), title, parameterList );
  }
}
