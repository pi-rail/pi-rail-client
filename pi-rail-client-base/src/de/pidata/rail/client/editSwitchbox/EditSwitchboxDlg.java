/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.editSwitchbox;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.railway.RailFunction;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.SwitchBox;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditSwitchboxDlg extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    MutableControllerGroup ctrlGroup = sourceCtrl.getControllerGroup();
    if (dataContext instanceof RailFunction) {
      RailAction railAction = (RailAction) dataContext;
      Logger.info("selected RailAction: " + railAction.getId());
      RailDevice railDevice = railAction.getRailDevice();
      if (railDevice != null) {
        EditCfgParamList parameterList = new EditCfgParamList( railDevice.getId(), railDevice.getDisplayName(), railDevice.getAddress().getInetAddress(), "SwitchBox" );
        String title = SystemManager.getInstance().getLocalizedMessage( "editSwitchBoxEdit_H", null, null );
        ctrlGroup.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_switchbox" ), title, parameterList );
      }
    }
  }
}
