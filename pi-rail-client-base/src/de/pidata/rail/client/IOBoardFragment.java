package de.pidata.rail.client;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.SwitchBox;

public class IOBoardFragment extends ModuleGroup {

  private SwitchBox selectedTower;

  public SwitchBox getSelectedTower() {
    if (selectedTower == null) {
      selectedTower = PiRail.getInstance().getModelRailway().getSwitchBox( null );
    }
    return selectedTower;
  }

  public void setSelectedTower( SwitchBox selectedTower ) {
    this.selectedTower = selectedTower;
  }

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   * @param moduleContainer
   */
  @Override
  public void activateModule( UIContainer moduleContainer ) {
    setModel( getSelectedTower() );
    super.activateModule( moduleContainer );
  }

  @Override
  public synchronized void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
  }
}
