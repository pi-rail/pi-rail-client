package de.pidata.rail.client.selectProduct;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SelectProductParamList extends AbstractParameterList {

  public static final QName ITEM_ID = ModelRailway.NAMESPACE.getQName( "itemID" );
  public static final QName STORAGE_TYPE = ModelRailway.NAMESPACE.getQName( "storageType" );
  public static final QName CATALOG_PATH = ModelRailway.NAMESPACE.getQName( "catalogPath" );
  public static final QName PRODUCT_ID = ModelRailway.NAMESPACE.getQName( "productID" );
  public static final QName PRODUCT_CFG_ID = ModelRailway.NAMESPACE.getQName( "productCfgID" );

  public SelectProductParamList() {
    super(5 );
    defineParam( 0, ParameterType.QNameType, ITEM_ID, null );
    defineParam( 1, ParameterType.StringType, STORAGE_TYPE, null );
    defineParam( 2, ParameterType.StringType, CATALOG_PATH, null );
    defineParam( 3, ParameterType.QNameType, PRODUCT_ID, null );
    defineParam( 4, ParameterType.QNameType, PRODUCT_CFG_ID, null );
  }

  public SelectProductParamList( String storageType, String catalogPath ) {
    this();
    setString( STORAGE_TYPE, storageType );
    setString( CATALOG_PATH, catalogPath );
  }

  public SelectProductParamList( QName itemID, String storageType, String catalogPath, QName productID, QName productCfgID ) {
    this();
    setQName( ITEM_ID, itemID );
    setString( STORAGE_TYPE, storageType );
    setString( CATALOG_PATH, catalogPath );
    setQName( PRODUCT_ID, productID );
    setQName( PRODUCT_CFG_ID, productCfgID );
  }

  public QName getItemID() {
    return getQName( ITEM_ID );
  }

  public QName getProductID() {
    return getQName( PRODUCT_ID );
  }

  public QName getProductCfgID() {
    return getQName( PRODUCT_CFG_ID );
  }

  public String getStorageType() {
    return getString( STORAGE_TYPE );
  }

  public String getCatalogPath() {
    return getString( CATALOG_PATH );
  }
}
