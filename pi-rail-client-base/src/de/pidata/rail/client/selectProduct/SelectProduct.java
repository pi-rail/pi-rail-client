package de.pidata.rail.client.selectProduct;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editiocfg.EditCfgDelegate;
import de.pidata.rail.client.uiModels.AddConfigUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.Action;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.EnumAction;
import de.pidata.rail.model.TrackPos;
import de.pidata.rail.railway.*;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class SelectProduct extends GuiDelegateOperation<SelectProductDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, SelectProductDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    AddConfigUI model = (AddConfigUI) dataContext;
    QName newId = EnumAction.NAMESPACE.getQName( model.getId() );
    if (newId == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectProductNameEmpty_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectProductNameEmpty_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    TableController productTableCtrl = (TableController) sourceCtrl.getControllerGroup().getController( GuiBuilder.NAMESPACE.getQName( "productTable" ) );
    Product selectedRow = (Product) productTableCtrl.getSelectedRow( 0 );
    if (selectedRow == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectProductSelectProduct_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectProductSelectProduct_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    TableController productCfgTableCtrl = (TableController) sourceCtrl.getControllerGroup().getController( GuiBuilder.NAMESPACE.getQName( "productCfgTable" ) );
    ProductCfg selectedCfgRow = (ProductCfg) productCfgTableCtrl.getSelectedRow( 0 );
    if (selectedCfgRow == null) {
      String title = SystemManager.getInstance().getLocalizedMessage( "selectProductSelectConfig_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "selectProductSelectConfig_TXT", null, null );
      showMessage( sourceCtrl, title, text );
      return;
    }
    RailDevice railDevice = null;
    Cfg cfg = null;
    DialogController cfgDlgCtrl = sourceCtrl.getDialogController().getParentDialogController();
    if (cfgDlgCtrl != null) {
      DialogControllerDelegate cfgDlgDelegate = cfgDlgCtrl.getDelegate();
      if (cfgDlgDelegate instanceof EditCfgDelegate) {
        railDevice = ((EditCfgDelegate) cfgDlgDelegate).getRailDevice();
        cfg = ((EditCfgDelegate) cfgDlgDelegate).getUiModel().getCfg();
      }
    }
    if (railDevice instanceof Locomotive) {
      Action action = cfg.getAction( newId );
      if (action != null) {
        Properties params = new Properties();
        params.put( "action", action );
        String title = SystemManager.getInstance().getLocalizedMessage( "selectProductDupName_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "selectProductDupName_TXT", null, params );
        showMessage( sourceCtrl, title, text );
        return;
      }
    }
    else {
      RailAction railAction = PiRail.getInstance().getModelRailway().getRailAction( null, newId );
      // TODO: check also for new created or modified actions in this config
      if (railAction != null) {
        // Check for dummy actions like a block calculated from existing Balises - for these it must be allowed to create a config entry
        if (railAction.getItemConn() != null) {
          Properties params = new Properties();
          params.put( "action", railAction );
          String title = SystemManager.getInstance().getLocalizedMessage( "selectProductDupName_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "selectProductDupName_TXT", null, params );
          showMessage( sourceCtrl, title, text );
          return;
        }
      }
    }

    String value = newId.getName();
    if (selectedRow.getId() == ProductCfg.ID_IR_BALISE) {
      String msg = TrackPos.checkPosID( value );
      if (msg != null) {
        Properties params = new Properties();
        params.put( "id", value );
        String title = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidPosID_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidPosID_TXT", null, params );
        showMessage( sourceCtrl, title, text+"\n"+msg );
        return;
      }
    }
    else if (selectedRow.getId() == ProductCfg.ID_BLOCK) {
      String msg = RailBlock.checkBlockID( value );
      if (msg != null) {
        Properties params = new Properties();
        params.put( "id", value );
        String title = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidBlockID_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidBlockID_TXT", null, params );
        showMessage( sourceCtrl, title, text+"\n"+msg );
        return;
      }
    }
    else {
      String msg = Action.checkID( value );
      if (msg != null) {
        Properties params = new Properties();
        params.put( "id", value );
        String title = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidName_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "selectProductInvalidName_TXT", null, params );
        showMessage( sourceCtrl, title, text+"\n"+msg );
        return;
      }
    }
    delegate.selected( newId, selectedRow, selectedCfgRow );
  }
}
