package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.StateScript;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.RailFunction;
import de.pidata.service.base.ServiceException;

public class SendFunctionCommand extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (dataContext instanceof RailAction) {
      RailAction railAction = (RailAction) dataContext;
      String evName = eventID.getName();
      int index = (evName.charAt( evName.length() - 1 )) - '1';
      railAction.invokeAction( index );
    }
  }
}
