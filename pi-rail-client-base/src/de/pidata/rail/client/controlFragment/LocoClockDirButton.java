package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.MotorState;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.ServiceException;

public class LocoClockDirButton extends GuiOperation {

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive selectedLoco = (Locomotive) dataContext;
    char dir = selectedLoco.getClockDir();
    if (dir == MotorState.CHAR_FORWARD) {
      selectedLoco.setClockDir( MotorState.CHAR_BACK );
    }
    else {
      selectedLoco.setClockDir( MotorState.CHAR_FORWARD );
    }
  }
}
