package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.depot.SelectedLocoParams;
import de.pidata.rail.client.depot.SelectWagonParams;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.RailBlock;
import de.pidata.rail.railway.Wagon;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.rail.track.WagonCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class AddWagon extends GuiOperation {

  Locomotive selectedLoco;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    selectedLoco = (Locomotive) dataContext;
    SelectedLocoParams currentWagonParams = new SelectedLocoParams( selectedLoco.getId() );
    String title = SystemManager.getInstance().getLocalizedMessage( "addWaggonSelect_H", null, null );
    sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "depot_selection_dialog" ), title, currentWagonParams );
  }

   @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
     if (resultOK && (resultList instanceof SelectWagonParams)) {
       QName selectedID = ((SelectWagonParams) resultList).getSelectedWagonId();
       if (selectedID != null) {
         WagonCfg wagonCfg = new WagonCfg( selectedID );
         Wagon selectedWagon = PiRail.getInstance().getModelRailway().getOrCreateWagon( wagonCfg.getId(), wagonCfg.getIcon() );

         selectedLoco.wagonRead( selectedWagon );
         RailBlock currentBlock = selectedLoco.getCurrentBlock();
         if (currentBlock != null) {
           currentBlock.entered( selectedWagon, System.currentTimeMillis() );
         }

       }
     }
  }
}
