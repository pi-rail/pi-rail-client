/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.ServiceException;

public class ChangeSpeedButton extends GuiOperation {

  public static final QName ID_SPEED_DEC_LARGE = NAMESPACE.getQName( "speedDecLarge" );
  public static final QName ID_SPEED_DEC_SMALL = NAMESPACE.getQName( "speedDecSmall" );
  public static final QName ID_SPEED_INC_SMALL = NAMESPACE.getQName( "speedIncSmall" );
  public static final QName ID_SPEED_INC_LARGE = NAMESPACE.getQName( "speedIncLarge" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive loco = (Locomotive) dataContext;
    int delta = 0;
    if (eventID == ID_SPEED_DEC_LARGE) {
       delta = -10;
    }
    else if (eventID == ID_SPEED_DEC_SMALL) {
      delta = -1;
    }
    else if (eventID == ID_SPEED_INC_SMALL) {
      delta = 1;
    }
    else if (eventID == ID_SPEED_INC_LARGE) {
      delta = 10;
    }
    if ((delta != 0) && (loco != null)) {
      DecimalObject decValue = loco.getTargetSpeedPercent();
      int value;
      if (decValue == null) {
        value = 0;
      }
      else {
        value = decValue.intValue();
      }
      int newValue = value + delta;
      if (newValue < 0) {
        newValue = 0;
      }
      else if (newValue > 100) {
        newValue = 100;
      }
      loco.setTargetSpeedPercent( new DecimalObject( newValue, 0 ) );
    }
  }
}
