package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.MotorState;
import de.pidata.rail.railway.Locomotive;
import de.pidata.service.base.ServiceException;

public class LocoDirButton extends GuiOperation {

  public static final QName ID_PAUSE = NAMESPACE.getQName( "LocoPlayPauseController" );
  public static final QName ID_FORWARD = NAMESPACE.getQName( "LocoForward" );
  public static final QName ID_BACKWARD = NAMESPACE.getQName( "LocoBackward" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive selectedLoco = (Locomotive) dataContext;
    if (eventID == ID_PAUSE) {
      boolean stopped = selectedLoco.getStopped();
      selectedLoco.setStopped( !stopped );
    }
    else if (eventID == ID_FORWARD) {
      selectedLoco.setStopped( false );
      selectedLoco.setTargetDir( MotorState.DIR_FORWARD );
    }
    else if (eventID == ID_BACKWARD) {
      selectedLoco.setStopped( false );
      selectedLoco.setTargetDir( MotorState.DIR_BACK );
    }
  }
}
