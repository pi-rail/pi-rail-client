package de.pidata.rail.client.controlFragment;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectedLocoParam extends AbstractParameterList {

  public static final QName SELECTED_LOCO_NAME = ModelRailway.NAMESPACE.getQName( "selectedLocoName" );
  public static final QName SELECTED_LOCO_ID = ModelRailway.NAMESPACE.getQName( "selectedLocoID" );


  public SelectedLocoParam() {
    super( ParameterType.QNameType, SELECTED_LOCO_NAME, ParameterType.QNameType, SELECTED_LOCO_ID  );
  }

  public SelectedLocoParam( QName locoName, QName locoID) {
    this();
    setQName( SELECTED_LOCO_NAME, locoName );
    setQName( SELECTED_LOCO_ID, locoID );
  }


  public QName getSelectedLocoName() {
    return getQName( SELECTED_LOCO_NAME );
  }

  public QName getSelectedLocoID() {
    return getQName( SELECTED_LOCO_ID );
  }


}
