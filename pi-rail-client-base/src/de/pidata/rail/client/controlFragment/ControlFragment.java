/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailRange;

public class ControlFragment extends ModuleGroup implements Runnable {

  private Thread updateThread;
  private boolean updaterRunning = false;

  public Locomotive getSelectedLoco() {
    return (Locomotive) getModel();
  }

  public void setSelectedLoco( Locomotive selectedLoco ) {
    setModel( selectedLoco );
  }

  @Override
  public void activate( UIContainer uiContainer ) {
    super.activate( uiContainer );
    if (!updaterRunning) {
      updateThread = new Thread( this );
      updateThread.start();
    }
  }

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   * @param moduleContainer
   */
  @Override
  public void activateModule( UIContainer moduleContainer ) {
    setModel( getSelectedLoco() );
    super.activateModule( moduleContainer );
  }

  @Override
  public synchronized void deactivateModule() {
    updaterRunning = false;
    super.deactivateModule();
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used
   * to create a thread, starting the thread causes the object's
   * <code>run</code> method to be called in that separately executing
   * thread.
   * <p>
   * The general contract of the method <code>run</code> is that it may
   * take any action whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    updaterRunning = true;
    while (updaterRunning) { // TODO: analog Switches: refactor to use a central place; handling who controls
      try {
        Locomotive locomotive = (Locomotive) getModel();
        if (locomotive != null) {
          Cfg cfg = locomotive.getConfig();
          if (cfg != null) {
            QName myLockID = PiRail.getInstance().getMyLockID();
            if (locomotive.isLocker( myLockID )) {
              State locoState = locomotive.getState();
              if (locoState != null) {
                RailRange mainMotor = locomotive.getMainMotor();
                if (mainMotor != null) {
                  MotorAction motorAction = (MotorAction) mainMotor.getAction();
                  MotorState motorState = locoState.getSp( motorAction.getId() );
                  if (motorState != null) {
                    short targetSpeed = locomotive.calcTargetSpeed();
                    char targetDir = locomotive.getTargetDirChar();
                    int motorTarget = motorState.getTgtInt();
                    char motorTgtDir = motorState.getTgtChar();
                    if ((motorTarget != targetSpeed) || !(targetDir == motorTgtDir)) {
                      if (locomotive.getStopped()) {
                        PiRail.getInstance().sendMotorSpeed( mainMotor, targetDir, targetSpeed, true );
                      }
                      else {
                        PiRail.getInstance().sendMotorSpeed( mainMotor, targetDir, targetSpeed, false );
                      }
                    }
                    else if (locomotive.needsSifaPing( myLockID )) {
                      // Resending targetDir and targetSpeed is the sifa ping
                      PiRail.getInstance().sendMotorSpeed( mainMotor, targetDir, targetSpeed, false );
                    }
                    else if (locomotive.getStopped()) {
                      PiRail.getInstance().sendLockCommand( locomotive, false );
                    }
                    Thread.sleep( 100 );
                  }
                }
              }
            }
            else if (!locomotive.getStopped()) {
              PiRail.getInstance().sendLockCommand( locomotive, true );
              Thread.sleep( 100 );
            }
          }
        }
        else {
          ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
          Locomotive loco = (Locomotive) modelRailway.firstChild( ModelRailway.ID_LOCO );
          if (loco != null) {
           setModel( loco );
          }
        }
        Thread.sleep( 200 );
      }
      catch (InterruptedException e) {
        updaterRunning = false;
      }
      catch (Exception ex) {
        Logger.error( "Error in LocoControl-Loop", ex );
      }
    }
    this.updateThread = null;
  }
}

