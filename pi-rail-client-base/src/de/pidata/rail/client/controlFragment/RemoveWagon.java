package de.pidata.rail.client.controlFragment;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.RailBlock;
import de.pidata.rail.railway.Wagon;
import de.pidata.service.base.ServiceException;

import java.util.List;

public class RemoveWagon extends GuiOperation {


  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Locomotive selectedLoco = (Locomotive) dataContext;
    if (selectedLoco.wagonCount() > 0) {
      List<Wagon> wagonList = selectedLoco.getWagonList();
      Wagon lastWagon = wagonList.get( wagonList.size()-1 );
      if (lastWagon != null) {
        selectedLoco.removeWagon( lastWagon );
        RailBlock currentBlock = selectedLoco.getCurrentBlock();
        if (currentBlock != null) {
          currentBlock.disconnected( lastWagon );
        }
      }
    }
  }
}
