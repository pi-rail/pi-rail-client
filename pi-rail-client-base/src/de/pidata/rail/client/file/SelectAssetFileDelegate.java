package de.pidata.rail.client.file;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.KeyAndValue;
import de.pidata.gui.guidef.StringTable;
import de.pidata.models.tree.Model;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.filebased.FilebasedStorage;

import java.io.File;

public class SelectAssetFileDelegate implements DialogControllerDelegate<SelectAssetFileParamList,SelectAssetFileParamList> {

  private SelectAssetFileParamList parameterList;
  private String externalFilePath = null;
  private DialogController dlgCtrl;

  public SelectAssetFileParamList getParameterList() {
    return parameterList;
  }

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, SelectAssetFileParamList parameterList ) throws Exception {
    this.parameterList = parameterList;
    this.dlgCtrl = dlgCtrl;
    this.externalFilePath = null;
    Storage assetStorage = SystemManager.getInstance().getStorage( parameterList.getStorageType(), parameterList.getDirPath() );
    String[] files = assetStorage.list();
    StringTable fileList = StringTable.fromArray( GuiBuilder.NAMESPACE.getQName( "FileList" ), files );
    TextController topLabel = (TextController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "topLabel" ) );
    topLabel.setValue( parameterList.getInfoText() );
    return fileList;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public SelectAssetFileParamList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    String selFileName = null;
    if (ok) {
      if (externalFilePath == null) {
        ListController listController = (ListController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "filesList" ) );
        if (listController != null) {
          KeyAndValue selEntry = (KeyAndValue) listController.getSelectedRow( 0 );
          if (selEntry != null) {
            String path = parameterList.getDirPath();
            if (Helper.isNullOrEmpty( path ) || path.equals( "." )) {
              selFileName = selEntry.getValue();
            }
            else {
              if (!FilebasedStorage.isPathSeparator( path.charAt( path.length()-1 ))) {
                path += '/';
              }
              selFileName = path + selEntry.getValue();
            }
          }
        }
      }
      else {
        parameterList.setStorageType( SystemManager.STORAGE_PRIVATE_DOWNLOADS );
        selFileName = externalFilePath;
      }
    }
    parameterList.setFileName( selFileName );
    return parameterList;
  }

  public void externalFileSelected( String path ) {
    this.externalFilePath = path;
    this.dlgCtrl.close( true );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }
}
