package de.pidata.rail.client.file;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.File;

public class ShowFileChooser extends GuiDelegateOperation<SelectAssetFileDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, SelectAssetFileDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    String catalogDirPath;
    SelectAssetFileParamList paramList = delegate.getParameterList();
    Storage storage = SystemManager.getInstance().getStorage( SystemManager.STORAGE_PRIVATE_DOWNLOADS, "." );
    File dir = new File( storage.getPath( paramList.getDirPath() ) );
    if (dir.exists() && dir.isDirectory()) {
      catalogDirPath = storage.getPath( paramList.getDirPath() );
    }
    else {
      catalogDirPath = ".";
    }
    String filePattern = paramList.getFilePattern();
    String dialogType;
    if (Helper.isNullOrEmpty( filePattern )) {
      filePattern = "*";
      dialogType = FileChooserParameter.TYPE_OPEN_DIR;
    }
    else {
      dialogType = FileChooserParameter.TYPE_OPEN_FILE;
    }
    FileChooserParameter params = new FileChooserParameter( catalogDirPath, filePattern, dialogType );
    String title = SystemManager.getInstance().getLocalizedMessage( "showFileChooserSelect_H", null, null );
    sourceCtrl.getDialogController().showFileChooser( title, params );
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof FileChooserResult) {
      if (resultOK) {
        String path = ((FileChooserResult) resultList).getFilePath();
        if (!Helper.isNullOrEmpty(path)) {
          path = path.replace( "\\", "/" );
        }
        ((SelectAssetFileDelegate) parentDlgCtrl.getDelegate()).externalFileSelected( path );
      }
    }
  }
}
