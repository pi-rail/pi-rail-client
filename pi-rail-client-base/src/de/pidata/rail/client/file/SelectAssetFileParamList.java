package de.pidata.rail.client.file;

import de.pidata.gui.guidef.GuiService;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

public class SelectAssetFileParamList extends AbstractParameterList {

  public static final QName PARAM_STORAGE_TYPE = GuiService.NAMESPACE.getQName( "storageType" );
  public static final QName PARAM_DIR_PATH = GuiService.NAMESPACE.getQName( "dirPath" );
  public static final QName PARAM_FILE_PATTERN = GuiService.NAMESPACE.getQName( "filePattern" );
  public static final QName PARAM_FILE_NAME = GuiService.NAMESPACE.getQName( "fileName" );
  public static final QName PARAM_INFO_TEXT = GuiService.NAMESPACE.getQName( "infoText" );

  public SelectAssetFileParamList() {
    super( 5 );
    defineParam( 0, ParameterType.StringType, PARAM_STORAGE_TYPE, null );
    defineParam( 1, ParameterType.StringType, PARAM_DIR_PATH, null );
    defineParam( 2, ParameterType.StringType, PARAM_FILE_PATTERN, null );
    defineParam( 3, ParameterType.StringType, PARAM_FILE_NAME, null );
    defineParam( 4, ParameterType.StringType, PARAM_INFO_TEXT, null );
  }

  public SelectAssetFileParamList( String storageType, String dirPath, String filePattern, String fileName, String infoText ) {
    this();
    setString( PARAM_STORAGE_TYPE, storageType );
    setString( PARAM_DIR_PATH, dirPath );
    setString( PARAM_FILE_PATTERN, filePattern );
    setString( PARAM_FILE_NAME, fileName );
    setString( PARAM_INFO_TEXT, infoText );
  }

  public String getStorageType()  {
    return getString( PARAM_STORAGE_TYPE );
  }
  
  public String getDirPath()  {
    return getString( PARAM_DIR_PATH );
  }

  public String getFilePattern()  {
    return getString( PARAM_FILE_PATTERN );
  }

  public String getFileName()  {
    return getString( PARAM_FILE_NAME );
  }

  public String getInfoText()  {
    return getString( PARAM_INFO_TEXT );
  }
  
  public void setStorageType( String storageType )  {
    setString( PARAM_STORAGE_TYPE, storageType );
  }

  public void setFileName( String fileName )  {
    setString( PARAM_FILE_NAME, fileName );
  }
}
