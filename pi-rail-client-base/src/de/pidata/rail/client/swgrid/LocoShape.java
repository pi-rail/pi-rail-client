package de.pidata.rail.client.swgrid;

import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.gui.view.figure.TextShapePI;
import de.pidata.rail.model.TrackPos;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.RailAction;
import de.pidata.rect.Rect;

public class LocoShape extends TextShapePI {

  public static final int LOCO_SHAPE_WIDTH = 500;
  public static final int LOCO_SHAPE_HEIGHT = 40;
  
  private Locomotive locomotive;

  public LocoShape( Figure figure, Rect bounds, ShapeStyle style, Locomotive locomotive ) {
    super( figure, bounds, style, locomotive.getDisplayName() );
    this.locomotive = locomotive;
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  public void updatePos( int cellWidth, int cellHeight ) {
    TrackPos trackPos = locomotive.getLastEventPos();
    Rect bounds = getBounds();
    if (trackPos == null) {
      bounds.setPos( 0, 0 );
    }
    else {
      bounds.setPos( trackPos.getXInt() * cellWidth, trackPos.getYInt() * cellHeight );
    }
  }
}
