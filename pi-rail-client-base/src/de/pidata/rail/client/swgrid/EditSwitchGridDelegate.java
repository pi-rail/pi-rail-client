/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelHelper;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.editcfg.SaveCfgOperation;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.comm.ConfigLoaderListener;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.model.EnumAction;
import de.pidata.rail.model.SensorAction;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.PanelCfg;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;
import java.util.*;

public class EditSwitchGridDelegate implements DialogControllerDelegate<EditCfgParamList, ParameterList>, ConfigLoaderListener, EventListener {

  private EditTrackUI uiModel;
  private DialogController dlgCtrl;
  private ToolboxFigure toolboxFigure;
  private SwitchGridFigure switchGridFigure;
  private PanelCfg panelCfg;

  public SwitchGridFigure getSwitchGridFigure() {
    return switchGridFigure;
  }

  public InetAddress getDeviceAddress() {
    return uiModel.getInetAddress();
  }

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, EditCfgParamList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    QName deviceId = parameterList.getDeviceId();
    InetAddress ipAddress = parameterList.getIPAddress();
    this.uiModel = new EditTrackUI( parameterList.getDeviceName(), ipAddress );
    this.uiModel.setEditTrackPos( new TrackPos() );
    ConfigLoader.loadConfig( deviceId, ipAddress, ConfigLoader.TRACK_CFG_XML, this, false, false );

    PaintController gridToolbox = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "gridToolbox" ) );
    PaintViewPI gridToolboxView = (PaintViewPI) gridToolbox.getView();
    toolboxFigure = new ToolboxFigure( gridToolboxView.getBounds() );
    gridToolboxView.addFigure( toolboxFigure );

    TableController actionTable = (TableController) dlgCtrl.getController( ControllerBuilder.NAMESPACE.getQName( "actionTable" ) );
    actionTable.addListener( this );

    return uiModel;
  }

  @Override
  public void dialogCreated( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogShowing( DialogController dlgCtrl ) {

  }

  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {

  }

  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public void setPanelCfg( PanelCfg panelCfg ) {
    PaintController switchGrid = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
    TableController actionTable = (TableController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "actionTable" ) );
    PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
    if (switchGridFigure != null) {
      switchGridView.removeFigure( switchGridFigure );
    }
    this.panelCfg = panelCfg;
    switchGridFigure = new SwitchGridFigure( switchGridView.getBounds(), panelCfg, actionTable, SwitchGridModule.cellSize );
    switchGridFigure.startEditMode( toolboxFigure, uiModel );
    switchGridView.addFigure( switchGridFigure );

    dlgCtrl.activate( dlgCtrl.getDialogComp() );
    toolboxFigure.setSelectedTool( toolboxFigure.getShowTool() );
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    if (success) {
      TrackCfg trackCfg = (TrackCfg) configLoader.getConfigModel();
      trackCfg.setDeviceAddress( uiModel.getInetAddress() );
      uiModel.setTrackCfg( trackCfg );
      panelCfg = trackCfg.getPanelCfg();
      if (panelCfg != null) {
        setPanelCfg( panelCfg );
      }
    }
  }

  public TrackPos getEditTrackPos() {
    return uiModel.getEditTrackPos();
  }

  private void createTrackMsgItemConn( TrackCfg trackCfg, RailMessage railMessage, TrackMsg trackMsg, QName posID ) {
    Cfg trackActionCfg = trackCfg.getActionCfg();
    if (trackActionCfg == null) {
      trackActionCfg = new Cfg();
      QName trackID = trackCfg.getId();
      trackActionCfg.setId( trackID.getNamespace().getQName( trackID.getName() + "-Aktionen" ) );
      trackCfg.setActionCfg( trackActionCfg );
    }
    QName msgID = railMessage.getId();
    QName itemConnID = msgID.getNamespace().getQName( msgID.getName() + "-conn" );
    ItemConn itemConn = new ItemConn( itemConnID );
    itemConn.setProduct( ItemConn.PRODUCT_NFC_BALISE );
    itemConn.addParamChar( TrackMsg.ID_CMD, '-' );
    itemConn.addParamInt( TrackMsg.ID_CMDDIST, 0 );
    trackActionCfg.addItemConn( itemConn );
    trackMsg.setItemID( itemConnID );
    trackMsg.setGroup( ModelRailway.GROUP_BALISE );
    trackActionCfg.addTrackMsg( trackMsg );

    QName triggerID;
    if (posID == null) {
      triggerID = msgID.getNamespace().getQName( msgID.getName() + "-Trigger" );
    }
    else {
      triggerID = msgID.getNamespace().getQName( posID.getName() + "-Trigger" );
    }
    TriggerAction triggerAction = new TriggerAction( triggerID );
    triggerAction.setGroup( ModelRailway.GROUP_TRIGGER );
    triggerAction.setItemID( itemConnID );
    triggerAction.setType( ValueType.charVal );
    triggerAction.setOnAction( TriggerAction.ON_ACTION_UNDEFINED );
    StateScript onState = new StateScript( PiRailFactory.NAMESPACE.getQName( "all" ), '*', 0 );
    SetCmd setCmd = new SetCmd( Param.PARAM_CMD, "?", null );
    onState.addSet( setCmd );
    triggerAction.addOnState( onState );
    trackActionCfg.addTrigger( triggerAction );
  }

  private void createBlockItemConn( TrackCfg trackCfg, RailBlock railBlock, SensorAction sensorAction ) {
    Cfg trackActionCfg = trackCfg.getActionCfg();
    if (trackActionCfg == null) {
      trackActionCfg = new Cfg();
      QName trackID = trackCfg.getId();
      trackActionCfg.setId( trackID.getNamespace().getQName( trackID.getName() + "-Aktionen" ) );
      trackCfg.setActionCfg( trackActionCfg );
    }
    QName msgID = railBlock.getId();
    QName itemConnID = msgID.getNamespace().getQName( msgID.getName() + "-conn" );
    ItemConn itemConn = new ItemConn( itemConnID );
    itemConn.setProduct( PiRailFactory.NAMESPACE.getQName("Block") );
    trackActionCfg.addItemConn( itemConn );
    sensorAction.setItemID( itemConnID );
    sensorAction.setGroup( ModelRailway.GROUP_BLOCK );
    sensorAction.setType( InModeType.Block );
    trackActionCfg.addSensor( sensorAction );
  }

  public void saveGrid() {
    switchGridFigure.saveEditAction();
    dlgCtrl.getDialogComp().showBusy( true );
    Runnable task = new Runnable() {
      @Override
      public void run() {
        TrackCfg trackCfg = uiModel.getTrackCfg();
        InetAddress trackCfgAddr = trackCfg.getDeviceAddress();
        Map<RailAction, TrackPos> modifiedTrackActions = switchGridFigure.getModifiedTrackItems();

        //--- Collect SignalTowers to update cfg file
        Map<InetAddress,Cfg> updateCfgMap = new HashMap<>();
        Map<InetAddress,TrackCfg> updateTrackCfgMap = new HashMap<>();
        StringBuilder towerNames = new StringBuilder();
        for (Map.Entry<RailAction, TrackPos> entry : modifiedTrackActions.entrySet()) {
          RailAction railAction = entry.getKey();
          RailDevice railDevice = railAction.getRailDevice();
          if (railDevice == null) {
            if (railAction instanceof RailMessage) {
              RailMessage railMessage = (RailMessage) railAction;
              TrackMsg trackMsg = (TrackMsg) railAction.getAction();
              TrackPos trackPos = entry.getValue();
              if (trackMsg == null) {
                // New Tag has been seen by Loco
                trackMsg = new TrackMsg( railMessage.getId() );
                createTrackMsgItemConn( trackCfg, railMessage, trackMsg, trackPos.getPosID() );
              }
              else {
                Model actionOwner = trackMsg.getParent( false );
                if (actionOwner instanceof TrackCfg) {
                  // Old config: trackMsg is placed top level below trackCfg
                  // -> move it below actionCfg and create itemConn
                  actionOwner.remove( trackMsg.getParentRelationID(), trackMsg );
                  if (trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                    // Also remove trackMsg from working copy
                    trackCfg.remove( trackMsg.getParentRelationID(), trackCfg.getTrackMsg( trackMsg.getId() ) );
                  }
                  createTrackMsgItemConn( trackCfg, railMessage, trackMsg, trackPos.getPosID() );
                }
                else {
                  // New config: action is located below actionCfg which is child of trackCfg
                  actionOwner = actionOwner.getParent( false );
                  if (trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                    trackMsg = trackCfg.getActionCfg().getTrackMsg( trackMsg.getId() );
                  }
                }
                if (!trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                  updateTrackCfgMap.put( ((TrackCfg) actionOwner).getDeviceAddress(), (TrackCfg) actionOwner );
                }
              }
              trackMsg.setTrackPos( trackPos );
            }
            else if (railAction instanceof RailBlock) {
              RailBlock railBlock = (RailBlock) railAction;
              SensorAction sensorAction = (SensorAction) railAction.getAction();
              if (sensorAction == null) {
                // New Tag has been seen by Loco
                sensorAction = new SensorAction( railBlock.getId() );
                createBlockItemConn( trackCfg, railBlock, sensorAction );
              }
              else {
                Model actionOwner = sensorAction.getParent( false );
                if (actionOwner instanceof TrackCfg) {
                  // Old config: trackMsg is placed top level below trackCfg
                  // -> move it below actionCfg and create itemConn
                  actionOwner.remove( sensorAction.getParentRelationID(), sensorAction );
                  if (trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                    // Also remove trackMsg from working copy
                    trackCfg.remove( sensorAction.getParentRelationID(), trackCfg.getTrackMsg( sensorAction.getId() ) );
                  }
                  createBlockItemConn( trackCfg, railBlock, sensorAction );
                }
                else {
                  // New config: action is located below actionCfg which is child of trackCfg
                  actionOwner = actionOwner.getParent( false );
                  if (trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                    sensorAction = trackCfg.getActionCfg().getSensor( sensorAction.getId() );
                  }
                }
                if (!trackCfgAddr.equals(((TrackCfg) actionOwner).getDeviceAddress())) {
                  updateTrackCfgMap.put( ((TrackCfg) actionOwner).getDeviceAddress(), (TrackCfg) actionOwner );
                }
              }
              sensorAction.setTrackPos( entry.getValue() );
            }
/* TODO Actions as part of TrackCfg:
            else if (railAction instanceof Function) {
              if (towerAddr.equals( uiModel.getInetAddress() )) {
                // if railAction is part of panel's trackCfg we have to update our existing copy
                EnumAction actionCfg = trackCfg.getActionCfg( railAction.getId() );
                actionCfg.setTrackPos( (TrackPos) railAction.getTrackPos().clone( null, true, false ) );
              }
              else {
                TrackCfg cfg = updateTrackCfgMap.get( towerAddr );
                if (cfg == null) {
                  cfg = railDevice.getTrackCfg();
                  updateTrackCfgMap.put( towerAddr, cfg );
                }
                EnumAction actionCfg = cfg.getActionCfg( railAction.getId() );
                actionCfg.setTrackPos( (TrackPos) railAction.getTrackPos().clone( null, true, false ) );
              }
            } */
          }
          else {
            InetAddress towerAddr = railDevice.getAddress().getInetAddress();
            Cfg cfg = updateCfgMap.get( towerAddr );
            if (cfg == null) {
              cfg = railDevice.getConfig();
            }
            if (railAction instanceof RailFunction) {
              EnumAction enumAction = cfg.getFunc( railAction.getId() );
              enumAction.setTrackPos( entry.getValue() );
              updateCfgMap.put( towerAddr, cfg );
            }
            else if (railAction instanceof RailTimer) {
              TimerAction msgCfg = cfg.getTimer( railAction.getId() );
              msgCfg.setTrackPos( entry.getValue() );
              updateCfgMap.put( towerAddr, cfg );
            }
            else if (railAction instanceof RailSensor) {
              SensorAction sensorAction = cfg.getSensor( railAction.getId() );
              sensorAction.setTrackPos( entry.getValue() );
              updateCfgMap.put( towerAddr, cfg );
            }
            else if (railAction instanceof RailMessage) {
              TrackMsg trackMsg = cfg.getTrackMsg( railAction.getId() );
              trackMsg.setTrackPos( entry.getValue() );
              updateCfgMap.put( towerAddr, cfg );
            }
          }
        }

        List<RailAction> updated = new LinkedList<>();
        ModelRailway modelRailway = PiRail.getInstance().getModelRailway();

        String panelDevice = null;
        if (trackCfg != null) {
          InetAddress updateAddr = uiModel.getInetAddress();
          if (SaveCfgOperation.uploadTrackCfg( updateAddr, trackCfg, dlgCtrl )) {
            panelDevice = uiModel.getDeviceName();
            RailDevice trackCfgDevice = modelRailway.getRailDevice( updateAddr );
            TrackCfg newTrackCfg = (TrackCfg) trackCfg.clone( null, true, false );
            newTrackCfg.setDeviceAddress( updateAddr );
            modelRailway.processTrackCfg( newTrackCfg, trackCfgDevice );
            for (RailAction trackItem : modifiedTrackActions.keySet()) {
              if (trackItem instanceof RailMessage) {
                TrackCfg owningTrackCfg = (TrackCfg) trackItem.getAction().getParent( false ).getParent( false );
                if (owningTrackCfg == newTrackCfg) {
                  updated.add( trackItem );
                }
              }
            }
          }
        }

        //--- Update SignalTowers' trackCfg file
        for (Map.Entry<InetAddress,TrackCfg> updateEntry : updateTrackCfgMap.entrySet()) {
          InetAddress updateAddr = updateEntry.getKey();
          TrackCfg cfg = updateEntry.getValue();
          if (SaveCfgOperation.uploadTrackCfg( updateAddr, cfg, dlgCtrl )) {
            RailDevice trackCfgDevice = modelRailway.getRailDevice( updateAddr );
            TrackCfg newTrackCfg = (TrackCfg) trackCfg.clone( null, true, false );
            newTrackCfg.setDeviceAddress( updateAddr );
            modelRailway.processTrackCfg( newTrackCfg, trackCfgDevice );
            for (RailAction trackItem : modifiedTrackActions.keySet()) {
              if (trackItem instanceof RailMessage) {
                TrackCfg owningTrackCfg = (TrackCfg) trackItem.getAction().getParent( false ).getParent( false );
                if (owningTrackCfg == newTrackCfg) {
                  updated.add( trackItem );
                }
              }
            }
          }
        }
        // No need to wait - updating trackCfg does not reboot device

        //--- Update SignalTowers' cfg file
        for (Map.Entry<InetAddress,Cfg> updateEntry : updateCfgMap.entrySet()) {
          InetAddress updateAddr = updateEntry.getKey();
          Cfg cfg = updateEntry.getValue();
          if (SaveCfgOperation.uploadCfg( updateAddr, cfg, dlgCtrl )) {
            if (towerNames.length() > 0) {
              towerNames.append( ", " );
            }
            towerNames.append( updateEntry.getValue().getId().getName() );
            for (RailAction trackItem : modifiedTrackActions.keySet()) {
              RailDevice railDevice = trackItem.getRailDevice();
              if ((railDevice != null) && !(trackItem instanceof RailMessage)) {
                InetAddress towerAddr = railDevice.getAddress().getInetAddress();
                if (updateAddr.equals( towerAddr )) {
                  updated.add( trackItem );
                }
              }
            }
          }
        }

        //--- Remove all successfully updated TrackActions
        for (RailAction railAction : updated) {
          modifiedTrackActions.remove( railAction );
        }

        dlgCtrl.getDialogComp().showBusy( false );
        if (panelDevice == null) {
          String title = SystemManager.getInstance().getLocalizedMessage( "editSwitchGridUpdated_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editSwitchGridUpdated_TXT", null, null );
          dlgCtrl.showMessage( title, text + "\n" + towerNames );
        }
        else {
          Properties params = new Properties();
          params.put( "moduleName", panelDevice );
          String title = SystemManager.getInstance().getLocalizedMessage( "editSwitchGridUpdatedPanel_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "editSwitchGridUpdatedPanel_TXT", null, params );
          dlgCtrl.showMessage( title, text + "\n" + towerNames );
        }
      }
    };
    new Thread( task ).start();
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (newValue instanceof RailAction) {
      if (switchGridFigure != null) {
        RailAction trackItem = (RailAction) newValue;
        switchGridFigure.setSelectedTrackShape( null );
        TrackPos trackPos = switchGridFigure.getModifiedTrackItems().get( trackItem );
        if (trackPos == null) {
          trackPos = trackItem.getTrackPos();
        }
        if ((trackPos != null) && (trackPos.getPanelID() == panelCfg.getId())) {
          ShapePI trackShape = switchGridFigure.getShapeAt( trackPos.getYInt(), trackPos.getXInt() );
          if (trackShape instanceof TrackShape) {
            switchGridFigure.setSelectedTrackShape( (TrackShape) trackShape );
          }
        }
        if (toolboxFigure != null) {
          toolboxFigure.setSelectedTool( toolboxFigure.getAssignTool() );
        }
      }
    }
  }
}
