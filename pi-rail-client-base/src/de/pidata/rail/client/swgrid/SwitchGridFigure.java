/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.controller.base.ValueController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.figure.*;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.qnames.QName;
import de.pidata.rail.client.editcfg.EditCfgParamList;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.PanelCfg;
import de.pidata.rail.track.PanelRow;
import de.pidata.rect.*;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwitchGridFigure extends AbstractFigure implements EventListener {

  private int cellWidth;
  private int cellHeight;

  private PanelCfg panelCfg;
  private ToolboxFigure toolboxFigure;
  private EditTrackUI editTrackUI;
  private int width;
  private int height;
  private TrackShape[][] shapes;
  private ShapePI borderShape;
  private List<LocoShape> locoShapeList = new ArrayList<>();
  private ShapePI selectedTrackShape;
  private TableController actionTable;
  private Map<RailAction,TrackPos> modifiedTrackItems = new HashMap<>();
  private PathSelection pathSelection;


  public SwitchGridFigure( Rect bounds, PanelCfg panelCfg, TableController actionTable, int cellSize ) {
    super( bounds );
    setModifiable( false );
    this.panelCfg = panelCfg;
    this.width = panelCfg.panelColCount();
    this.height = panelCfg.panelRowCount();
    this.shapes = new TrackShape[height][width];
    this.actionTable = actionTable;
    this.cellWidth = cellSize;
    this.cellHeight = cellSize;
  }

  public Map<RailAction, TrackPos> getModifiedTrackItems() {
    return modifiedTrackItems;
  }

  @Override
  public synchronized int shapeCount() {
    return (width * height) + 1 + locoShapeList.size();
  }

  public void startEditMode( ToolboxFigure toolboxFigure, EditTrackUI editTrackUI ) {
    this.toolboxFigure = toolboxFigure;
    this.editTrackUI = editTrackUI;
  }

  @Override
  public synchronized ShapePI getShape( int index ) {
    if (index == 0) {
      return getBorderShape();
    }
    index--;
    int numGridShapes = width * height;
    if (index < numGridShapes) {
      int row = index / width;
      int col = index % width;
      return getShapeAt( row, col );
    }
    else {
      index -= numGridShapes;
      if (index < locoShapeList.size()) {
        return locoShapeList.get( index );
      }
    }
    return null;
  }

  private RailAction getRailActionAt( QName ctrlPanelID, int x, int y ) {
    for (ActionGroup actionGroup : PiRail.getInstance().getModelRailway().actionGroupIter()) {
      for (RailAction railAction : actionGroup.railActionIter()) {
        TrackPos trackPos = getTrackPos( railAction );
        if (trackPos != null) {
          if ((trackPos.getPanelID() == ctrlPanelID) && (trackPos.getXInt() == x) && (trackPos.getYInt() == y)) {
            return railAction;
          }
        }
      }
    }
    return null;
  }

  public TrackShape getShapeAt( int row, int col ) {
    if ((col >= 0) && (col < width) && (row >= 0) && (row < height)) {
      TrackShape shapePI = shapes[row][col];
      if (shapePI == null) {
        Rect figureBounds = getBounds();
        double posX = 1 + col * cellWidth;
        double posY = 1 + row * cellHeight;
        char ch = panelCfg.getPanelRowList().get( row ).getCols().charAt( col );
        TrackPart trackPart = TrackPart.getByChar( ch );
        if (trackPart != null) {
          RailAction railAction = getRailActionAt( panelCfg.getId(), col, row );
          ShapeStyle style = new ShapeStyle( ComponentColor.LIGHTGRAY, (QName) null );
          int angle = trackPart.getAngleByChar( ch );
          int rot = trackPart.getRotByChar( ch );
          Pos center = new RelativePos( figureBounds, posX + (cellWidth / 2), posY + (cellHeight / 2) );
          Rotation rotation = new Rotation( null, rot, center );
          RectRelatedRect bounds = new RectRelatedRect( figureBounds, 0.0, posX, 0.0, posY, cellWidth, cellHeight, rotation );
          shapePI = new TrackShape( this, bounds, trackPart, angle, style, railAction );
          shapes[row][col] = shapePI;
        }

      }
      return shapePI;
    }
    else {
      return null;
    }
  }


  public ShapePI getBorderShape() {
    if (borderShape == null) {
      borderShape = new RectanglePI( this, getBounds(), new ShapeStyle( ComponentColor.BLACK, ComponentColor.TRANSPARENT ) );
    }
    return borderShape;
  }

  public void assignRailAction( TrackPos trackPos, RailAction trackAction ) {
    if (trackPos != null) {
      int x = trackPos.getXInt();
      int y = trackPos.getYInt();
      TrackShape trackShape = getShapeAt( y, x );
      if (trackShape != null) {
        trackShape.setRailAction( trackAction );
      }
    }
  }

  private LocoShape findLocoShape( Locomotive locomotive ) {
    for (LocoShape shape : locoShapeList) {
      if (shape.getLocomotive().getId() == locomotive.getId()) {
        return shape;
      }
    }
    return null;
  }

  public synchronized void updateLocoPos( Locomotive locomotive ) {
    LocoShape locoShape = findLocoShape( locomotive );
    TrackPos trackPos = locomotive.getLastEventPos();
    if ((trackPos == null) || (trackPos.getPanelID() != this.panelCfg.getId())) {
      if (locoShape != null) {
        // remove locoShape if no longer visible on this panel
        locoShapeList.remove( locoShape );
        PaintViewPI paintViewPI = getPaintView();
        if (paintViewPI != null) {
          paintViewPI.shapeRemoved( this, locoShape );
        }
      }
    }
    else {
      double posX = getBounds().getX() + trackPos.getXInt() * cellWidth;
      double posY = getBounds().getY() + trackPos.getYInt() * cellHeight - (cellHeight/3);
      if (locoShape == null) {
        ShapeStyle style = new ShapeStyle( ComponentColor.ORANGE, (QName) null );
        Rect bounds = new SimpleRect( posX, posY, LocoShape.LOCO_SHAPE_WIDTH, LocoShape.LOCO_SHAPE_HEIGHT );
        locoShape = new LocoShape( this, bounds, style, locomotive );
        locoShapeList.add( locoShape );
        PaintViewPI paintViewPI = getPaintView();
        if (paintViewPI != null) {
          paintViewPI.figureModified( this, null ); // force repaint
        }
      }
      else {
        Rect bounds = locoShape.getBounds();
        bounds.setPos( posX, posY );
      }
    }
  }

  private TrackPos getTrackPos( RailAction railAction ) {
    TrackPos trackPos = modifiedTrackItems.get( railAction );
    if (trackPos == null) {
      return railAction.getTrackPos();
    }
    else {
      return trackPos;
    }
  }

  private TrackPos updateTrackPos( TrackPos trackPos, TrackShape shapePI, RailAction railAction ) {
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (getShapeAt( y, x ) == shapePI) {
          if ((trackPos.getXInt() != x) || (trackPos.getYInt() != y)
              || (trackPos.getPanelID() != panelCfg.getId())
              || ((railAction != shapePI.getRailAction()))) {
            if (trackPos.getParent( false ) != null) {
              trackPos = (TrackPos) trackPos.clone( null, true, false );
            }
            trackPos.setX( Integer.valueOf( x ) );
            trackPos.setY( Integer.valueOf( y ) );
            trackPos.setPanelID( panelCfg.getId() );
            shapePI.setRailAction( railAction );
            return trackPos;
          }
          else {
            return null;
          }
        }
      }
    }
    throw new IllegalArgumentException( "Internal error: Shape not found in grid" );
  }

  private void updatePanelCfg( TrackShape trackShape, int angle ) {
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (getShapeAt( y, x ) == trackShape) {
          PanelRow panelRow = panelCfg.getPanelRowList().get( y );
          StringBuilder newRow = new StringBuilder( panelRow.getCols() );
          TrackPart trackPart = trackShape.getTrackPart();
          newRow.setCharAt( x, trackPart.getCharByAngle( angle ) );
          panelRow.setCols( newRow.toString() );
        }
      }
    }
  }

  public void setSelectedTrackShape( TrackShape trackShape ) {
    if (selectedTrackShape != null) {
      ShapePI borderShape = selectedTrackShape;
      borderShape.getShapeStyle().setBorderColor( ComponentColor.LIGHTGRAY );
      borderShape.getShapeStyle().setStrokeWidth( 1 );

      saveEditAction();
    }

    if (trackShape != null) {
      trackShape.getShapeStyle().setBorderColor( ComponentColor.ORANGE );
      trackShape.getShapeStyle().setStrokeWidth( 5 );
      selectedTrackShape = trackShape;
      TrackPos editTrackPos = this.editTrackUI.getEditTrackPos();
      RailAction railAction = trackShape.getRailAction();
      if (railAction == null) {
        editTrackPos.update( new TrackPos() );
        this.editTrackUI.setEditActionID( null );
      }
      else {
        TrackPos trackPos = getTrackPos( railAction );
        editTrackPos.update( trackPos );
        this.editTrackUI.setEditActionID( railAction.getId() );
      }
    }
  }

  public void saveEditAction() {
    TrackPos editTrackPos = this.editTrackUI.getEditTrackPos();
    Controller focusCtrl = getPaintView().getController().getDialogController().getFocusController();
    if (focusCtrl instanceof ValueController) {
      ((ValueController) focusCtrl).saveValue();
    }
    if (selectedTrackShape instanceof TrackShape) {
      TrackShape oldTrackShape = ((TrackShape) selectedTrackShape);
      RailAction oldRailAction = oldTrackShape.getRailAction();
      if (oldRailAction != null) {
        TrackPos oldTrackPos = getTrackPos( oldRailAction );
        if ((oldTrackPos != null) && (editTrackPos.getPanelID() != null)) {
          if (oldTrackPos.getParent( false ) != null) {
            oldTrackPos = (TrackPos) oldTrackPos.clone( null, true, false );
          }
          boolean modified = oldTrackPos.update( editTrackPos );
          if (this.panelCfg.getId() != oldTrackPos.getPanelID()) {
            oldTrackPos.setPanelID( this.panelCfg.getId() );
            modified = true;
          }
          if (modified) {
            modifiedTrackItems.put( oldRailAction, oldTrackPos );
          }
        }
      }
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (eventID == EventListener.ID_MODEL_DATA_CHANGED) {
      if (modelID == RailFunction.ID_CURRENT_ICON) {
        if (source instanceof RailAction) {
          RailAction railAction = (RailAction) source;
          TrackPos trackPos = getTrackPos( railAction );
          if ((trackPos != null) && (trackPos.getPanelID() == panelCfg.getId())) {
            assignRailAction( trackPos, railAction );
          }
        }
      }
      else if (modelID == Locomotive.ID_LAST_EVENT) {
        Locomotive locomotive = (Locomotive) source;
        updateLocoPos( locomotive );
      }
    }
    else if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
      if (modelID == ActionGroup.ID_RAILACTION) {
        RailAction railAction = (RailAction) newValue;
        TrackPos trackPos = getTrackPos( railAction );
        if ((trackPos != null) && (trackPos.getPanelID() == panelCfg.getId())) {
          assignRailAction( trackPos, railAction );
        }
      }
      else if (modelID == ModelRailway.ID_LOCO) {
        updateLocoPos( (Locomotive) newValue );
      }
    }
  }

  /**
   * Called by UI when this shape is selected. Shows/hides resize handles
   *
   * @param button
   * @param x
   * @param y
   * @param shapePI
   */
  @Override
  public void onMousePressed( int button, double x, double y, ShapePI shapePI ) {

    if (button == 1) {

      if (toolboxFigure != null) {
        BitmapPI selectedTool = toolboxFigure.getSelectedTool();
        if (selectedTool == toolboxFigure.getAssignTool()) {
          if (actionTable != null) {
            // Assign railAction to selected shape in grid but do not modify original
            // trackPos, because user might press cancel
            RailAction selectedRailAction = (RailAction) actionTable.getSelectedRow( 0 );
            TrackShape trackShape = ((TrackShape) shapePI);
            RailAction oldRailAction = trackShape.getRailAction();
            if ((oldRailAction != null) && (oldRailAction != selectedRailAction)) {
              TrackPos trackPos = getTrackPos( oldRailAction );
              if (trackPos.getParent( false ) == oldRailAction) {
                // we got original trackPos, so make a copy
                trackPos = (TrackPos) trackPos.clone( null, true, false );
              }
              trackPos.setX( Integer.valueOf( -1 ) );
              trackPos.setY( Integer.valueOf( -1 ) );
              modifiedTrackItems.put( oldRailAction, trackPos );
            }
            if (selectedRailAction != null) {
              TrackPos trackPos = getTrackPos( selectedRailAction );
              if (trackPos == null) {
                trackPos = new TrackPos();
              }
              else {
                if (trackPos.getPanelID() == panelCfg.getId()) {
                  TrackShape oldShape = getShapeAt( trackPos.getYInt(), trackPos.getXInt() );
                  if ((oldShape != null) && (oldShape.getRailAction() == selectedRailAction)) {
                    oldShape.setRailAction( null );
                  }
                }
              }
              trackPos = updateTrackPos( trackPos, trackShape, selectedRailAction );
              if (trackPos != null) { // trackPos is null if pos not modified
                trackShape.setRailAction( selectedRailAction );
                modifiedTrackItems.put( selectedRailAction, trackPos );
              }
            }
            setSelectedTrackShape( trackShape );
            if (toolboxFigure != null) {
              toolboxFigure.setSelectedTool( toolboxFigure.getShowTool() );
            }
          }
        }
        else if (selectedTool == toolboxFigure.getShowTool()) {
          if (shapePI instanceof TrackShape) {
            setSelectedTrackShape( (TrackShape) shapePI );
          }
        }
        else if (selectedTool instanceof ToolShape) {
          TrackShape trackShape = (TrackShape) shapePI;
          int angle = ((ToolShape) selectedTool).getAngle();
          RailAction railAction = trackShape.getRailAction();
          if (railAction != null) {
            trackShape.setRailAction( null );
            modifiedTrackItems.put( railAction, null );
          }
          trackShape.setTrackPart( ((ToolShape) selectedTool).getTrackPart(), angle );
          updatePanelCfg( (TrackShape) shapePI, angle );
          setSelectedTrackShape( trackShape );
        }
      }
      else if (shapePI instanceof TrackShape) {

        TrackShape trackShape = (TrackShape) shapePI;

        if (pathSelection != null) {
          if (pathSelection.getStateScript( trackShape ) == null || pathSelection.getStartShape() == trackShape) {
            pathSelection.resetColors();
            pathSelection = null;
            return;
          }
        }

        if (trackShape.getRailAction() != null && trackShape.getRailAction().stateCount() > 0) {
          if (trackShape.getRailAction().getLockID() == null) {
            trackShape.setBitmapID( GuiBuilder.NAMESPACE.getQName( "icons/update.png" ) );
          }
        }

        RailAction railAction = trackShape.getRailAction();
        if (railAction != null) {
          if (railAction instanceof RailFunction) {
            RailFunction railFunction = (RailFunction) railAction;
            if (railFunction.getType() == FunctionType.Sign) {
              SystemManager sysMan = SystemManager.getInstance();
              StringBuilder msg = new StringBuilder( sysMan.getGlossaryString( "sign" ) + " " );
              msg.append( railFunction.getId().getName() );
              ActionState actionState = railFunction.getActionState();
              if (actionState != null) {
                msg.append( "\n" ).append( sysMan.getGlossaryString( "command" ) ).append( ": " );
                msg.append( "'" ).append( actionState.getCur() ).append( "'" );
              }
              TrackPos trackPos = railFunction.getTrackPos();
              if (trackPos != null) {
                if (trackPos.getPosID() != null) {
                  String posID = trackPos.getPosID().getName();
                  msg.append( "\nPosID=" ).append( posID );
                }
              }
              getPaintView().getController().getDialogController().showMessage( sysMan.getGlossaryString( "sign" ), msg.toString() );
            }
            else {
              String value = railFunction.getNextValue();
              char charValue = 0;
              if (!Helper.isNullOrEmpty( value )) {
                charValue = value.charAt( 0 );
              }
              PiRail.getInstance().sendSetCommand( railFunction, charValue, 0, null );
            }
          }
          else if (railAction instanceof RailBlock) {
            RailBlock railBlock = (RailBlock) railAction;

            // TODO: - Info nicht mehr als PopUp anzeigen, evtl Textfeld verwenden, zum. in Desktopversion

            if (pathSelection == null) {
              // Get list of paths (Fahrstraßen)
              List<EnumAction> pathList = railBlock.getPathList();

              if (pathList.size() > 0) {
                // Color the start block
                pathSelection = new PathSelection( trackShape );

                for (EnumAction enumAction : pathList) {
                  for (StateScript stateScript : enumAction.getOnStateList()) {
                    QName endPos = stateScript.getEndPos();
                    if (endPos != null) {
                      RailBlock endBlock = PiRail.getInstance().getModelRailway().getOrCreateBlock( endPos );
                      TrackPos trackPos = endBlock.getTrackPos();
                      if (trackPos != null) {
                        if (trackPos.getPanelID() == panelCfg.getId()) {
                          TrackShape endShape = getShapeAt( trackPos.getYInt(), trackPos.getXInt() );
                          trackPos.setPosID( endPos );
                          if (endShape != null) {
                            pathSelection.addEnd( endShape, stateScript );
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            else {
              StateScript selectedScript = pathSelection.getStateScript( trackShape );

              PiRail.getInstance().getModelRailway().executeScript( selectedScript );

              pathSelection.resetColors();
              pathSelection = null;
            }
          }
          else if (railAction instanceof RailTimer) {
            RailTimer railTimer = (RailTimer) railAction;
            TimerAction timerAction = (TimerAction) railTimer.getAction();
            SystemManager sysMan = SystemManager.getInstance();
            StringBuilder msg = new StringBuilder( sysMan.getGlossaryString( "balise" ) + " " );
            msg.append( railTimer.getId().getName() );
            if (timerAction != null) {
              String text = sysMan.getLocalizedMessage( "switchGridFigureDistance_MSG", null, null );
              msg.append( "\n" ).append( text ).append( ": " ).append( timerAction.getDistInt() ).append( " cm" );
            }
            ActionState actionState = railTimer.getActionState();
            if (actionState != null) {
              msg.append( "\n" ).append( sysMan.getGlossaryProps( "command" ) ).append(": '" );
              msg.append( actionState.getCur() ).append( "' = " ).append( Command.command4Char( actionState.getCurChar() ) );
              msg.append( " in " ).append( actionState.getCurInt() ).append( " cm" );
            }
            String text = sysMan.getLocalizedMessage( "switchGridFigureConnectedTo_MSG", null, null );
            msg.append( "\n\n" ).append( text ).append( ": " );
            msg.append( railTimer.getRailDevice().getDisplayName() );
            getPaintView().getController().getDialogController().showMessage( sysMan.getGlossaryString( "balise" ), msg.toString() );
          }
          else if (railAction instanceof RailMessage) {
            RailMessage railMessage = (RailMessage) railAction;
            TrackMsg trackMsg = (TrackMsg) railMessage.getAction();
            SystemManager sysMan = SystemManager.getInstance();
            StringBuilder msg = new StringBuilder( sysMan.getGlossaryString( "balise" ) + " " );
            TrackPos trackPos = null;
            if (trackMsg != null) {
              trackPos = trackMsg.getTrackPos();
              if (trackPos != null) {
                String posID = "null";
                if (trackPos.getPosID() != null) {
                  posID = trackPos.getPosID().getName();
                }
                msg.append( posID );
              }
            }
            msg.append( "\nTag ID=" ).append( railMessage.getId().getName() );
            if (trackPos != null) {
              MsgState msgState = PiRail.getInstance().getModelRailway().getOrCreateMsgState( trackMsg );
              msg.append( "\n" ).append( sysMan.getLocalizedMessage("switchGridFigureDistance_MSG", null, null  ) );
              msg.append( ": " ).append( msgState.getDistInt() ).append( " cm" );
              msg.append( "\n" ).append( sysMan.getGlossaryString( "command" )).append( ": '" );
              msg.append( msgState.getCmd() ).append( "' = " ).append( Command.command4Char( msgState.getCmdChar() ) );
              msg.append( " in " ).append( msgState.getCmdDistInt() ).append( " cm" );
            }
            getPaintView().getController().getDialogController().showMessage( sysMan.getGlossaryString( "balise" ), msg.toString() );
          }
        }
      }
    }
    else if (button == 2) {
      if (toolboxFigure == null) {
        if (shapePI instanceof TrackShape) {
          RailAction railAction = ((TrackShape) shapePI).getRailAction();
          if (railAction != null) {
            if (railAction instanceof RailBlock) {
              SystemManager sysMan = SystemManager.getInstance();
              RailBlock railBlock = (RailBlock) railAction;
              QName unlockID = railBlock.getReservedForID();
              if (unlockID == null) {
                StringBuilder msg = new StringBuilder( sysMan.getGlossaryString( "block" ) + " " );
                msg.append( railBlock.getId().getName() );
                QName reservedFor = railBlock.getReservedForID();
                Locomotive currentLoco = railBlock.getCurrentLoco();
                if (currentLoco == null) {
                  if (reservedFor == null) {
                    msg.append( "\n - " ).append( sysMan.getLocalizedMessage("switchGridFigureStateFree_MSG", null, null  ) );
                  }
                  else {
                    msg.append( "\n - " ).append( sysMan.getLocalizedMessage("switchGridFigureReserved_MSG", null, null  ) );
                    msg.append( " " ).append( reservedFor.getName() );
                  }
                }
                else {
                  msg.append( "\n - " ).append( sysMan.getLocalizedMessage("switchGridFigureOccupied_MSG", null, null  ) );
                  msg.append( " " ).append( currentLoco.getDisplayName() );
                }
                msg.append( "\n\n").append( sysMan.getLocalizedMessage("switchGridFigureExitSignals_MSG", null, null  ) );
                msg.append( ":\n - ").append( sysMan.getGlossaryString( "right" ) ).append( ": " );
                RailFunction signal = railBlock.getExitSignal( MotorState.CHAR_FORWARD );
                if (signal == null) {
                  msg.append( "---" );
                }
                else {
                  msg.append( signal.getId().getName() );
                  msg.append( "\n    ").append( sysMan.getGlossaryString( "command" ) ).append( ": " ).append( Command.command4Char( signal.getStateChar() ) );
                }
                msg.append( "\n - ").append( sysMan.getGlossaryString( "left" ) ).append( ": " );
                signal = railBlock.getExitSignal( MotorState.CHAR_BACK );
                if (signal == null) {
                  msg.append( "---" );
                }
                else {
                  msg.append( signal.getId().getName() );
                  msg.append( "\n    ").append( sysMan.getGlossaryString( "command" ) ).append( ": " ).append( Command.command4Char( signal.getStateChar() ) );
                }
                getPaintView().getController().getDialogController().showMessage( sysMan.getGlossaryString( "block" ), msg.toString() );

              }
              else {
                Controller ctrl = getPaintView().getController();
                ctrl.getDialogController().subAction( UnlockBlock.getInstance(), null, ctrl, railAction );
              }
            }
            RailDevice railDevice = railAction.getRailDevice();
            if (railDevice != null) {
              String deviceType = railDevice.getDeviceType();
              EditCfgParamList parameterList = new EditCfgParamList( railDevice.getId(), railDevice.getDisplayName(), railDevice.getAddress().getInetAddress(), deviceType );
              if (railDevice instanceof SwitchBox) {
                String title = SystemManager.getInstance().getLocalizedMessage( "switchGridFigureEditSwitchBox_H", null, null );
                getPaintView().getController().getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_switchbox" ), title, parameterList );
              }
            }
          }
        }
      }
    }
  }


  /**
   * Called by UI while handle is being moved.
   *
   * @param button
   * @param deltaX  horizontal delta to start point of move
   * @param deltaY  vertical delta to start point of move
   * @param shapePI
   */
  @Override
  public void onMouseDragging( int button, double deltaX, double deltaY, ShapePI shapePI ) {
    // do nothing
  }
}
