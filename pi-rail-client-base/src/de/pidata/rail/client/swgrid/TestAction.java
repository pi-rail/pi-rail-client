package de.pidata.rail.client.swgrid;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.RailFunction;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;

public class TestAction extends GuiDelegateOperation<EditSwitchGridDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditSwitchGridDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    if (dataContext instanceof RailFunction) {
      RailFunction railFunction = (RailFunction) dataContext;
      String value = railFunction.getNextValue();
      char charValue = 0;
      if (!Helper.isNullOrEmpty( value )) {
        charValue = value.charAt( 0 );
      }
      PiRail.getInstance().sendSetCommand( railFunction, charValue, 0, null );
    }
  }
}
