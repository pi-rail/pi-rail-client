package de.pidata.rail.client.swgrid;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.model.TrackPos;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class EditPosID extends GuiDelegateOperation<EditSwitchGridDelegate> {

  public String title = "";

  @Override
  protected void execute( QName eventID, EditSwitchGridDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    TrackPos trackPos = delegate.getEditTrackPos();
    if (trackPos != null) {
      QName posID = trackPos.getPosID();
      String oldValue = null;
      if (posID != null) {
        oldValue = posID.getName();
      }
      this.title = SystemManager.getInstance().getLocalizedMessage( "editPosIDValue_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "editPosIDValue_TXT", null, null );
      String btnOk = SystemManager.getInstance().getGlossaryString( "ok" );
      String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
      showInput( sourceCtrl, this.title, text, oldValue, btnOk, btnCancel );
    }
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK && (resultList instanceof QuestionBoxResult)) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (title.equals( result.getTitle() )) {
        EditSwitchGridDelegate delegate = (EditSwitchGridDelegate) parentDlgCtrl.getDelegate();
        TrackPos trackPos = delegate.getEditTrackPos();
        if (trackPos != null) {
          String value = result.getInputValue();
          if (Helper.isNullOrEmpty( value )) {
            trackPos.setPosID( null );
          }
          else{
            String msg = TrackPos.checkPosID( value );
            if (msg == null) {
              trackPos.setPosID( PiRailFactory.NAMESPACE.getQName( value ) );
            }
            else {
              Properties params = new Properties();
              params.put( "name", value );
              this.title = SystemManager.getInstance().getLocalizedMessage( "editPosIDInvalid_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "editPosIDInvalid_TXT", null, params );
              parentDlgCtrl.showMessage( this.title, text + "\n" + msg );
            }
          }
        }
      }
    }
  }
}
