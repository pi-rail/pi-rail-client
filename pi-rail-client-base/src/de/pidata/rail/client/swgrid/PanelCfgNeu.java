package de.pidata.rail.client.swgrid;

import de.pidata.gui.controller.base.*;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.track.PanelCfg;
import de.pidata.rail.track.PanelRow;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class PanelCfgNeu extends GuiDelegateOperation<EditSwitchGridDelegate> {

  private String title = "";

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditSwitchGridDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditTrackUI editTrackUI = (EditTrackUI) dataContext;
    TrackCfg trackCfg = editTrackUI.getTrackCfg();
    PanelCfg panelCfg = null;
    if (trackCfg != null) {
      panelCfg = editTrackUI.getTrackCfg().getPanelCfg();
    }
    if (panelCfg == null) {
      this.title = SystemManager.getInstance().getLocalizedMessage( "panelCfgNeuName_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "panelCfgNeuName_TXT", null, null );
      String btnOk = SystemManager.getInstance().getGlossaryString( "create" );
      String btnCancel = SystemManager.getInstance().getGlossaryString( "cancel" );
      source.getDialogController().showInput( this.title, text, "", btnOk, btnCancel );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && title.equals( result.getTitle() )) {
        String name = result.getInputValue();
        String msg = PanelCfg.checkID( name );
        if (msg != null) {
          Properties params = new Properties();
          params.put( "name", name );
          this.title = SystemManager.getInstance().getLocalizedMessage( "panelCfgNeuInvalidName_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "panelCfgNeuInvalidName_TXT", null, params );
          parentDlgCtrl.showMessage( title, text + "\n" + msg );
          return;
        }
        EditTrackUI editTrackUI = (EditTrackUI) parentDlgCtrl.getModel();
        TrackCfg trackCfg = editTrackUI.getTrackCfg();
        if (trackCfg == null) {
          trackCfg = new TrackCfg( PiRailFactory.NAMESPACE.getQName( editTrackUI.getDeviceName() ) );
          trackCfg.setDeviceAddress( editTrackUI.getInetAddress() );
          trackCfg.setVersion( Integer.valueOf( 1 ) );
          editTrackUI.setTrackCfg( trackCfg );
        }
        PanelCfg panelCfg = new PanelCfg( PiRailFactory.NAMESPACE.getQName( name ) );
        for (int i = 0; i < 10; i++) {
          PanelRow panelRow = new PanelRow( "          " );
          panelCfg.addPanelRow( panelRow );
        }
        trackCfg.setPanelCfg( panelCfg );
        ((EditSwitchGridDelegate) parentDlgCtrl.getDelegate()).setPanelCfg( panelCfg );
      }
    }
    else {
      super.dialogClosed( parentDlgCtrl, resultOK, resultList );
    }
  }
}
