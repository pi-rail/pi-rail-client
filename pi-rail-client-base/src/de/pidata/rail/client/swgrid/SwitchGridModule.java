/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.PaintController;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.track.*;
import de.pidata.rect.Rect;
import de.pidata.rect.SimpleRect;

import java.util.HashMap;
import java.util.Map;

public class SwitchGridModule extends ModuleGroup implements EventListener {

  private static final boolean TESTMODE = false;

  public static int cellSize = 40;  // TODO dynamic base on Screen size -- Android overwites cellSize in MobileMainDelegate

  private Map<QName,SwitchGridFigure> switchGridFigureMap = new HashMap<QName,SwitchGridFigure>();
  private RailroadCfg railroadCfg;
  private SectionCfg sectionCfg;

  @Override
  public synchronized void activate( UIContainer uiContainer ) {
    setModel( PiRail.getInstance().getModelRailway() );
    super.activate( uiContainer );
  }

  /**
   * Called by ModuleViewPI after ModuleGroup's UI Fragment has been loaded
   *
   * @param moduleContainer
   */
  @Override
  public synchronized void activateModule( UIContainer moduleContainer ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    setModel( modelRailway );
    modelRailway.addListener( this );
    super.activateModule( moduleContainer );


    SelectionController sectionCfgCtrl = (SelectionController) getController( ControllerBuilder.NAMESPACE.getQName( "sectionCfgTable" ) );

    if (sectionCfgCtrl != null) {
      SectionCfg sectionCfg = (SectionCfg) sectionCfgCtrl.getSelectedRow( 0 );
      if (sectionCfg != null) {
        setSectionCfg( sectionCfg );
      }
      sectionCfgCtrl.addListener( this );
      if (TESTMODE) {
        PaintController switchGrid = (PaintController) getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
        PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
        if (switchGridView.figureCount() == 0) {
          PanelCfg panelCfg = new PanelCfg( PiRailFactory.NAMESPACE.getQName( "test" ) );
          panelCfg.addPanelRow( new PanelRow( " ,---p----T--. " ) );
          panelCfg.addPanelRow( new PanelRow( "I     Q--D    i" ) );
          panelCfg.addPanelRow( new PanelRow( "|    --3*     a" ) );
          panelCfg.addPanelRow( new PanelRow( "|   --D       |" ) );
          panelCfg.addPanelRow( new PanelRow( "|  --D        |" ) );
          panelCfg.addPanelRow( new PanelRow( "|  -*         |" ) );
          panelCfg.addPanelRow( new PanelRow( "|             |" ) );
          panelCfg.addPanelRow( new PanelRow( "| ----p----T- |" ) );
          panelCfg.addPanelRow( new PanelRow( "A  ---T3-pt-- |" ) );
          panelCfg.addPanelRow( new PanelRow( "l   -*/   \\   j" ) );
          panelCfg.addPanelRow( new PanelRow( " +---*     +-* " ) );
          createSwitchGridFigure( panelCfg, switchGridView.getBounds(), switchGridView, modelRailway );
          modelRailway.addListener( this );
        }
      }
    }

  }
  @Override
  public synchronized void deactivateModule() {
    super.deactivateModule();
  }

  private void createSwitchGridFigure( PanelCfg panelCfg, Rect bounds, PaintViewPI switchGridView, ModelRailway modelRailway ) {
    SwitchGridFigure switchGridFigure;
    switchGridFigure = new SwitchGridFigure( bounds, panelCfg, null, cellSize );
    this.switchGridFigureMap.put( panelCfg.getId(), switchGridFigure );
    switchGridView.addFigure( switchGridFigure );
    modelRailway.addListener( switchGridFigure );
    for (Locomotive locomotive : modelRailway.locoIter()) {
      switchGridFigure.updateLocoPos( locomotive );
    }
  }

  private void addPanelCfg( PanelRef panelRef ) {
    ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
    PanelCfg panelCfg = modelRailway.getPanelCfg( panelRef.getRefID() );
    if (panelCfg != null) {
      PaintController switchGrid = (PaintController) getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
      PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
      double w = cellSize;
      double h = cellSize;
      SimpleRect bounds = new SimpleRect( panelRef.getXInt() * w, panelRef.getYInt() * h, 2 + panelCfg.panelColCount() * w, 2 + panelCfg.panelRowCount() * h );
      createSwitchGridFigure( panelCfg, bounds, switchGridView, modelRailway );
    }
  }

  public void setSectionCfg( SectionCfg sectionCfg ) {
    try {
      PaintController switchGrid = (PaintController) getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
      PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();

      if (this.sectionCfg != null) {
        for (SwitchGridFigure switchGridFigure : switchGridFigureMap.values()) {
          switchGridView.removeFigure( switchGridFigure );
        }
        switchGridFigureMap.clear();
      }
      if (sectionCfg != null) {
        this.sectionCfg = sectionCfg;
        for (PanelRef panelRef : sectionCfg.panelRefIter()) {
          addPanelCfg( panelRef );
        }
      }
      SelectionController sectionCtrl = (SelectionController) getController( GuiBuilder.NAMESPACE.getQName( "sectionCfgTable" ) );
      sectionCtrl.selectRow( this.sectionCfg );
    }
    catch (Exception e) {
      Logger.error("Error in setSectionCfg at SwitchGridModule", e);
    }
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (newValue instanceof SectionCfg) {
      if (eventID == EventListener.ID_MODEL_DATA_ADDED) {
        if (sectionCfg == null) {
          setSectionCfg( (SectionCfg) newValue );
        }
      }
      else if (newValue != sectionCfg) {
        setSectionCfg( (SectionCfg) newValue );
      }
    }
    else if ((modelID == ModelRailway.ID_TRACKCFG) && (eventID == EventListener.ID_MODEL_DATA_ADDED)) {
      try {
        PaintController switchGrid = (PaintController) getController( GuiBuilder.NAMESPACE.getQName( "switchGrid" ) );
        PaintViewPI switchGridView = (PaintViewPI) switchGrid.getView();
        TrackCfg trackCfg = (TrackCfg) newValue;
        ModelRailway modelRailway = PiRail.getInstance().getModelRailway();

        if (switchGridView.figureCount() == 0) {
          RailroadCfg railroadCfg = trackCfg.getRailroadCfg();
          if (railroadCfg != null) {
            this.railroadCfg = railroadCfg;
            SectionCfg sectionCfg  = railroadCfg.getSectionCfg( null );
            if (sectionCfg != null) {
              this.sectionCfg = sectionCfg;
              for (PanelRef panelRef : sectionCfg.panelRefIter()) {
                addPanelCfg( panelRef );
              }
            }
          }
        }
        else if (this.sectionCfg != null) {
          PanelCfg panelCfg = trackCfg.getPanelCfg();
          if (panelCfg != null) {
            QName panelID = panelCfg.getId();
            PanelRef panelRef = sectionCfg.getPanelRef( panelID );
            if (panelRef != null) {
              SwitchGridFigure switchGridFigure = switchGridFigureMap.get( panelID );
              if (switchGridFigure == null) {
                double w = cellSize;
                double h = cellSize;
                SimpleRect bounds = new SimpleRect( panelRef.getXInt() * w, panelRef.getYInt() * h, 2 + panelCfg.panelColCount() * w, 2 + panelCfg.panelRowCount() * h );
                createSwitchGridFigure( panelCfg, bounds, switchGridView, modelRailway );
              }
            }
          }
        }
      }
      catch (Exception e) {
        Logger.error("Error in eventOccured at SwitchGridModule", e);
      }
    }
  }
}
