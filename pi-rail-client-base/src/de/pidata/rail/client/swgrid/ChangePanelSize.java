package de.pidata.rail.client.swgrid;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.EditTrackUI;
import de.pidata.rail.track.PanelCfg;
import de.pidata.rail.track.PanelRow;
import de.pidata.rail.track.TrackCfg;
import de.pidata.service.base.ServiceException;

public class ChangePanelSize extends GuiDelegateOperation<EditSwitchGridDelegate> {

  private static final QName ID_PANEL_ROW_DEC = ControllerBuilder.NAMESPACE.getQName( "panelRowDec" );
  private static final QName ID_PANEL_ROW_INC = ControllerBuilder.NAMESPACE.getQName( "panelRowInc" );
  private static final QName ID_PANEL_COL_DEC = ControllerBuilder.NAMESPACE.getQName( "panelColDec" );
  private static final QName ID_PANEL_COL_INC = ControllerBuilder.NAMESPACE.getQName( "panelColInc" );

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param source      original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditSwitchGridDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    EditTrackUI editTrackUI = (EditTrackUI) dataContext;
    TrackCfg trackCfg = editTrackUI.getTrackCfg();
    PanelCfg panelCfg = null;
    if (trackCfg != null) {
      panelCfg = editTrackUI.getTrackCfg().getPanelCfg();
      if (panelCfg != null) {
        if (eventID == ID_PANEL_ROW_DEC) {
          PanelRow panelRow = (PanelRow) panelCfg.lastChild( PanelCfg.ID_PANELROW );
          panelCfg.removePanelRow( panelRow );
        }
        else if (eventID == ID_PANEL_ROW_INC) {
          StringBuilder cols = new StringBuilder();
          for (int i = 0; i < panelCfg.panelColCount(); i++) {
            cols.append( ' ' );
          }
          PanelRow panelRow = new PanelRow( cols.toString() );
          panelCfg.addPanelRow( panelRow );
        }
        else if (eventID == ID_PANEL_COL_DEC) {
          // Strings kürzen
          int newLen = panelCfg.panelColCount() - 1;
          for (PanelRow panelRow : panelCfg.panelRowIter()) {
            panelRow.setCols( panelRow.getCols().substring( 0, newLen ) );
          }
        }
        else if (eventID == ID_PANEL_COL_INC) {
          // Strings verlängern
          int newLen = panelCfg.panelColCount() - 1;
          for (PanelRow panelRow : panelCfg.panelRowIter()) {
            panelRow.setCols( panelRow.getCols() + " " );
          }
        }
      }
      // Update View
      delegate.setPanelCfg( panelCfg );
    }
  }
}
