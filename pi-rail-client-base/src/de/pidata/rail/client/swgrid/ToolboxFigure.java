/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.figure.*;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.TrackPart;
import de.pidata.rect.*;

public class ToolboxFigure extends AbstractFigure {

  private double cellWidth = 40.0;
  private double cellHeight = 40.0;
  private ToolShape[] tools = new ToolShape[11];
  private ShapePI[] rotations = new ShapePI[8];
  private BitmapPI selectedTool;
  private ShapePI selectedRotation;
  private ShapePI frame;
  private BitmapPI assignTool;
  private BitmapPI showTool;

  public ToolboxFigure( Rect figureBounds ) {
    super( figureBounds );

    int i = 0;
    tools[i] = createTool( TrackPart.STRAIGHT, i ); i++;
    tools[i] = createTool( TrackPart.BOW_L, i ); i++;
    tools[i] = createTool( TrackPart.BOW_R, i ); i++;
    tools[i] = createTool( TrackPart.RIGHT, i ); i++;
    tools[i] = createTool( TrackPart.LEFT, i  ); i++;
    tools[i] = createTool( TrackPart.THREE, i ); i++;
    tools[i] = createTool( TrackPart.LIGHT, i ); i++;
    tools[i] = createTool( TrackPart.CROSS, i ); i++;
    tools[i] = createTool( TrackPart.IR_STRAIGHT, i ); i++;
    tools[i] = createTool( TrackPart.SENSOR_STRAIGHT, i ); i++;
    tools[i] = createTool( TrackPart.EMPTY, i ); i++;

    i = 0;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_up.png" ), i );  i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_diag.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_up.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_diag.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_up.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_diag.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_up.png" ), i ); i++;
    rotations[i] = createRotation( ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_diag.png" ), i ); i++;

    setSelectedTool( tools[0] );
    setSelectedRotation( rotations[0] );
  }

  private ToolShape createTool( QName trackPartName, int index ) {
    TrackPart trackPart = TrackPart.getByName( trackPartName );
    ShapeStyle style = new ShapeStyle( ComponentColor.LIGHTGRAY, null, 2.0 );
    double posY = 10.0 + index * (cellHeight + 10);
    double posX = 10.0;
    Pos center = new SimplePos( posX + (cellWidth / 2), posY + (cellHeight / 2) );
    Rotation rotation = new Rotation( null, 0, center );
    SimpleRectDir bounds = new SimpleRectDir( posX, posY, cellWidth, cellHeight, rotation );
    return new ToolShape( this, bounds, trackPart, style );
  }

  private ShapePI createRotation( QName imageID, int index ) {
    ShapeStyle style = new ShapeStyle( ComponentColor.LIGHTGRAY, null, 2.0 );
    double posY = 10.0 + index * (cellHeight + 10);
    double posX = cellWidth + 20.0;
    Pos center = new SimplePos( posX + (cellWidth / 2), posY + (cellHeight / 2) );
    int angle = index * 45;
    int rot = (index / 2) * 90;
    Rotation rotation = new Rotation( null, rot, center );
    SimpleRectDir bounds = new SimpleRectDir( posX, posY, cellWidth, cellHeight, rotation );
    return new ToolRotation( this, bounds, imageID, style, angle );
  }

  @Override
  public int shapeCount() {
    return tools.length + rotations.length + 3;
  }

  @Override
  public ShapePI getShape( int index ) {
    if (index == 0) {
      if (frame == null) {
        double width = (2 * cellWidth) + 30;
        double heigth = 20 + (tools.length * (cellHeight + 10));
        ShapeStyle style = new ShapeStyle( ComponentColor.DARKGRAY, ComponentColor.TRANSPARENT, 2.0 );
        frame = new RectanglePI( this, new SimpleRect( 0, 0, width, heigth ), style );
      }
      return frame;
    }
    index--;
    if (index < tools.length) {
      return tools[index];
    }
    index -= tools.length;
    if (index == 0) {
      if (assignTool == null) {
        double posY = 10.0 + (rotations.length * (cellHeight + 10));
        double posX = cellWidth + 20.0;
        ShapeStyle style = new ShapeStyle( ComponentColor.BLACK, null, 2.0 );
        QName imageID = ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_edit.png" );
        assignTool = new BitmapPI( this, new SimpleRect( posX, posY, cellWidth, cellHeight ), imageID, style );
      }
      return assignTool;
    }
    index--;
    if (index == 0) {
      if (showTool == null) {
        double posY = 10.0 + ((rotations.length + 1) * (cellHeight + 10));
        double posX = cellWidth + 20.0;
        ShapeStyle style = new ShapeStyle( ComponentColor.BLACK, null, 2.0 );
        QName imageID = ControllerBuilder.NAMESPACE.getQName( "icons/track/icon_show.png" );
        showTool = new BitmapPI( this, new SimpleRect( posX, posY, cellWidth, cellHeight ), imageID, style );
      }
      return showTool;
    }
    index--;
    if (index < rotations.length) {
      return rotations[index];
    }
    return null;
  }

  public void setSelectedTool( BitmapPI newTool ) {
    if (this.selectedTool != null) {
      this.selectedTool.getShapeStyle().setBorderColor( ComponentColor.LIGHTGRAY );
    }
    if (newTool != null) {
      newTool.getShapeStyle().setBorderColor( ComponentColor.BLUE );
    }
    this.selectedTool = newTool;
  }

  private void setSelectedRotation( ShapePI shapePI ) {
    if (selectedRotation != null) {
      selectedRotation.getShapeStyle().setBorderColor( ComponentColor.LIGHTGRAY );
    }
    shapePI.getShapeStyle().setBorderColor( ComponentColor.ORANGE );
    selectedRotation = shapePI;

    int angle = ((ToolRotation) shapePI).getAngle();
    for (ToolShape toolShape : tools) {
      toolShape.setAngle( angle );
    }
  }

  public BitmapPI getAssignTool() {
    return assignTool;
  }

  public BitmapPI getShowTool() {
    return showTool;
  }

  public BitmapPI getSelectedTool() {
    return selectedTool;
  }

  /**
   * Called by UI when this shape is selected. Shows/hides resize handles
   *
   * @param button
   * @param x
   * @param y
   * @param shapePI
   */
  @Override
  public void onMousePressed( int button, double x, double y, ShapePI shapePI ) {
    if (shapePI != null) {
      if (shapePI == assignTool) {
        setSelectedTool( assignTool );
      }
      else if (shapePI == showTool) {
        setSelectedTool( showTool );
      }
      else if (shapePI instanceof ToolRotation) {
        setSelectedRotation( shapePI );
      }
      else if (shapePI instanceof ToolShape) {
        setSelectedTool( (ToolShape) shapePI );
      }
    }
  }

  /**
   * Called by UI while handle is being moved.
   *
   * @param button
   * @param deltaX  horizontal delta to start point of move
   * @param deltaY  vertical delta to start point of move
   * @param shapePI
   */
  @Override
  public void onMouseDragging( int button, double deltaX, double deltaY, ShapePI shapePI ) {
    // do nothing
  }
}
