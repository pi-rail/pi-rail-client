/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.figure.BitmapPI;
import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.rail.railway.TrackPart;
import de.pidata.rect.Rect;
import de.pidata.rect.Rotation;
import de.pidata.rect.SimpleRectDir;

public class ToolShape extends BitmapPI {

  private TrackPart trackPart;
  private int angle;

  public ToolShape( Figure figure, Rect bounds, TrackPart trackPart, ShapeStyle style ) {
    super( figure, bounds,  ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconStraight() ), style );
    this.trackPart = trackPart;
    this.angle = 0;
  }

  public TrackPart getTrackPart() {
    return trackPart;
  }

  public int getAngle() {
    return angle;
  }

  public void setAngle( int angle ) {
    this.angle = angle;
    int index = angle / 45;
    int rot = (index / 2) * 90;
    SimpleRectDir bounds = (SimpleRectDir) getBounds();
    Rotation oldRotation = bounds.getRotation();
    Rotation newRotation = new Rotation( oldRotation.getOwner(), rot, oldRotation.getCenter() );
    bounds.setRotation( newRotation );
    if ((index % 2) == 0) {
      setBitmapID(  ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconStraight() ) );
    }
    else {
      setBitmapID(  ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconDiag() ) );
    }
  }
}
