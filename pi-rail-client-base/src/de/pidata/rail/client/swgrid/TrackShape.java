/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.view.figure.BitmapPI;
import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.qnames.QName;
import de.pidata.rail.model.TrackPos;
import de.pidata.rail.railway.RailAction;
import de.pidata.rail.railway.TrackPart;
import de.pidata.rect.Rect;
import de.pidata.rect.RectRelatedRect;
import de.pidata.rect.Rotation;

public class TrackShape extends BitmapPI {

  private RailAction railAction;
  private boolean modified = false;
  private TrackPart trackPart;
  private int angle = 0;

  public TrackShape( Figure figure, Rect bounds, TrackPart trackPart, int angle, ShapeStyle style, RailAction railAction ) {
    super( figure, bounds, ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconStraight() ), style );
    this.trackPart = trackPart;
    this.angle = angle;
    setRailAction( railAction );
  }

  public TrackPart getTrackPart() {
    return trackPart;
  }

  public void setTrackPart( TrackPart trackPart, int angle ) {
    this.trackPart = trackPart;
    this.angle = angle;
    int index = angle / 45;
    int rot = (index / 2) * 90;
    RectRelatedRect bounds = (RectRelatedRect) getBounds();
    Rotation oldRotation = bounds.getRotation();
    Rotation newRotation = new Rotation( oldRotation.getOwner(), rot, oldRotation.getCenter() );
    bounds.setRotation( newRotation );
    applyBitmap();
  }


  public RailAction getRailAction() {
    return railAction;
  }

  public int getAngle() {
    return angle;
  }

  public void setRailAction( RailAction railAction ) {
    this.railAction = railAction;
    applyBitmap();
  }

  protected void applyBitmap() {
    QName imageID;
    int index = getAngle() / 45;
    boolean diag = ((index % 2) != 0);
    if (railAction == null) {
      if (diag) {
        imageID = ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconDiag() );
      }
      else {
        imageID = ControllerBuilder.NAMESPACE.getQName( "icons/track/" + trackPart.getIconStraight() );
      }
    }
    else {
      imageID = railAction.getCurrentImageID( diag );
    }
    setBitmapID( imageID );
    appearanceChanged(); // imported to apply possibly changed angle
  }

  public void setModified( boolean modified ) {
    this.modified = modified;
  }

  public boolean isModified() {
    return modified;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder( trackPart.getName().getName() );
    if (railAction != null) {
      buf.append( " " ).append( railAction.getId().getName() );
      TrackPos pos = railAction.getTrackPos();
      if (pos != null) {
        buf.append( "(" ).append( pos.getX() ).append( "," ).append( pos.getY() ).append( ")" );
      }
    }
    return buf.toString();
  }
}
