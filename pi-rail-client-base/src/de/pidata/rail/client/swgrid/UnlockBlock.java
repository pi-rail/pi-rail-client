package de.pidata.rail.client.swgrid;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.RailBlock;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class UnlockBlock extends GuiOperation {

  private static UnlockBlock instance = null;

  public synchronized static UnlockBlock getInstance() {
    if (instance == null) {
      instance = new UnlockBlock();
    }
    return instance;
  }

  private RailBlock railBlock;

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    this.railBlock = (RailBlock) dataContext;
    QName unlockID = railBlock.getReservedForID();
    if (unlockID != null) {
      Properties params = new Properties();
      params.put( "name", unlockID.getName() );
      String title = SystemManager.getInstance().getLocalizedMessage( "unlockBlockConfirm_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "unlockBlockConfirm_TXT", null, params );
      String btnYes = SystemManager.getInstance().getGlossaryString( "unlock" );
      String btnNo = SystemManager.getInstance().getGlossaryString( "cancel" );
      showQuestion( sourceCtrl, title, text, btnYes, btnNo, null );
    }
    else {
      this.railBlock = null;
    }
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if ((this.railBlock != null) && resultOK && (resultList instanceof QuestionBoxResult)) {
      PiRail.getInstance().getModelRailway().unlock( railBlock.getReservedForID().getName() );
      this.railBlock.reserveFor( null );
    }
    this.railBlock = null;
  }
}
