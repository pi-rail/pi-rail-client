package de.pidata.rail.client.swgrid;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.rail.model.StateScript;

import java.util.HashMap;
import java.util.Map;

public class PathSelection {

  private final TrackShape startShape;
  private Map<TrackShape, StateScript> endShapes = new HashMap<>();

  public PathSelection( TrackShape startShape ) {
    this.startShape = startShape;
    colorPathStart( startShape );
  }

  public void addEnd(TrackShape trackShape, StateScript stateScript ) {
    if (!endShapes.containsKey( trackShape )) {
      colorPathEnd(trackShape);
      endShapes.put( trackShape, stateScript );
    }
  }

  public StateScript getStateScript(TrackShape trackShape ) {
    return endShapes.get( trackShape );
  }

  public TrackShape getStartShape() {
    return startShape;
  }

  private void colorPathStart( TrackShape trackShape ) {
    trackShape.getShapeStyle().setBorderColor( ComponentColor.ORANGE );
    trackShape.getShapeStyle().setStrokeWidth( 4 );
  }

  private void colorPathEnd( TrackShape trackShape ) {
    trackShape.getShapeStyle().setBorderColor( ComponentColor.BLUE );
    trackShape.getShapeStyle().setStrokeWidth( 4 );
  }

  private void resetPathColor( TrackShape trackShape ) {
    trackShape.getShapeStyle().setBorderColor( ComponentColor.LIGHTGRAY );
    trackShape.getShapeStyle().setStrokeWidth( 1 );
  }

  public void resetColors() {
    resetPathColor( startShape );
    endShapes.keySet().forEach( this::resetPathColor );
  }
}
