/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.swgrid;

import de.pidata.gui.view.figure.BitmapPI;
import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.qnames.QName;
import de.pidata.rect.Rect;

public class ToolRotation extends BitmapPI {

  private int angle;

  public ToolRotation( Figure figure, Rect bounds, QName bitmapID, ShapeStyle style, int angle ) {
    super( figure, bounds, bitmapID, style );
    this.angle = angle;
  }

  public int getAngle() {
    return angle;
  }
}
