/*
 * This file is part of PI-Rail Android (https://gitlab.com/pi-rail/pi-rail-android).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

public class MobileMainDelegate implements DialogControllerDelegate<ParameterList, ParameterList> {

  public static final QName FRAGMENT_CONTAINER_LEFT = ControllerBuilder.NAMESPACE.getQName( "fragmentContainerLeft" );
  public static final QName FRAGMENT_CONTAINER_CENTER = ControllerBuilder.NAMESPACE.getQName( "fragmentContainerCenter" );
  public static final QName FRAGMENT_CONTAINER_RIGHT = ControllerBuilder.NAMESPACE.getQName( "fragmentContainerRight" );
  public static final QName FRAGMENT_CONTAINER_DOUBLE_SIZE = ControllerBuilder.NAMESPACE.getQName( "fragmentContainerDoubleSize" );

  public static final QName TAB_FRAGMENT = ControllerBuilder.NAMESPACE.getQName( "mobile_tab_fragment" );

  private DialogController dlgCtrl;
  private ModuleController leftPanel;
  private ModuleController centerPanel;
  private ModuleController rightPanel;
  private ModuleController doublePanel;
  private long lastBackPressed;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ParameterList parameterList ) throws Exception {
    this.dlgCtrl = dlgCtrl;
    return null;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
    // do nothing
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
    leftPanel = initializeContainer( FRAGMENT_CONTAINER_LEFT, TAB_FRAGMENT );
    centerPanel = initializeContainer( FRAGMENT_CONTAINER_CENTER, TAB_FRAGMENT );
    rightPanel = initializeContainer( FRAGMENT_CONTAINER_RIGHT, TAB_FRAGMENT );
    doublePanel = initializeContainer( FRAGMENT_CONTAINER_DOUBLE_SIZE, TAB_FRAGMENT );
  }

  public ModuleController initializeContainer( QName containerId, QName moduleId ) {
    ModuleController moduleCtrl = (ModuleController) dlgCtrl.getController( containerId );
    try {
      if (moduleCtrl != null) {
        dlgCtrl.getControllerBuilder().loadModule( moduleCtrl, moduleId, dlgCtrl, "." );
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return moduleCtrl;
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    long currentTime = System.currentTimeMillis();
    long lastBackDiff = currentTime - lastBackPressed;
    if(lastBackDiff<=5000 ) {
      //close Application after clicking back button twice in 5 Seconds
      dlgCtrl.close( true );
    }
    else{
      dlgCtrl.showToast( "Nochmaliges Tippen beendet die App." );
      lastBackPressed = currentTime;
    }
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation
   *
   * @param dlgCtrl    parent of the closed dialog
   * @param resultOK   if true dialog was closed with ok, false if closed with cancel
   * @param resultList result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    // do nothing
  }

  /**
   * Called after a popup has been closed that was requested to open within this guiOperation
   *
   * @param oldModuleGroup of the closed popup
   */
  @Override
  public void popupClosed( ModuleGroup oldModuleGroup ) {
    // do nothing
  }

  public void updateLayout( boolean leftVisible, boolean centerVisible, boolean rightVisible, boolean doubleVisible ) {
    leftPanel.getView().setVisible( false );
    centerPanel.getView().setVisible( false );
    rightPanel.getView().setVisible( false );
    doublePanel.getView().setVisible( false );
    if(leftPanel != null){
      leftPanel.getView().setVisible( leftVisible );
    }
    if(centerPanel != null){
      centerPanel.getView().setVisible( centerVisible );
    }
    if (rightPanel != null){
      rightPanel.getView().setVisible( rightVisible );
    }
    if(doublePanel != null){
      doublePanel.getView().setVisible( doubleVisible );
    }
  }
}
