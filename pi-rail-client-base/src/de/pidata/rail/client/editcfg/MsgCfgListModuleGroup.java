package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.binding.Selection;
import de.pidata.rail.model.TimerAction;

public class MsgCfgListModuleGroup extends ModuleGroup {

  public TimerAction getSelectedMsgCfg() {
    TableController tableCtrl = (TableController) this.getController( ControllerBuilder.NAMESPACE.getQName( "msgCfgTable" ) );
    Selection selection = tableCtrl.getSelection();
    int selectedValueCount = selection.selectedValueCount();
    if(selectedValueCount == 1){
      TimerAction selectedValue = (TimerAction) selection.getSelectedValue( 0 );
      return selectedValue;
    }
    return null;
  }
}