package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.binding.Selection;
import de.pidata.rail.model.EnumAction;

public class FunctionListModuleGroup extends ModuleGroup {
  public EnumAction getSelectedFunction(){
    TableController tableCtrl = (TableController) this.getController( ControllerBuilder.NAMESPACE.getQName( "funcTable" ) );
    Selection selection = tableCtrl.getSelection();
    int selectedValueCount = selection.selectedValueCount();
    if(selectedValueCount == 1){
      EnumAction selectedValue = (EnumAction) selection.getSelectedValue( 0 );
      return selectedValue;
    }
    return null;
  }
}
