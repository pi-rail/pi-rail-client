package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.binding.Selection;
import de.pidata.rail.model.EnumAction;

public class TurnoutListModuleGroup extends ModuleGroup {
  
  public EnumAction getSelectedTurnout(){
    TableController tableController = (TableController) this.getController( ControllerBuilder.NAMESPACE.getQName( "turnTable" ) );
    Selection selection1 = tableController.getSelection();
    int selectedValueCount1 = selection1.selectedValueCount();
    if(selectedValueCount1 == 1){
      EnumAction selectedValue1 = (EnumAction) selection1.getSelectedValue( 0 );
      return selectedValue1;
    }
    return null;
  }
}
