package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.client.uiModels.EditCfgUI;
import de.pidata.rail.model.Cfg;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;

public class SaveName extends SaveCfgOperation<EditNameDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, EditNameDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    EditCfgUI model = (EditCfgUI) dataContext;
    Cfg config = model.getCfg();
    if (config == null) {
      Logger.info( "Could not upload configuration, UI Model is null" );
      return;
    }
    String deviceName = model.getDeviceName();
    if (Helper.isNullOrEmpty( deviceName )) {
      config.setId( null );
    }
    else {
      config.setId( Cfg.NAMESPACE.getQName( deviceName ) );
    }

    sourceCtrl.getDialogController().getDialogComp().showBusy( true );
    new Thread( new Runnable() {
      @Override
      public void run() {
        if (uploadCfg( delegate.getDeviceAddress(), config, sourceCtrl.getDialogController() )) {
          sourceCtrl.getDialogController().close( true );
        }
        sourceCtrl.getDialogController().getDialogComp().showBusy( false );
      }
    } ).start();
  }

}
