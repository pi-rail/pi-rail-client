package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.SystemManager;

public class EditName extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    RailDevice railDevice = (RailDevice) dataContext;
    EditCfgParamList parameterList = new EditCfgParamList( railDevice.getId(), railDevice.getDisplayName(), railDevice.getAddress().getInetAddress(), railDevice.getDeviceType() );
    String title = SystemManager.getInstance().getLocalizedMessage( "editNameEdit_H", null, null );
    sourceCtrl.getDialogController().openChildDialog( ControllerBuilder.NAMESPACE.getQName( "edit_name" ), title, parameterList );
  }
}
