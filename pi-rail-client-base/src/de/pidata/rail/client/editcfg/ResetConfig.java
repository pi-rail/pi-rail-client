package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.file.SelectAssetFileParamList;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class ResetConfig extends SaveCfgOperation {

  public static final String TITLE_CFG_HOCHLADEN = "Cfg hochladen?";
  private RailDevice railDevice = null;
  private Cfg cfg = null;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, DialogControllerDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (dataContext instanceof RailDevice) {
      this.railDevice = (RailDevice) dataContext;
      String firmwareName = railDevice.getFirmwareSketch();
      int pos = firmwareName.indexOf( ".ino" );
      if (pos > 0) {
        firmwareName = firmwareName.substring( 0, pos );
      }
      String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigSelectAsset_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigSelectAsset_TXT", null, null );
      SelectAssetFileParamList params = new SelectAssetFileParamList( SystemManager.STORAGE_CLASSPATH, "config/"+firmwareName, null, null, text );
      openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_asset_file" ), title, params );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof SelectAssetFileParamList) {
      SystemManager sysMan = SystemManager.getInstance();
      if (resultOK) {
        SelectAssetFileParamList assetFileParamList = (SelectAssetFileParamList) resultList;
        if (this.railDevice == null) {
          String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorNull_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorNull_TXT", null, null );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          return;
        }
        Storage storage = SystemManager.getInstance().getStorage( assetFileParamList.getStorageType(), assetFileParamList.getFileName() );
        File cfgFile = new File( storage.getPath( "cfg.xml" ) );
        if (!cfgFile.exists()) {
          Logger.warn( "Config File not found: '"+cfgFile.getAbsolutePath() + "'" );
          String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorNoIoCfg_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorNoIoCfg_TXT", null, null );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          return;
        }
        Model model;
        try {
          model = XmlReader.loadData( cfgFile.getAbsolutePath() );
          if (!(model instanceof Cfg)) {
            Logger.warn( "Config File '"+cfgFile.getAbsolutePath() +"' has wrong content: type="+model.type().name() + ", relation="+model.getParentRelationID() );
            String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorFileType_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorFileType_TXT", null, null );
            parentDlgCtrl.showMessage( title, text );
            this.railDevice = null;
            return;
          }
        }
        catch (Exception ex) {
          Logger.warn( "Config File '"+cfgFile.getAbsolutePath() +"' damaged, msg="+ex.getMessage() );
          String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorDamaged_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorDamaged_TXT", null, null );
          parentDlgCtrl.showMessage( title, text+"\n"+ex.getMessage() );
          this.railDevice = null;
          return;
        }
        this.cfg = (Cfg) model;
        Cfg oldCfg = railDevice.getConfig();
        String newName;
        if (oldCfg != null) {
          newName = railDevice.getConfig().getId().getName();
        }
        else {
          newName = cfg.getId().getName();
        }
        File readmeFile = new File( storage.getPath( "readme.txt" ) );
        StringBuilder sb = new StringBuilder();
        if (readmeFile.exists()) {
          BufferedReader br = new BufferedReader( new FileReader( readmeFile ) );
          try {
            String line = br.readLine();
            while (line != null) {
              sb.append( line ).append( "\n" );
              line = br.readLine();
            }
          }
          catch (Exception ex) {
            Logger.warn( "Error reading readme file='"+readmeFile.getAbsolutePath()+"', msg ="+ex.getMessage() );
            String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorReadme_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigErrorReadme_TXT", null, null );
            parentDlgCtrl.showMessage( title, text+"\n"+ex.getMessage() );
            this.railDevice = null;
            this.cfg = null;
            return;
          }
          finally {
            br.close();
          }
        }
        else {
          if (assetFileParamList.getFileName().endsWith( "standard" )) {
            sb.append( SystemManager.getInstance().getLocalizedMessage( "resetConfigUploadOkStd_TXT", null, null ) );
          }
          else {
            sb.append( SystemManager.getInstance().getLocalizedMessage( "resetConfigUploadOkElse_TXT", null, null ) );
          }
        }
        sb.append( "\n\n" ).append( SystemManager.getInstance().getLocalizedMessage( "resetConfigUploadOkWarn_TXT", null, null ) );
        String btnYes = SystemManager.getInstance().getGlossaryString( "upload" );
        String btnNo = SystemManager.getInstance().getGlossaryString( "cancel" );
        parentDlgCtrl.showQuestion( TITLE_CFG_HOCHLADEN, sb.toString(), btnYes, btnNo, null );
      }
    }
    else if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      String newName = result.getInputValue();
      if (!Helper.isNullOrEmpty( newName )) {
        cfg.setId( PiRailFactory.NAMESPACE.getQName( newName ) );
      }
      if (resultOK && (TITLE_CFG_HOCHLADEN.equals( result.getTitle()) )) {
        new Thread( new Runnable() {
          @Override
          public void run() {
            parentDlgCtrl.getDialogComp().showBusy( true );
            if (uploadCfg( railDevice.getAddress().getInetAddress(), cfg, parentDlgCtrl )) {
              Properties params = new Properties();
              params.put( "deviceName", railDevice.getDisplayName()  );
              String title = SystemManager.getInstance().getLocalizedMessage( "resetConfigSuccess_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "resetConfigSuccess_TXT", null, params );
              parentDlgCtrl.showMessage( title, text);
            }
            railDevice = null;
            cfg = null;
            parentDlgCtrl.getDialogComp().showBusy( false );
          }
        } ).start();
      }
      else {
        this.railDevice = null;
        this.cfg = null;
      }
    }
    else {
      this.railDevice = null;
      this.cfg = null;
    }
  }
}
