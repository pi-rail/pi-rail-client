package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.util.Properties;

public class AddDepot extends GuiOperation {

  private RailDevice railDevice;

  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    this.railDevice = (RailDevice) dataContext;
    if (dataContext != null) {
      sourceCtrl.getDialogController().showInput( "Betriebswerk (BW) anlegen", "Name für das neue Betriebswerk:", railDevice.getDisplayName(), "Anlegen", "Abbruch" );
    }
  }

  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (!Helper.isNullOrEmpty( result.getInputValue() )) {
        ModelRailway modelRailway = PiRail.getInstance().getModelRailway();
        Depot oldDepot = modelRailway.findDepot( railDevice.getAddress().getInetAddress() );
        if (oldDepot == null) {
          Depot depot = new Depot( PiRailTrackFactory.NAMESPACE.getQName( result.getInputValue() ) );
          depot.setDeviceAddress( railDevice.getAddress().getInetAddress() );
          modelRailway.addDepot( depot );
          Properties params = new Properties();
          params.put( "depotName", depot.getName().getName() );
          String title = SystemManager.getInstance().getLocalizedMessage( "addDepotSuccess_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addDepotSuccess_TXT", null, params );
          showMessage( parentDlgCtrl, title, text );
        }
        else {
          Properties params = new Properties();
          params.put( "moduleName", railDevice.getDisplayName() );
          params.put( "depotName", oldDepot.getName().getName() );
          String title = SystemManager.getInstance().getLocalizedMessage( "addDepotAlreadyExists_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "addDepotAlreadyExists_TXT", null, params );
          showMessage( parentDlgCtrl, title, text );
        }
      }
    }
    railDevice = null;
  }
}
