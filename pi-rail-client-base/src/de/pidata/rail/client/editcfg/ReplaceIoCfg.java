package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.client.file.SelectAssetFileParamList;
import de.pidata.rail.model.IoCfg;
import de.pidata.rail.railway.RailDevice;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class ReplaceIoCfg extends SaveCfgOperation {

  public static final String TITLE_IO_CFG_HOCHLADEN = "IoCfg hochladen?";
  private RailDevice railDevice;
  private IoCfg ioCfg;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param delegate    the delegate of the DialogController of which ctrlGroup is part of
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext context model for this call - differs form source's binding when called from render cell of a tabele or tree
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, DialogControllerDelegate delegate, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    if (dataContext instanceof RailDevice) {
      this.railDevice = (RailDevice) dataContext;
      String firmwareName = railDevice.getFirmwareSketch();
      int pos = firmwareName.indexOf( ".ino" );
      if (pos > 0) {
        firmwareName = firmwareName.substring( 0, pos );
      }
      String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigSelectAsset_H", null, null );
      String infoText = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigSelectAsset_TXT", null, null );
      SelectAssetFileParamList params = new SelectAssetFileParamList( SystemManager.STORAGE_CLASSPATH, "config/"+firmwareName, null, null, infoText );
      openChildDialog( sourceCtrl, NAMESPACE.getQName( "select_asset_file" ), title, params );
    }
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultList instanceof SelectAssetFileParamList) {
      SystemManager sysMan = SystemManager.getInstance();
      if (resultOK) {
        SelectAssetFileParamList assetFileParamList = (SelectAssetFileParamList) resultList;
        if (this.railDevice == null) {
          String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorNull_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorNull_TXT", null, null );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          return;
        }
        Storage storage = SystemManager.getInstance().getStorage( assetFileParamList.getStorageType(), assetFileParamList.getFileName() );
        File ioCfgFile = new File( storage.getPath( "ioCfg.xml"  ));
        if (!ioCfgFile.exists()) {
          Logger.warn( "IO-Config File not found: '"+ioCfgFile.getAbsolutePath() + "'" );
          String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorNoIoCfg_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorNoIoCfg_TXT", null, null );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          return;
        }
        Model model;
        try {
          model = XmlReader.loadData( ioCfgFile.getAbsolutePath() );
          if (!(model instanceof IoCfg)) {
            Logger.warn( "IO-Config File '"+ioCfgFile.getAbsolutePath() +"' has wrong content: type="+model.type().name() + ", relation="+model.getParentRelationID() );
            String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFileType_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFileType_TXT", null, null );
            parentDlgCtrl.showMessage( title, text );
            this.railDevice = null;
            return;
          }
        }
        catch (Exception ex) {
          Logger.warn( "IO-Config File '"+ioCfgFile.getAbsolutePath() +"' damaged, msg="+ex.getMessage() );
          String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorDamaged_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorDamaged_TXT", null, null );
          parentDlgCtrl.showMessage( title, text+"\n"+ex.getMessage() );
          this.railDevice = null;
          return;
        }
        this.ioCfg = (IoCfg) model;
        String firmwareName = this.railDevice.getFirmwareSketch();
        if (firmwareName != null) {
          int pos = firmwareName.indexOf( ".ino" );
          if (pos > 0) {
            firmwareName = firmwareName.substring( 0, pos );
          }
        }
        String firmwareVersion = this.railDevice.getFirmwareVersion();
        if (firmwareName == null || !firmwareName.equals( ioCfg.getFw() )) {
          Logger.warn( "Wrong firmware, ioCfg.fw=" + ioCfg.getFw() + ", module firmware= " + firmwareName );
          Properties params = new Properties();
          params.put( "cfgFwType", ioCfg.getFw() );
          params.put( "fwType", firmwareName );
          String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFwType_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFwType_TXT", null, params );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          this.ioCfg = null;
          return;
        }
        if ((firmwareVersion == null) || (firmwareVersion.compareTo( ioCfg.getVersion() ) < 0)) {
          Logger.warn( "Wrong firmware version, ioCfg.version=" + ioCfg.getVersion() + ", module version= " + firmwareVersion );
          Properties params = new Properties();
          params.put( "cfgFwVersion", ioCfg.getVersion() );
          params.put( "fwVersion", firmwareVersion );
          String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFwVersion_H", null, null );
          String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorFwVersion_TXT", null, params );
          parentDlgCtrl.showMessage( title, text );
          this.railDevice = null;
          this.ioCfg = null;
          return;
        }

        File readmeFile = new File( storage.getPath("readme.txt" ) );
        StringBuilder sb = new StringBuilder();
        if (readmeFile.exists()) {
          BufferedReader br = new BufferedReader( new FileReader( readmeFile ) );
          try {
            String line = br.readLine();
            while (line != null) {
              sb.append( line ).append( "\n" );
              line = br.readLine();
            }
          }
          catch (Exception ex) {
            Logger.warn( "Error reading readme file='"+readmeFile.getAbsolutePath()+"', msg ="+ex.getMessage() );
            String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorReadme_H", null, null );
            String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigErrorReadme_TXT", null, null );
            parentDlgCtrl.showMessage( title, text+"\n"+ex.getMessage() );
            this.railDevice = null;
            this.ioCfg = null;
            return;
          }
          finally {
            br.close();
          }
        }
        else {
          if (assetFileParamList.getFileName().endsWith( "standard" )) {
            sb.append( SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigUploadOkStd_TXT", null, null ) );
          }
          else {
            sb.append( SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigUploadOkElse_TXT", null, null ) );
          }
        }
        sb.append( "\n\n" ).append( SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigUploadOkWarn_TXT", null, null ) );
        String btnYes = SystemManager.getInstance().getGlossaryString( "upload" );
        String btnNo = SystemManager.getInstance().getGlossaryString( "cancel" );
        parentDlgCtrl.showQuestion( TITLE_IO_CFG_HOCHLADEN, sb.toString(), btnYes, btnNo, null );
      }
    }
    else if (resultList instanceof QuestionBoxResult) {
      QuestionBoxResult result = (QuestionBoxResult) resultList;
      if (resultOK && (TITLE_IO_CFG_HOCHLADEN.equals( result.getTitle()) )) {
        new Thread( new Runnable() {
          @Override
          public void run() {
            boolean success = uploadIoCfg( railDevice, ioCfg, parentDlgCtrl );
            if (success) {
              Properties params = new Properties();
              params.put( "deviceName", railDevice.getDisplayName()  );
              String title = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigSuccess_H", null, null );
              String text = SystemManager.getInstance().getLocalizedMessage( "replaceIOConfigSuccess_TXT", null, params );
              parentDlgCtrl.showMessage( title, text);
            }
            railDevice = null;
            ioCfg = null;
            parentDlgCtrl.getDialogComp().showBusy( false );
          }
        } ).start();
      }
      else {
        this.railDevice = null;
        this.ioCfg = null;
      }
    }
    else {
      this.railDevice = null;
      this.ioCfg = null;
    }
  }
}

