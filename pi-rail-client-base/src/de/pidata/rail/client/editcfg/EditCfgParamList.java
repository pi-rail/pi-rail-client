/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.editcfg;

import de.pidata.qnames.QName;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterType;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EditCfgParamList extends AbstractParameterList {

  public static final QName DEVICE_ID = ModelRailway.NAMESPACE.getQName( "deviceID" );
  public static final QName DEVICE_NAME = ModelRailway.NAMESPACE.getQName( "deviceName" );
  public static final QName IP_ADDRESS = ModelRailway.NAMESPACE.getQName( "ipAddress" );
  public static final QName DEVICE_TYPE = ModelRailway.NAMESPACE.getQName( "deviceType" );

  public EditCfgParamList() {
    super( ParameterType.QNameType, DEVICE_ID, ParameterType.StringType, DEVICE_NAME, ParameterType.StringType, IP_ADDRESS, ParameterType.StringType, DEVICE_TYPE );
  }

  public EditCfgParamList( QName deviceID, String deviceName, InetAddress ipAddress, String deviceType ) {
    this();
    setQName( DEVICE_ID, deviceID );
    setString( DEVICE_NAME, deviceName );
    if (ipAddress != null) {
      setString( IP_ADDRESS, ipAddress.getHostAddress() );
    }
    setString( DEVICE_TYPE, deviceType );
  }

  public QName getDeviceId() {
    return getQName( DEVICE_ID );
  }
  public String getDeviceName() {
    return getString( DEVICE_NAME );
  }

  public InetAddress getIPAddress() throws UnknownHostException {
    return InetAddress.getByName( getString( IP_ADDRESS ) );
  }

  public String getDeviceType() {
    return getString( DEVICE_TYPE );
  }
}

