/*
 * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.log.Logger;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.rail.comm.ConfigLoader;
import de.pidata.rail.model.Cfg;
import de.pidata.rail.model.IoCfg;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.rail.track.TrackCfg;
import de.pidata.system.base.SystemManager;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

public abstract class SaveCfgOperation<DCD extends DialogControllerDelegate> extends GuiDelegateOperation<DCD> {
  
  public static boolean uploadCfg( InetAddress deviceAddress, Cfg cfg, DialogController dlgCtrl ) {
    Integer version = cfg.getVersion();
    if (version == null) {
      cfg.setVersion( Integer.valueOf( 1 ) );
    }
    else {
      cfg.setVersion( Integer.valueOf( version.intValue() + 1 ) );
    }

    XmlWriter xmlWriter = new XmlWriter( null );
    StringBuilder stringBuffer = xmlWriter.writeXML( cfg, PiRailFactory.ID_CFG, true );
    Properties params = new Properties();
    params.put( "filename", ConfigLoader.CFG_XML );
    params.put( "addr", deviceAddress );
    try {
      int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.CFG_XML );
      if ((rc >= 200) &&  (rc < 300)) {
        Logger.info( "Successfully updated "+ConfigLoader.CFG_XML+" on "+deviceAddress );
        return true;
      }
      else {
        Logger.error( "Error updating " + ConfigLoader.CFG_XML + " on " + deviceAddress + ", responseCode=" + rc );
        String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
        dlgCtrl.showMessage( title, text + "\nHTTP rc="+rc );
      }
    }
    catch (Exception e) {
      Logger.error( "Error while upload", e );
      String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
      dlgCtrl.showMessage( title, text + "\nmsg="+e.getLocalizedMessage() );
    }
    return false;
  }

  public static boolean uploadIoCfg( RailDevice railDevice, IoCfg ioCfg, DialogController dlgCtrl ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    StringBuilder stringBuffer = xmlWriter.writeXML( ioCfg, PiRailFactory.ID_IOCFG, true );
    InetAddress deviceAddress = railDevice.getAddress().getInetAddress();
    Properties params = new Properties();
    params.put( "filename", ConfigLoader.IO_CFG_XML );
    params.put( "addr", deviceAddress );
    try {
      int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.IO_CFG_XML );
      if ((rc >= 200) &&  (rc < 300)) {
        Logger.info( "Successfully updated "+ConfigLoader.IO_CFG_XML+" on "+deviceAddress );
        dlgCtrl.close( true );
        return true;
      }
      else {
        Logger.error( "Error updating " + ConfigLoader.IO_CFG_XML + " on " + deviceAddress + ", responseCode=" + rc );
        String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
        dlgCtrl.showMessage( title, text + "\nHTTP rc="+rc );
        return false;
      }
    }
    catch (Exception e) {
      Logger.error( "Error updating " + ConfigLoader.IO_CFG_XML + " on " + railDevice, e );
      String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
      dlgCtrl.showMessage( title, text + "\nmsg="+e.getLocalizedMessage() );
      return false;
    }
  }

  public static boolean uploadTrackCfg( InetAddress deviceAddress, TrackCfg trackCfg, DialogController dlgCtrl ) {
    Integer version = trackCfg.getVersion();
    if (version == null) {
      trackCfg.setVersion( Integer.valueOf( 1 ) );
    }
    else {
      trackCfg.setVersion( Integer.valueOf( version.intValue() + 1 ) );
    }

    XmlWriter xmlWriter = new XmlWriter( null );
    StringBuilder stringBuffer = xmlWriter.writeXML( trackCfg, PiRailTrackFactory.ID_TRACKCFG, true );
    Properties params = new Properties();
    params.put( "filename", ConfigLoader.TRACK_CFG_XML );
    params.put( "addr", deviceAddress );
    try {
      int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.TRACK_CFG_XML );
      if ((rc >= 200) &&  (rc < 300)) {
        Logger.info( "Successfully updated "+ConfigLoader.TRACK_CFG_XML+" on "+deviceAddress );
        return true;
      }
      else {
        Logger.error( "Error updating " + ConfigLoader.TRACK_CFG_XML + " on " + deviceAddress + ", responseCode=" + rc );
        String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
        dlgCtrl.showMessage( title, text + "\nHTTP rc="+rc );
        return false;
      }
    }
    catch (Exception e) {
      Logger.error( "Error updating " + ConfigLoader.TRACK_CFG_XML + " on " + deviceAddress, e );
      String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
      dlgCtrl.showMessage( title, text + "\nmsg="+e.getLocalizedMessage() );
      return false;
    }
  }

  public static boolean uploadDepot( InetAddress deviceAddress, Depot depot, DialogController dlgCtrl ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    StringBuilder stringBuffer = xmlWriter.writeXML( depot, PiRailTrackFactory.ID_DEPOT, true );
    Properties params = new Properties();
    params.put( "filename", ConfigLoader.DEPOT_XML );
    params.put( "addr", deviceAddress );
    try {
      int rc = ConfigLoader.doUpload( deviceAddress, stringBuffer, ConfigLoader.DEPOT_XML );
      if ((rc >= 200) &&  (rc < 300)) {
        Logger.info( "Successfully updated "+ConfigLoader.DEPOT_XML+" on "+deviceAddress );
        return true;
      }
      else {
        Logger.error( "Error updating " + ConfigLoader.DEPOT_XML + " on " + deviceAddress + ", responseCode=" + rc );
        String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
        String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
        dlgCtrl.showMessage( title, text + "\nHTTP rc="+rc );
        return false;
      }
    }
    catch (IOException e) {
      Logger.error( "Error updating " + ConfigLoader.DEPOT_XML + " on " + deviceAddress, e );
      String title = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_H", null, null );
      String text = SystemManager.getInstance().getLocalizedMessage( "saveCfgOperation_Error_TXT", null, params );
      dlgCtrl.showMessage( title, text + "\nmsg="+e.getLocalizedMessage() );
      return false;
    }
  }
}
