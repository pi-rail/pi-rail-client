package de.pidata.rail.client.editcfg;

import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.TextEditorController;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.WifiState;
import de.pidata.rail.model.Csv;
import de.pidata.rail.model.State;
import de.pidata.rail.railway.DataListener;
import de.pidata.service.base.ParameterList;

import java.net.InetAddress;

public abstract class AbstractEditConfig implements DialogControllerDelegate<EditCfgParamList, ParameterList>, DataListener {

  protected QName deviceId;
  protected InetAddress deviceAddress;
  protected TextEditorController updaterOutput;
  protected StringBuilder messageCollector = new StringBuilder();

  public QName getDeviceId() {
    return deviceId;
  }

  public InetAddress getDeviceAddress() {
    return deviceAddress;
  }

  public TextEditorController getUpdaterOutput() {
    return updaterOutput;
  }

  public StringBuilder getMessageCollector() {
    return messageCollector;
  }

  @Override
  public void processData( Csv csvData ) {
    if (updaterOutput != null) {
      String dat = csvData.getDat();
      String[] data = dat.split( ";" );
      for (String str : data) {
        if (str.startsWith( "\"" )) {
          str = str.substring( 1, str.length()-1 );
        }
        messageCollector.append( str ).append( " " );
      }
      messageCollector.append( "\n" );
      updaterOutput.setValue( messageCollector.toString() );
    }
  }

  @Override
  public void processState( State state ) {
    // do nothing
  }

  @Override
  public void processWifiState( WifiState newState ) {
    // do nothing
  }
}
