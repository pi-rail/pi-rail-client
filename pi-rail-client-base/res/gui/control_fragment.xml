<?xml version="1.0" encoding="UTF-8"?>
<!--
  * This file is part of PI-Rail Client (https://gitlab.com/pi-rail/pi-rail-client).
  * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, version 3.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
-->
<dialogModule name="control_fragment" modelTypeID="de.pidata.rail.railway.Locomotive"
        moduleClass="de.pidata.rail.client.controlFragment.ControlFragment"
        xmlns="de.pidata.gui"
        xmlns:rw="http://res.pirail.org/railway.xsd"
        xmlns:pir="http://res.pirail.org/pi-rail.xsd">

  <!-- Top container item  -->

  <textCtrl ID="LocoName" textCompID="T_displayname" readOnly="true">
    <attrBinding attrName="rw:displayname"/>
  </textCtrl>

  <imageCtrl ID="wlanicon" imageCompID="TT_ctrl_wlanicon">
    <attrBinding attrName="rw:wlanicon"/>
  </imageCtrl>

  <buttonCtrl ID="PreviousLoco" buttonCompID="T_PreviousLoco">
    <action classname="de.pidata.rail.client.controlFragment.PreviousLoco"/>
  </buttonCtrl>

  <buttonCtrl ID="NextLoco" buttonCompID="T_NextLoco">
    <action classname="de.pidata.rail.client.controlFragment.NextLoco"/>
  </buttonCtrl>

  <imageCtrl ID="LocoImage" imageCompID="T_Locoimage" readOnly="true">
    <attrBinding attrName="rw:icon"/>
  </imageCtrl>

  <buttonCtrl ID="LocoLockBtn" buttonCompID="P_Lock">
    <action classname="de.pidata.rail.client.controlFragment.LocoLockButton"/>
    <iconAttrBinding attrName="rw:iconLock"/>
  </buttonCtrl>

  <buttonCtrl ID="EmergencyBtn" buttonCompID="emergency_btn">
    <action classname="de.pidata.rail.client.controlFragment.EmergencyStop"/>
  </buttonCtrl>

  <buttonCtrl ID="LocoClockDir" buttonCompID="loco_clock_dir">
    <action classname="de.pidata.rail.client.controlFragment.LocoClockDirButton"/>
    <iconAttrBinding attrName="rw:iconClockDir"/>
  </buttonCtrl>
  <buttonCtrl ID="AutoDrive" buttonCompID="auto_drive">
    <action classname="de.pidata.rail.client.controlFragment.AutoDriveButton"/>
    <iconAttrBinding attrName="rw:iconAutoDrive"/>
  </buttonCtrl>

  <paintCtrl ID="wagon_pane" paintCompID="wagon_pane">
    <attrBinding attrName="rw:wagonImage"/>
  </paintCtrl>

  <buttonCtrl ID="add_wagon" buttonCompID="add_wagon">
    <action classname="de.pidata.rail.client.controlFragment.AddWagon"/>
  </buttonCtrl>

  <buttonCtrl ID="remove_wagon" buttonCompID="remove_wagon">
    <action classname="de.pidata.rail.client.controlFragment.RemoveWagon"/>
  </buttonCtrl>

  <progressCtrl ID="LocoSpeed" progressCompID="P_Speed">
    <attrBinding attrName="rw:targetSpeedPercent"/>
  </progressCtrl>
  <textCtrl ID="lineSpeed" textCompID="line_speed" readOnly="true">
    <attrBinding attrName="rw:lineSpeed"/>
  </textCtrl>
  <textCtrl ID="speedLimit" textCompID="speed_limit" readOnly="true">
    <attrBinding attrName="rw:speedLimit"/>
  </textCtrl>
  <textCtrl ID="targetSpeed" textCompID="target_speed" readOnly="true">
    <attrBinding attrName="rw:targetSpeed"/>
  </textCtrl>
  <textCtrl ID="accel" textCompID="accel" readOnly="true">
    <attrBinding attrName="rw:accel"/>
  </textCtrl>

  <textCtrl ID="LastEvent" textCompID="T_lastrfid" readOnly="true">
    <attrBinding attrName="rw:lastEvent"/>
  </textCtrl>
  <textCtrl ID="EventDist" textCompID="event_dist" readOnly="true">
    <attrBinding attrName="rw:eventDist"/>
  </textCtrl>

  <buttonCtrl ID="speedDecLarge" buttonCompID="speedDecLarge">
    <action classname="de.pidata.rail.client.controlFragment.ChangeSpeedButton"/>
  </buttonCtrl>
  <buttonCtrl ID="speedDecSmall" buttonCompID="speedDecSmall">
    <action classname="de.pidata.rail.client.controlFragment.ChangeSpeedButton"/>
  </buttonCtrl>
  <textCtrl ID="currentSpeed" textCompID="T_speed" readOnly="true">
    <attrBinding attrName="rw:targetSpeedPercent"/>
  </textCtrl>
  <buttonCtrl ID="speedIncSmall" buttonCompID="speedIncSmall">
    <action  classname="de.pidata.rail.client.controlFragment.ChangeSpeedButton"/>
  </buttonCtrl>
  <buttonCtrl ID="speedIncLarge" buttonCompID="speedIncLarge">
    <action  classname="de.pidata.rail.client.controlFragment.ChangeSpeedButton"/>
  </buttonCtrl>

  <buttonCtrl ID="LocoPlayPauseController" buttonCompID="P_Stop">
    <action classname="de.pidata.rail.client.controlFragment.LocoDirButton"/>
    <iconAttrBinding attrName="rw:iconPause"/>
  </buttonCtrl>
  <buttonCtrl ID="LocoForward" buttonCompID="P_Forward">
    <action classname="de.pidata.rail.client.controlFragment.LocoDirButton"/>
    <iconAttrBinding attrName="rw:iconFwd"/>
  </buttonCtrl>
  <buttonCtrl ID="LocoBackward" buttonCompID="P_Backward">
    <action classname="de.pidata.rail.client.controlFragment.LocoDirButton"/>
    <iconAttrBinding attrName="rw:iconBack"/>
  </buttonCtrl>

  <tableCtrl ID="functionTable" tableViewCompID="function_tab" selectable="false">
    <listBinding selType="SINGLE" relationID="rw:action"/>
    <row rowCompID="function_row">
      <column ID="functionID" columnCompID="function_id" readOnly="true">
        <attrBinding attrName="rw:id"/>
      </column>
      <column ID="state1" columnCompID="state_1" readOnly="true" align="center">
        <cellAction ID="sendCommand1">
          <action classname="de.pidata.rail.client.controlFragment.SendFunctionCommand"/>
        </cellAction>
        <iconAttrBinding attrName="rw:iconState1"/>
      </column>
      <column ID="state2" columnCompID="state_2" readOnly="true" align="center">
        <cellAction ID="sendCommand2">
          <action classname="de.pidata.rail.client.controlFragment.SendFunctionCommand"/>
        </cellAction>
        <iconAttrBinding attrName="rw:iconState2"/>
      </column>
      <column ID="state3" columnCompID="state_3" readOnly="true" sortable="false" align="center">
        <cellAction ID="sendCommand3">
          <action classname="de.pidata.rail.client.controlFragment.SendFunctionCommand"/>
        </cellAction>
        <iconAttrBinding attrName="rw:iconState3"/>
      </column>
      <column ID="state4" columnCompID="state_4" readOnly="true" sortable="false" align="center">
        <cellAction ID="sendCommand4">
          <action classname="de.pidata.rail.client.controlFragment.SendFunctionCommand"/>
        </cellAction>
        <iconAttrBinding attrName="rw:iconState4"/>
      </column>
      <column ID="state5" columnCompID="state_5" readOnly="true" sortable="false" align="center">
        <cellAction ID="sendCommand5">
          <action classname="de.pidata.rail.client.controlFragment.SendFunctionCommand"/>
        </cellAction>
        <iconAttrBinding attrName="rw:iconState5"/>
      </column>
      <column ID="functionDetails" columnCompID="function_details_btn" readOnly="true" sortable="false" align="center">
        <cellAction ID="editTower">
          <action classname="de.pidata.rail.client.controlFragment.FunctionDetailsDlg"/>
        </cellAction>
      </column>
    </row>
  </tableCtrl>

</dialogModule>

