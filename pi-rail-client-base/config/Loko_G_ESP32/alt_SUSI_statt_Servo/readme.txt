Diese IO-Config ist nur für die erste Generation CTC-Lokmodul-G gedacht.
Sie ergänzt die SUSI-Schnittstelle als Adapter auf den Servo-Anschlüssen.
Siehe dazu auch unsere Umbau-Doku der Piko-G BR50.