Alternative IO-Config für die alte Weiche-G.
Statt der beiden IR-Ports gibt es vier weitere Sensor-Anschlüsse.
ACHTUNG: Die Sensoren 3 bis 6 haben keinen Optokoppler, sind also
direkt mit dem Prozessor verbunden und vertragen somit max. 3,3 Volt!
